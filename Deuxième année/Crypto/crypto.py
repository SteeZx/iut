import math as mt
import time
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress
import random as rd

##Exercice 1##
list_nb_a_tester=[6, 13,1009,10007,100003,1000003,10000019,100000007,1000000007,10000000019,100000000003,1000000000039,100000000000031]



def nb_premier(n):
  for i in range(2,int(mt.sqrt(n))+1):
    if (n%i) == 0:
      return False
  return True

def ppdp(n):
  for i in range(2,n):
    if nb_premier(i):
      return i
  return -1

def nb_premier_opti(n):
    p = ppdp(n)
    # print(p)
    if mt.sqrt(n)<=p :
        return False
    return True


def list_nbpremier(n):
    result = []
    for i in range(n, 2, -1):
        if nb_premier_opti(i):
            result.append(i)
    proportion = len(result)/n
    proportion_ln = 1/(mt.log(n)-1)
    
    return result, proportion, proportion_ln

def pgcd(a, b):
    d = 0
    while a!=b :
        d = abs(b-a)
        b = a
        a = d
    return d

# for i in list_nb_a_tester:
    # print(nb_premier(i))
    # print(list_nbpremier(i))
    
def euclide_etendu(a, b):
    u0, u1, v0, v1= 1,0,0,1
    q,r=a//b, a%b
    while r!= 0:
        a,b=b,r
        u2,v2=u0-q*u1, v0-q*v1
        u0,v0,u1,v1 = u0,v0,u1,v1
        q,r=a//b,a%b
    return (b, u1, v1)
##Exercice 3##
dict_cod={'a':1,'b':2,'c':3,'d':4,'e':5,'f':6,'g':7,'h':8,'i':9,'j':10,'k':11,'l':12,'m':13,'n':14,'o':15,'p':16,'q':17,'r':18,'s':19,'t':20,'u':21,'v':22,'w':23,'x':24,'y':25,'z':26,' ':27,',':28,'.':29,'?':30,':':0}

def chiff_affine(mess : str, a : int, b : int):
    new_message = ""
    
    for i in mess:
        id = dict_cod.get(i)
        id = (id*a)+b
        if id > 30 :
            id = id - 30
        dict = dict_cod.keys()
        
        new_message = new_message + dict
    return new_message

# print(chiff_affine("abc", 2, 1))

def gen_aleatoire(p):
    """
    Renvoie un nombre générateur compris entre 2 et p-1
    """
    # p doit être premier
    
    g = rd.randint(2, p-1)
    reste = g
    list = [g]
    # Ajoute le résultat à la liste tant que 1 n'a pas été trouvé
    while reste != 1:
        reste = (g * reste) % p
        list.append(reste)

    # Si la liste contient tous les numéros entre 1 et p-1, g choisi aléatoirement au début est générateur.
    return g if sorted(list) == [i for i in range(1, p)] else gen_aleatoire(p)

# print(gen_aleatoire(11))

def discrete_log(g,p):
    y = rd.randint(2, p-1)
    for i in range(1, p):
        if (g**i % p == y):
            return i
    return 0
g = gen_aleatoire(7)
print(g)
print(discrete_log(g, 7))

from typing import Tuple, List

def run_shanks_algo(self, a: int, b: int, p: int) -> Tuple[int, int, List, List]:

        m = int(mt.ceil(mt.sqrt(p)))
        giant_dict = {}
        step = self.exp(a, m, p)

        baby_list = []
        giant_list = [1]

        # giant steps calculation
        last_val = 1
        for i in range(m):
            last_val = (last_val * step) % p
            giant_list.append(last_val)
            if last_val not in giant_dict:
                giant_dict[last_val] = i + 1

        # baby steps calculation, terminates when we match value in giant_dict
        last_val = b
        for i in range(m):
            baby_list.append(last_val)
            if last_val in giant_dict:
                self.x1 = giant_dict[last_val]
                self.x2 = i
                break
            last_val = (last_val * a) % p

        return self.x1, self.x2, giant_list, baby_list
    
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================
#==============================================================================================================================================================================================


def generateur(p):
    g = rd.randint(2,p-1)
    list = [g]
    reste = g
    while(reste != 1):
        reste = g * reste
        if reste > p:
            reste = reste % p
        if reste in list:
            break
        list.append(reste)
    bon = True

    for i in range (1,p):
        bon = i in list
        if bon == False:
            break

    if bon == False:
        g = generateur(p)

    # print(list)
    return g

print(generateur(11))




def log_discret(y,p,g):
    for i in range(1,p):
        if (g**i%p == y):
            return i
    return 0


p = 7
g = generateur(p)
y = rd.randint(2,p-1) #y doit se situer entre 2 et p
print(log_discret(y,p,g))


def pgcd(a, b):
    """
    Calcul le PGCD d'un nombre selon la méthode d'euclide
    """
    if b == 0:
        return a
    return pgcd(b, a%b)

def euclideEtendu(a, b):
    """
    Retourne un set de nombre comprenants (b, u, v) avec
    d = PGCD de a et b
    u et v vérifiant l'équation au + bv = d
    """
    u0 = 1
    u1 = 0
    v0 = 0
    v1 = 1

    r = a%b
    q = a//b

    while r > 0:
        r = a%b
        q = a//b
        
        a = b
        b = r

        if r > 0:
            u2 = u0 - q * u1
            v2 = v0 - q * v1
            u0 = u1
            v0 = v1
            u1 = u2
            v1 = v2

    return (pgcd(a, b), u1, v1)


def log_discret_shanks(y,p,g):
    s = 1 + int(mt.sqrt(p))
    inv = euclideEtendu(g,p)[1]
    print(inv)
    l1 = [1]
    l1.extend([(g**i)%p for i in range(1,s)])
    l2 = [y]
    l2.extend([(y*inv**i)%p for i in range(1,s)])

    print(l1)
    print(l2)
    
    # for i in range(1,s):
    #     a=g**i
    #     if a in l2:
    #         b = a
    #         break
    #     l1.append(a)
    #     b = y*(inv**(i))
    #     if b in l1:
    #         break
    #     l2.append(b)

    a= 0

    for i in l1:
        if i in l2:
            a=i
    
    b = l2.index(a)
    a = l1.index(a)

    x = a+b*s

    return x

print(log_discret_shanks(y,p,g))



# a*1*p = 1 alors a**p-1 = 1[p] #p est la clé publique de la crypto

# x -> g**x[p]

# log discret(y0) = x0

# y0 = g**x0

# Protocole échange de clé : e : m -> m+k*[p]  traduction : à partir d'un message on crypte le message 

# Alice : g   (pars secret, générateur)                  Bobs : g (b aléatoire secret)

# a : Aléatoire (secret)                                 # b : Aléatoire (secret)

# Alice envoie le message crypté par (g**a)*[p] à Bobs          Bobs calcul k = ((g**a)**b)*[p]           k = (g**a*b) % [p]     g : générateur, connu par Bobs    [p] : clé publique, connu par Bobs(longueur de l'alphabet utilisé)   a : secret
# et Bobs envoie le message crypté par (g**b)*[p]               Alice calcul k = ((g**b)**a)*[p]          k = (g**a*b) % [p]     g : générateur, connu par Alice   [p] : clé publique, connu par Alice  a : secret

# El Gamal 

# Alice : clé publique : (p(premier), g(générateur), A) avec A obtenu tel que A = g**a (a secret)

# Bobs veut envoyer un message à Alice. (Il choisit un b secret) b se situe entre 2 et p-1 avec p longeur de l'alphabet utilisé

# Il envoie {- c (un msg codé) = m * (A**b) 
#           {- B = (g**b)

# Alice déchiffre le message en effectuant (c/(B**a))*[p] = ((m*g**(a*b))/(g**(a*b)))*[p] avec c("msg codé envoyé par Bobs")

#     ab
# m*g
# ---  * [p]
#   ab
# g

# Pas de bébé, Pas de géant : s = 1 + [racine(p)]

# y -> y0 le nombre qu'on veut inverser

# l1 : y, y*g, y*g**2, ... , y*g**s-1 une liste de s élément
# l2 : 1, y*g**-s, y*g**-2s, ... , y*g**-(s-1)s                 <- constater que c'est plus rapide qu'un simple tests

# tel que y*g**(r*o) = g**(-kos)

# On peut montrer que cet élément commun provient de (x0) = (k0)S + (r0)


# y*g**(-(k0)S) = g**(x0) * g **(-k0S)
#               = g**(r0 + k0 * S) * g **(-k0S)
#               = g**(r0)

import math as mt
import time
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress
import random as rd
import hashlib as hash


"""# Exercice 5 

## scénario

Je suis Alice
- je choisis un nombre premier p
- je cherche un générateur g de (Z/pZ)*
- je choisis un exposant a dans [2, p-2] qui sera ma clé privée
- je calcule A qui est égal à g^a
- je publie ma clé publique qui se compose de (p, g, A)

Je suis Bob
- j'ai à ma disposition p, g et A qui sont publics
- je choisis un exposant b dans [2, p-2] qui sera ma clé privée
- je calcule B qui est égal à g^b
- je publie ma clé publique qui se compose de (p, g, B)
- je souhaite chiffrer un message m à Alice
- je vais utiliser le calcul A^b qui dans les faits est égal à (g^b)^a ou encore g^(b*a) ou encore (g^a)^b qu'Alice peut calculer aussi de son côté
- je chiffre le message avec la fonction m*A^b mod(p) ce qui me donne c, et j'envoie à Alice le coupe (c, B)

Je suis Alice

- je calcule l'[inverse modulaire](https://fr.wikipedia.org/wiki/Inverse_modulaire) de B^a (i.e. (g^b)^a) qui correspond au coeff de Bezout u calculé à partir de l'algorithme d'Euclide étendue 
- je déchiffre le message chiffré c à l'aide de la fonction c*u mod(p)
"""


""" 
## Mise en oeuvre

Je suis Alice
- pour un semblant de réalité (mais pas trop), je choisis p dans [1000, 2000]. Pour ce faire, je peux réutiliser mes méthodes précédemment développées. Par exemple je peux précalculer les nombres premiers de 1 à 2000. Puis tirer au hazard un nombre de cette liste tant que j'en obtiens pas un de plus de 1000... Probablement que les 25 premiers nombres de cette liste peuvent être sautés.
TODO
""" 

def premier(n):
    """
    Retourne True si un nombre est premier.
    Fonction optimisée (Question 4)
    """
    if n <= 1:
        return False
    elif n == 2:
        return True
    elif n % 2 == 0:
        return False


    for i in range(3, int(mt.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

def listPrime(b,n):
    """
    Retourne la liste des nombres premiers strictement inférieurs à n
    """
    if b <= 0:
        return []

    premiers = []

    while(n != len(premiers)):

        if premier(b):
            premiers.append(b)
        b+=1

    return premiers






""" 
Je suis Alice
- pour trouver g je peux calculer (Z/pZ)* puis pour chaque valeur de g compris entre 1 et p-1 tester si g est générateur en construisant l'ensemble générée par g et en le comparant à (Z/pZ)*. Je stoppe quand ils sont identiques.
TODO
""" 
def generateur(p):
    g = rd.randint(2,p-1)
    list = [g]
    reste = g
    while(reste != 1):
        reste = g * reste
        if reste > p:
            reste = reste % p
        if reste in list:
            break
        list.append(reste)
    bon = True

    for i in range (1,p):
        bon = i in list
        if bon == False:
            break

    if bon == False:
        g = generateur(p)

    # print(list)
    return g


""" 
Je suis Alice
- je choisis aléatoirement a dans [2, p-2]
TODO
""" 
def key_creation():

    p = rd.choice(listPrime(1000,134))

    a = rd.randint(2,p-2)

    g = generateur(p)

    A = (g**a)%p

    return p,g,A,a

p,g,A,a = key_creation()
print(p,g,A,a)

""" 
Je suis Bob
- je prépare mon message pour pouvoir le chiffrer i.e. que je le transforme en valeur numérique et je le découpe en paquets suivant le modulo p ; je chiffrerai chaque paquet
TODO cf. code dans le sujet
"""

def empreinte():
     message="Je d´eclare etre bien l'auteur de ce texte par lequel\
     je te d´eclare ma flamme. Bien romantiquement. Bernard"
     #Mais Bernard c'est un peu confidentiel ¸ca quand m^eme
     message_utf8=message.encode()
     '''utilisation de la fonction de hashage SHA256 et encode
     en hexad´ecimal :'''
     message_hash=hash.sha256(message_utf8).hexdigest()
     #conversion en entier base 10 :
     number_message_hash=int(message_hash,16)

     print(number_message_hash)

     return number_message_hash
     

def conv(n):
    liste = []
    n=str(n)
    for i in n:
        liste.append(int(i))
    return liste

# empreinte()

# from ex1 import euclideEtendu

def pgcd(a, b):
    """
    Calcul le PGCD d'un nombre selon la méthode d'euclide
    """
    if b == 0:
        return a
    return pgcd(b, a%b)

def euclideEtendu(a, b):
    """
    Retourne un set de nombre comprenants (b, u, v) avec
    d = PGCD de a et b
    u et v vérifiant l'équation au + bv = d
    """
    u0 = 1
    u1 = 0
    v0 = 0
    v1 = 1

    r = a%b
    q = a//b

    while r > 0:
        r = a%b
        q = a//b
        
        a = b
        b = r

        if r > 0:
            u2 = u0 - q * u1
            v2 = v0 - q * v1
            u0 = u1
            v0 = v1
            u1 = u2
            v1 = v2

    return (pgcd(a, b), u1, v1)

def bernard(p, A,g):

   

    b = rd.randint(2,p-2)

    B = (g**b)%p

    message_number = empreinte()
    list_number = []
    nombre = 0
    taille =0
    liste = conv(message_number)
    print(liste)
    for i in range(0,len(liste)):
        nombre = liste[i] + nombre*10
        taille +=1
        if taille == 3:
            list_number.append(nombre)
            nombre = 0
            taille = 0

    if taille!=0:
        list_number.append(nombre)

    for i in range(len(list_number) // 2):
        list_number[i], list_number[-1 - i] = list_number[-1 - i], list_number[i]

    print(list_number)

    for i in range(0,len(list_number)):
        list_number[i] = (list_number[i]*(A**b))%p


    message_dechiffre = []

    for i in list_number:
        message_dechiffre.append((i*euclideEtendu(p,(B**a)%p)[2]) %p)

    message_dechiffre.reverse()
    
    print(list_number, B)

    return message_dechiffre

import numpy as np
import random as rd
def deg(A):
    res=-1
    for i in range(len(A)) :
        if A[i]!=0:
            res=len(A)-1-i
            return res
    return res
 
def plus(A,B):
    res=[]
    if len(A)>=len(B):
        for i in range(len(A)-len(B)):
            res.append(A[i])
        for i in range (len(A)-len(B), len(A)):
            res.append((A[i]+B[i-(len(A)-len(B))])%2)
        return res
    else :
        return plus(B,A)
    
    
A1=[0,1,1,0,1,0,1,0,0]
B1=[0,0,1,1,0,1,0,1,0]
print(plus(A1,B1))
print(plus(B1,A1))
            
def div_eucl_pol(A,B):
    if deg(A)<deg(B):
        return [0],A.copy()
    Q=[]
    while deg(A)>=deg(B):
        if A[0]==0:
            A=A[1:]
        else :
            if Q==[]:
                Q=[1]+[0 for i in range(deg(A)-deg(B))]
                A=plus(A,B+[0 for i in range(deg(A)-deg(B))])[1:]
            else :
                Q[deg(B)-deg(A)-1]=1
                A=plus(A,B+[0 for i in range(deg(A)-deg(B))])[1:]
        
    return Q,A[-deg(B):].copy()

G=[1,0,0,1,1]
MI=[1,0,1,1,1,0,1,0,0,1]
MR=[1,0,1,1,1,0,1,0,0,1,0,0,0,0]

print('div_eucl_pol(MR,G) : ',div_eucl_pol(MR,G))
#div_eucl_pol(MR,G) :  ([1, 0, 1, 0, 0, 1, 0, 0, 1, 0], [0, 1, 1, 0])
def envoi_message_CRC(mess,G):
    mess_decale=mess+[0 for i in range(deg(G))]
    return plus(mess_decale,div_eucl_pol(mess_decale,G)[1])

mess=list(np.random.randint(0,2,240))
G_CRC_16=[1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1]
mess_envoye=envoi_message_CRC(mess,G_CRC_16)
test_CRC=(sum(div_eucl_pol(mess_envoye,G_CRC_16)[1])==0)
print('test_CRC : ',test_CRC)

def test_echantillon(mess,G,l,n_essais):
    compteur_detec_erreur=0
    for k in range(n_essais):
        test_mess=mess.copy()
        deb=rd.randint(0,len(mess)-l)
        fin=deb+l-1
        test_mess[deb]=(mess[deb]+1)%2
        test_mess[fin]=(mess[fin]+1)%2
        for j in range(deb+1,fin):
            test_mess[j]=rd.randint(0,1)
        test_CRC_ech=(sum(div_eucl_pol(test_mess,G)[1])==0)
        if not test_CRC_ech :
            compteur_detec_erreur+=1
    return compteur_detec_erreur,compteur_detec_erreur/n_essais
    
#n_detec,prop_detec=test_echantillon(mess_envoye,G_CRC_16,17,100000)
n_detec,prop_detec=test_echantillon(mess_envoye,G_CRC_16,17,10000)
print("n_detec : ",n_detec)
        


    



print(bernard(p, A))