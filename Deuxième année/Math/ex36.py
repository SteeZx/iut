from cmath import pi, sin
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def f(x) : return (x*x)*np.sin(x)+4
def g(x) : return 30/((x*x)+1)

def create_graph(f, g, n, int): 
    array_f, array_g, ln = [], [], np.linspace(0, int, n)
    for i in ln :
        array_f.append(f(i))
        array_g.append(g(i)) 
    fig, ax = plt.subplots()
    ax.set_title("Graph osef tier")
    ax.plot(ln, array_f, color = 'r', linestyle = 'dashed')
    ax.plot(ln, array_g, color = 'g')
    plt.show()
    
# create_graph(f, g, 100, 3*pi)


def proba_remise(n, nbtirage):
    oth = n*nbtirage
    
    blanc, noir, i, counter, tab = 0, 0, 0, 0, []
    
    while i<n:
        i+=nbtirage
        j = 0
        blanc, noir = 0, 0
        while j < nbtirage :
            j += 1
            x = np.random.randint(1, 10)
            if x < 8 : noir +=1
            else : blanc += 1
            tab.append([blanc, noir])
    print(tab)
    for i in tab :
        if i[0] == 2 :
            counter +=1
    # print(counter)
    return (counter/n)*100

print(proba_remise(1000, 4)/40)

def bouleb(n, nb):
    res=np.zeros((n,4))
    count=np.zeros(n)
    for i in range(n):
        res[i]=np.random.randint(1,11,4)
        count[i]=np.count_nonzero(res[i]<8)
    return np.count_nonzero(count==nb)/n
print(bouleb(1000,2))

from scipy.stats import*

def test_binom(taille,n,p):
    valeurs,effectifs=np.unique(binom.rvs(n,p,size=taille), return_counts=True)
    effectifs=effectifs/taille
    plt.bar(valeurs,effectifs)
    x=np.arange(0, n+1)
    plt.vlines(x,0,binom.pmf(x,n,p),color='r')
    plt.show()

test_binom(1000,10,0.3)

