f(x) = λ.-λx pour x >= 0

F(x) [Fonction de répartition](https://fr.wikipedia.org/wiki/Fonction_de_r%C3%A9partition)

Savoir trouver une primitive
f(ax) = a*f'(ax)

λe-λt en primitive on passe en négatif : [-e-λt]x 0 = (-e-λx+1) = 1-e(λ-λx)

E[x] = 1/λ

