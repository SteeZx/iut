create table employe2 as select * from basetd.employe;
create table service2 as select * from basetd.service;
create table projet2 as select * from basetd.projet;
create table travail2 as select * from basetd.travail;
create table concerne2 as select * from basetd.concerne;

alter table employe2 add primary key (nuempl);
alter table service2 add primary key (nuserv);
alter table projet2 add primary key (nuproj);
alter table travail2 add primary key (nuempl, nuproj);
alter table concerne2 add primary key (nuserv, nuproj);

alter table employe2 add foreign key (affect) references service2 (nuserv);
alter table projet2 add foreign key (resp) references employe2 (nuempl);
alter table service2 add foreign key (chef) references  employe2 (nuempl) initially deferred;
alter table travail2 add foreign key (nuempl) references employe2 (nuempl);
alter table travail2 add foreign key (nuproj) references projet2 (nuproj);
alter table concerne2 add foreign key (nuproj) references projet2 (nuproj);
alter table concerne2 add foreign key (nuserv) references service2 (nuserv);

update employe2
    set employe2.salaire = 2500
    where (employe2.nuempl in (select resp from projet2)) and (employe2.salaire < 2500);
    
update employe2
    set employe2.salaire = 2500
    where (employe2.nuempl in (select chef from service2)) and (employe2.salaire < 3500);
    
    
-- ======================= TD 2 ===============
-- Exo 1

CREATE OR REPLACE TRIGGER trigger1_exo1 BEFORE
    UPDATE OF salaire ON employe2 FOR EACH ROW
    BEGIN
    IF :new.salaire < :old.salaire THEN
        RAISE_APPLICATION_ERROR(-20001, 'Viol de la r�gle : Diminution de salaire interdite.');
    END IF;
    END;
/

UPDATE employe2 SET salaire=2400 WHERE nomempl='claude';
UPDATE employe2 SET salaire=2600 WHERE nomempl='claude';

CREATE OR REPLACE TRIGGER trigger2_exo1 BEFORE
    UPDATE OF hebdo ON employe2 FOR EACH ROW
    BEGIN
    IF :new.hebdo > :old.hebdo then
        RAISE_APPLICATION_ERROR(-20002,'Viol de la r�gle : La dur�e hebdo pour un employ� ne peut augmenter');
    END IF;
END;
/

UPDATE employe2 SET hebdo=26 WHERE nomempl='claude';
UPDATE employe2 SET salaire=24 WHERE nomempl='claude';

-- Exo 2

CREATE OR REPLACE TRIGGER trigger1_exo2 AFTER
    DELETE ON employe2
    BEGIN
        DELETE FROM travail2 where nuempl NOT IN (SELECT nuempl FROM employe2);
    END;
/

CREATE OR REPLACE TRIGGER trigger2_exo2 AFTER
    DELETE ON projet2
    BEGIN
        DELETE FROM travail2 where nuempl NOT IN (SELECT resp FROM projet2);
        DELETE FROM concerne2 where nuproj NOT IN (SELECT nuproj FROM projet2);
    END;
/

-- Exo3 Aa

CREATE OR REPLACE TRIGGER trigger1a_exo3 BEFORE
    INSERT OR UPDATE OF duree on travail2
    DECLARE
    REC_employe2 employe2%ROWTYPE;
    BEGIN
        SELECT * INTO REC_employe2 FROM employe2 e WHERE (SELECT SUM(duree) FROM travail2 t WHERE e.nuempl=t.nuempl)> hebdo;
        RAISE_APPLICATION_ERROR(-20003,'Viol de la r�gle : somme des dur�es doit �tre inf�rieur ou �gal � hebdo');
        EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
        WHEN TOO_MANY_ROWS THEN RAISE_APPLICATION_ERROR(-20004,'Viol de la r�gle : Trop de tuples');
    END;
/

-- Exo3 Ab

CREATE OR REPLACE TRIGGER trigger1b_exo3 BEFORE
    INSERT OR UPDATE OF hebdo on employe2
    DECLARE
    REC_employe2 employe2%ROWTYPE;
    BEGIN
        SELECT * INTO REC_employe2 FROM employe2 e WHERE (SELECT SUM(duree) FROM travail2 t WHERE e.nuempl=t.nuempl)> hebdo;
        RAISE_APPLICATION_ERROR(-20003,'Viol de la r�gle : somme des dur�es doit �tre inf�rieur ou �gal � hebdo');
        EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
        WHEN TOO_MANY_ROWS THEN RAISE_APPLICATION_ERROR(-20004,'Viol de la r�gle : Trop de tuples');
    END;
/
--EXO 3 B
CREATE OR REPLACE TRIGGER trigger2_exo3 BEFORE
    INSERT OR UPDATE ON projet2
    DECLARE
    REC_employe2 employe2%ROWTYPE;
    BEGIN
        SELECT * INTO REC_employe2 FROM employe2 e WHERE (SELECT COUNT(p.resp) FROM projet2 p WHERE e.nuempl=p.resp)> 3;
        RAISE_APPLICATION_ERROR(-20005,'Viol de la r�gle : Un employ� ne peut �tre responsable de plus de 3 projets');
        EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
        WHEN TOO_MANY_ROWS THEN RAISE_APPLICATION_ERROR(-20004,'Viol de la r�gle : Trop de tuples');
    END;
/

select * from service2;

UPDATE projet2 SET resp=57 WHERE nuproj = 370;

--EXO 3 C
CREATE OR REPLACE TRIGGER trigger3_exo3 BEFORE
    INSERT OR UPDATE ON concerne2
    DECLARE
    REC_service2 service2%ROWTYPE;
    BEGIN
        SELECT * INTO REC_service2 FROM service2 s WHERE (SELECT COUNT(c.nuproj) FROM concerne2 c WHERE c.nuserv = s.nuserv) > 3;
        RAISE_APPLICATION_ERROR(-20006,'Viol de la r�gle : Un service ne peut �tre concern� par plus de 3 projets');
        EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
        WHEN TOO_MANY_ROWS THEN RAISE_APPLICATION_ERROR(-20004,'Viol de la r�gle : Trop de tuples');
    END;
/

--EXO 3 D

CREATE OR REPLACE TRIGGER trigger4_exo3 BEFORE
    INSERT OR UPDATE ON employe2
    DECLARE
    REC_employe2 employe2%ROWTYPE;
    BEGIN
        SELECT * INTO REC_employe2 FROM employe2 e_a WHERE nuempl IN (SELECT chef FROM service2) and salaire < (select MAX(salaire) from employe2 e_b WHERE e_a.affect = e_b.affect);
        RAISE_APPLICATION_ERROR(-20007, 'Viol de la r�gle : Un employe gagne plus que son chef de service');
        EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
        WHEN TOO_MANY_ROWS THEN RAISE_APPLICATION_ERROR(-20008,'Viol de la r�gle : Trop de personnes gagnent plus que leur chef de service');
    END;
/

-- EXO 3 E

CREATE OR REPLACE TRIGGER trigger5_exo3 AFTER
    INSERT OR UPDATE ON employe2
    DECLARE
    REC_employe2 employe2%ROWTYPE;
    BEGIN
        SELECT * INTO REC_employe2 FROM employe2 e_a WHERE nuempl IN (SELECT chef FROM service2) and salaire < (select MAX(salaire) from employe2 e_b, projet2 p WHERE p.resp = e_b.nuempl);
        RAISE_APPLICATION_ERROR(-20009, 'Viol de la r�gle : Un responsable gagne plus que son chef de service');
        EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
        WHEN TOO_MANY_ROWS THEN RAISE_APPLICATION_ERROR(-20010,'Viol de la r�gle : Trop de responsables gagnent plus que leur chef de service');
    END;
/

-- EXO 3 F

CREATE OR REPLACE TRIGGER trigger6_exo3 AFTER
    INSERT OR UPDATE ON employe2
    DECLARE
    REC_employe2 employe2%ROWTYPE;
    BEGIN
        SELECT * INTO REC_employe2 FROM employe2 e_a WHERE nuempl IN (SELECT chef FROM service2) and salaire < (select MAX(salaire) from employe2 e_b, projet2 p WHERE p.resp = e_b.nuempl OR e_a.affect = e_b.affect);
        RAISE_APPLICATION_ERROR(-20009, 'Viol de la r�gle : Un responsable gagne plus que son chef de service');
        EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
        WHEN TOO_MANY_ROWS THEN RAISE_APPLICATION_ERROR(-20010,'Viol de la r�gle : Trop de responsables gagnent plus que leur chef de service');
    END;
/

-- EXO 4
CREATE TABLE employe_alerte as select * from employe2;
DELETE FROM employe_alerte;

CREATE OR REPLACE TRIGGER trigger1_exo4 AFTER
INSERT OR UPDATE OF salaire ON employe2
DECLARE 
REC_employe2 employe2%ROWTYPE
BEGIN
    
    INSERT INTO employe_alerte
    VALUES(:new.nuempl, :new.nomempl, :new.hebdo, :new.affect, :new.salaire);
    END;