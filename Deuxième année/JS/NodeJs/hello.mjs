"use strict"

const g = 10

const etu1 = {
    nom : "Jojo",
    age : "Retard"
}

const etu2 = {
    nom : "Jojo",
    age : "Retardee"
}

const etu3 = {
    nom : "Jojo",
    age : "Retardeee"
}

const Classe = {
    nom: "Totor",
    etudiants: [etu1, etu2, etu3],
}

export default Classe