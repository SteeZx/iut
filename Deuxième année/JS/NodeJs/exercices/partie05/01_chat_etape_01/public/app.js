"use strict"
const socket = io();

const userlist = document.querySelector("#active_users_list");
const  roomlist = document.querySelector("#active_rooms_list");
const message = document.querySelector("#messageInput");
const sendMessageBtn = document.querySelector("#send_message_btn");
const  roomInput = document.querySelector("#roomInput");
const createRoomBtn = document.querySelector("#room_add_icon_holder");
const chatDisplay = document.querySelector("#chat");

let currentRoom = "global";
let  myUsername = "";

// Prompt for username on connecting to server
socket.on("connect",  () => {
  myUsername = prompt("Enter name: ");
  socket.emit("createUser", myUsername);
});

// Send message on button click
sendMessageBtn.addEventListener("click",  () => {
  socket.emit("sendMessage", message.value);
  message.value = "";
});

// Send message on enter key press
message.addEventListener("keyup",  (event)=> {
  if (event.key === "Enter") {
    sendMessageBtn.click();
  }
});

// Create new room on button click
createRoomBtn.addEventListener("click",  () => {
  // socket.emit("createRoom", prompt("Enter new room: "));
  let roomName = roomInput.value.trim();
  if (roomName !== "") {
    socket.emit("createRoom", roomName);
    roomInput.value = "";
  }
});

socket.on("updateChat",  (username, data) => {
  if (username === "INFO") {
    console.log("Displaying announcement");
    chatDisplay.innerHTML += `<div class="announcement"><span>${data}</span></div>`;
  } else {
    console.log("Displaying user message");
    chatDisplay.innerHTML += `<div class="message_holder ${
      username === myUsername ? "me" : ""
    }">
                                <div class="pic"></div>
                                <div class="message_box">
                                  <div id="message" class="message">
                                    <span class="message_name">${username}</span>
                                    <span class="message_text">${data}</span>
                                  </div>
                                </div>
                              </div>`;
  }

  chatDisplay.scrollTop = chatDisplay.scrollHeight;
});

socket.on("updateUsers", function (usernames) {
  userlist.innerHTML = "";
  console.log("usernames returned from server", usernames);
  for (let user in usernames) {
    userlist.innerHTML += `<div class="user_card">
                              <div class="pic"></div>
                              <span>${user}</span>
                            </div>`;
  }
});

socket.on("updateRooms", (rooms, newRoom) => {
  roomlist.innerHTML = "";

  for (let index in rooms) {
    roomlist.innerHTML += `<div class="room_card" id="${rooms[index].name}"
                                onclick="changeRoom('${rooms[index].name}')">
                                <div class="room_item_content">
                                    <div class="pic"></div>
                                    <div class="roomInfo">
                                    <span class="room_name">#${rooms[index].name}</span>
                                    <span class="room_author">${rooms[index].creator}</span>
                                    </div>
                                </div>
                            </div>`;
  }

  document.querySelector('#'+currentRoom).classList.add("active_item");
});

const  changeRoom = (room) =>  {
  if (room != currentRoom) {
    socket.emit("updateRooms", room);
    document.querySelector('#'+currentRoom).classList.remove("active_item");
    currentRoom = room;
    document.querySelector('#'+currentRoom).classList.add("active_item");
  }
}
