"use strict"
import assert from 'node:assert'
import {restaurantBDDao} from "./dao/restaurantBDDao.mjs";
import {userBDDao} from "./dao/UserBDDao.mjs";

import Restaurant from "./model/restaurant.mjs"
import User from "./model/user.mjs";

await restaurantBDDao.deleteAll()
await userBDDao.deleteAll()
let restaurants = await  restaurantBDDao.findAll()
assert.deepStrictEqual(restaurants,[])
let users = await  userBDDao.findAll()
assert.deepStrictEqual(users,[])

let userToAdd =  new User({login:"jojo", password:"p"})
let userAdded = await userBDDao.save(userToAdd)



assert.deepStrictEqual(userToAdd,userAdded)

let restauranttoAdd = new Restaurant({
    nomDeLOffreTouristique : "LA GALETTE BRETONNE",
    typeDeRestaurant : "Restaurant",
    categorieDuRestaurant : "Restaurant gastronomique - cuisin",
    adresse1 : "",
    adresse2 : "",
    adresse3 : "",
    codePostal  : "44 770",
    latitude    : 47.137,
    longitude   : -2.190
})

users = userBDDao.addRestaurant(userAdded, restauranttoAdd)
