'use strict'

export default class Restaurant {
    nomDeLOffreTouristique
    typeDeRestaurant
    categorieDuRestaurant
    adresse1
    adresse2
    adresse3
    codePostal
    latitude
    longitude
    constructor(obj) {
        Object.assign(this,obj)
    }

}