'use strict'
import { PrismaClient } from '@prisma/client'
let prisma = new PrismaClient()


import Restaurant from "../model/restaurant.mjs";

export const restaurantBDDao = {
    save : async (restaurant) => {
        try {
            const res =  await prisma.restaurant.create({data:restaurant})
            return new Restaurant(res)
        } catch (e) {
            return Promise.reject(e)
        }
    },
    deleteAll : async () => {
        try {
            return await prisma.restaurant.deleteMany({})
        } catch (e) {
            return Promise.reject(e)
        }
    },
    findAll : async () => {
        try {
            return (await prisma.restaurant.findMany({})).map(onj => new Restaurant(obj))
        } catch (e) {
            return Promise.reject(e)
        }
    }
}