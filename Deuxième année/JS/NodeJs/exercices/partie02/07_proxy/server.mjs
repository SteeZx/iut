'use strict'

import Hapi from '@hapi/hapi'
import Joi from 'joi'


import {parkingController} from "./controller/parkingController.mjs";



const routes =[
    {
        method: '*',
        path: '/{any*}',
        handler: function (request, h) {
            return h.response({message: "not found"}).code(404)
        }
    },
    {
        method: 'GET',
        path: '/parking',

        handler: async (request, h) => {

            try {
                const  parking = await parkingController.findAll()
                return h.response(parking).code(200)
            } catch (e) {
                return h.response(e).code(400)
            }
        }
    },
    {
        method: 'GET',
        options: {
            validate : {
                params: Joi.object({
                    latitude : Joi.number(),
                    longitude: Joi.number()
                })
            }
        },
        path: '/parking/distance/{latitude}/{longitude}',

        handler: async (request, h) => {

            try {
                const latitude = request.params.latitude
                const longitude = request.params.longitude

                const  parking = await parkingController.findByCoordinate(latitude,longitude)
                return h.response(parking).code(200)
            } catch (e) {
                return h.response(e).code(400)
            }
        }
    }

]

const server = Hapi.server({
    port: 3000,
    host: 'localhost'
});

server.route(routes);

export const init = async () => {

    await server.initialize();
    return server;
};

export  const start = async () => {
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    return server;
};


process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});


