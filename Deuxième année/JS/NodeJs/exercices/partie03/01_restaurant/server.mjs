'use strict'

import Hapi from '@hapi/hapi'
import Joi from'joi'
import * as dotenv from 'dotenv'
dotenv.config()
const POPULATE_BD = Boolean(process.env.POPULATE_BD)
const POPULATE_FILE = process.env.POPULATE_FILE


import {restaurantController} from './controller/restaurantController.mjs'
const routes =[
    {
        method: '*',
        path: '/{any*}',
        handler: (request, h) => {
            return h.response({message: "not found"}).code(404)}
    },{
        method: 'POST',
        path: "/restaurant/populate",
        options: {
            validate: {
                payload: Joi.string().required()
            }
        },
        handler: async (request, h) => {
            try {
                const fileName = request.payload
                const populate = await restaurantController.populate(fileName)
            } catch (e) {
                return h.response({message: "error"}).code(400)
            }
        }

    },
    {
        method: 'GET',
        path: "/restaurant",
        handler: async (request, h) => {
            try {
                const restaurants = await restaurantController.findAll()
                return h.response(restaurants).code(200)
            } catch (e) {
                return h.response({message: "error"}).code(400)
            }
        }
    },
    {
        method: 'GET',
        path: "/restaurant/{nomDeLOffreTouristique}",
        options: {
            validate: {
                params : Joi.object({
                    nomDeLOffreTouristique : Joi.string().required()
                })
            }
        },
        handler: async (request, h) => {
            try {
                const nomDeLOffreTouristique = request.params.nomDeLOffreTouristique

                const restaurant = await restaurantController.findByNomDeLOffreTouristique(nomDeLOffreTouristique)
                if (restaurant == null)
                    return h.response({message: "not found"}).code(404)
                return h.response(restaurant).code(200)
            } catch (e) {
                return h.response({message: "error"}).code(400)
            }
        }
    },
    {
        method: 'GET',
        path: "/restaurant/parking/{nomDeLOffreTouristique}",
        options: {
            validate: {
                params : Joi.object({
                    nomDeLOffreTouristique : Joi.string().required()
                })
            }
        },
        handler: async (request, h) => {
            try {
                const nomDeLOffreTouristique = request.params.nomDeLOffreTouristique

                const parkings = await restaurantController.findParkingsByNomDeLOffreTouristique(nomDeLOffreTouristique)
                return h.response(parkings).code(200)
            } catch (e) {
                return h.response({message: "error"}).code(400)
            }
        }
    },
    {
        method: 'GET',
        path: "/restaurant/distance/{identifiantParking}",
        options: {
            validate: {
                params : Joi.object({
                    identifiantParking : Joi.string().required()
                })
            }
        },
        handler: async (request, h) => {
            try {
                const identifiantParking = request.params.identifiantParking
                const restaurants = await restaurantController.findRestaurantsByParkingId(identifiantParking)
                return h.response(restaurants).code(200)
            } catch (e) {
                return h.response({message: "error"}).code(400)
            }
        }
    }

    ]
const server = Hapi.server({
    port: 3001,
    host: 'localhost'
});

server.route(routes);

export const init = async () => {

    await server.initialize();
    return server;
};

export  const start = async () => {
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);

    if (POPULATE_BD==true) {
        await restaurantController.populate(POPULATE_FILE)
    }
    return server;
};


process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});


