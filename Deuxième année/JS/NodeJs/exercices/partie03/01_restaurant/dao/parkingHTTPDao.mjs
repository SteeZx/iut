'use strict'
//acces au .env
import * as dotenv from 'dotenv'
dotenv.config()
const PARKING_BASE_URL = process.env. PARKING_BASE_URL

import fetch from 'node-fetch';

//ici plus de proxy
//pour pouvoir consulter un site avec un certificat invalide
//process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

import Parking from '../model/parking.mjs'

export const parkingHTTPDao = {
    //parking
    findParkingsByCoordinates : async (latitude,longitude) => {
        try {
            const url = PARKING_BASE_URL+"/distance/"+latitude+"/"+longitude
            let response = await fetch(url)
            let data = await response.json()
            const parkings = data.map(record => new Parking(record))
          return parkings

        } catch (e) {
            console.log(e)
            return Promise.reject(e)
        }
    },
    findParkingsById : async (parkingId) => {
        try {
            const url = PARKING_BASE_URL
            let response = await fetch(url)
            let data = await response.json()
            const parking = data.map(record => new Parking(record)).find(e => e.identifiant == parkingId)
            return parking
        } catch (e) {
            console.log(e)
            return Promise.reject(e)
        }
    }
}