'use strict'
import {restaurantBDDao} from "../dao/restaurantBDDao.mjs";
export const restaurantController = {
    populate : async (fileName) => {
        try {
            return await restaurantBDDao.populate(fileName)
        } catch (e) {return Promise.reject({message : "error"})}
    },
    findAll : async () => {
        try {
            return await restaurantBDDao.findAll()
        } catch (e) {return Promise.reject({message : "error"})}
    },
    findByNomDeLOffreTouristique : async (nomDeLOffreTouristique) => {
        try {
            return await restaurantBDDao.findByNomDeLOffreTouristique(nomDeLOffreTouristique)
        } catch (e) {
            return Promise.reject({message : "error"})}
    },
    findParkingsByNomDeLOffreTouristique : async (nomDeLOffreTouristique) => {
        try {
            return await restaurantBDDao.findParkingsByNomDeLOffreTouristique(nomDeLOffreTouristique)
        } catch (e) {
            return Promise.reject({message : "error"})}
    },
    findRestaurantsByParkingId : async (identifiantParking) => {
        try {
            const restaurants =  await restaurantBDDao.findRestaurantsByParkingId(identifiantParking)
            return  restaurants
        } catch (e) {
            return Promise.reject({message : "error"})}
    },


}