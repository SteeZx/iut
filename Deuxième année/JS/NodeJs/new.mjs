import Classe from "./hello.mjs";

Classe.ajout = function(etu){
    this.etudiants.push(etu)
}

Classe.delete = function(){
    this.etudiants = []
}

const etu4 = {
    nom : "Ewen",
    age : "Retard"
}

Classe.ajout(etu4)


const Animal = function (nom) {
    this.nom = nom
}

Animal.prototype.crie = function () {
    return this.nom + " crie"
}

const Chien = function (nom) {
    Animal.call(this, nom)
}

Chien.prototype = Object.create(Animal.prototype)
Chien.prototype.constructor = Chien

Chien.prototype.crie = function () {
    return Animal.prototype.crie.call((this)+" wawa")
}