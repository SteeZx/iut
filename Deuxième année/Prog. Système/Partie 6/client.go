package main

import (
	"bufio"
	"log"
	"net"
)

func main() {

	conn, err := net.Dial("unix", "test.sock")
	if err != nil {
		log.Println("Dial error:", err)
		return
	}
	// _, err = conn.Write([]byte("Hello"))
	write := bufio.NewWriter(conn)
	write.Flush()
	write.WriteString("Test \n")
	// if err != nil {
	// 	log.Println("NTM", err)
	// }
	defer conn.Close()

	log.Println("Je suis connecté")

}
