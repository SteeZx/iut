package main

import (
	"bufio"
	"log"
	"net"
	"time"
)

func main() {
	listener, err := net.Listen("unix", "test.sock")
	if err != nil {
		log.Println("listen error:", err)
		return
	}
	defer listener.Close()

	conn, err := listener.Accept()
	if err != nil {
		log.Println("accept error:", err)
		return
	}
	read := bufio.NewReader(conn)
	rd, _ := read.ReadString('\n')
	defer conn.Close()

	log.Println("Le client s'est connecté")
	log.Println(string(rd))
	time.Sleep(10 * time.Second)

}
