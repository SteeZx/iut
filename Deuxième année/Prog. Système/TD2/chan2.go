package main

import "fmt"

var c1 chan bool = make(chan bool)
var c2 chan bool = make(chan bool)

func routine1() {
	for {
		<-c1
		fmt.Println("Routine 1")
		c2 <- false
	}

}

func routine2() {
	for {
		<-c2
		fmt.Println("Routine 2")
		c1 <- true
	}
}

func main() {

	go routine1()
	c1 <- true
	routine2()
}
