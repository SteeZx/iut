package main

import (
	"log"
	"sync"
)

var com1, com2 []int
var w sync.WaitGroup
var s1 sync.Mutex
var s2 sync.Mutex
var c1 = make(chan int, 1000)
var c2 = make(chan int, 1000)

func prod(n int) {
	defer w.Done()
	
	for i := 0; i < n; i++ {
		// s1.Lock()
		c1 <- i
		// s1.Unlock()
	}
}

func trans() {
	defer w.Done()
	for {
		// s1.Lock()
		if len(c1) > 0 {
			// s2.Lock()
			c2 <- <- c1
			// s2.Lock()
		}
		// s1.Unlock()
	}
}

func cons() {
	defer w.Done()
	for {
		// if len(c2) > 0 {
		log.Print(<- c2)
		// }
	}
}

func main() {
	w.Add(3)
	go prod(1000)
	go trans()
	go cons()
	w.Wait()
}
