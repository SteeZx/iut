package Application.DAO

import Application.Classes.Employe
import Application.Classes.Service
import BD.SessionOracle
import oracle.jdbc.OracleTypes
import java.sql.CallableStatement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

class DAOLecture(ss : SessionOracle) {
    var session: SessionOracle? = null
    init {
        this.session=ss
    }

    fun list_empl(){
        var conn = session?.getConnectionOracle()

        var requete = "call ASSIETTE.liste_empl(?)"

        try {
            var callstmt : CallableStatement = conn.prepareCall(requete)
            callstmt.registerOutParameter(1, OracleTypes.CURSOR)
            callstmt.execute()
            var result = callstmt.getObject(1) as ResultSet
        }catch (e : SQLException){
            e.errorCode
            e.message
        }
    }

    fun list_projet(e: Employe){
        var conn = session?.getConnectionOracle()
        var nuempl = e.getNuempl()
        var requete = "call ASSIETTE."
        try {

        }catch (e: SQLException){
            e.errorCode
            e.message
        }
    }


    fun listeEmploye(){
        val conn: Connection?
        conn = session?.getConnectionOracle()

        val requete  = "call LECTURE.LISTE_EMPLOYES(?)"

        try{
            println("Liste Employees")
            val callstmt: CallableStatement = conn!!.prepareCall(requete)
            callstmt.registerOutParameter(1,OracleTypes.CURSOR)
            callstmt.execute()
            val result : ResultSet = callstmt.getObject(1) as ResultSet

            while(result.next()){
                var nuempl = result.getInt("nuempl")
                var nomempl = result.getString("nomempl")
                println("$nuempl, $nomempl")
            }


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }

    }

    fun listeProjet(e: Employe){
        val conn: Connection?
        conn = session?.getConnectionOracle()

        val requete  = "call LECTURE.LISTE_PROJET(?,?)"
        val nuempl = e.getNuempl()

        try{
            println("Liste Projets")
            val callstmt: CallableStatement = conn!!.prepareCall(requete)
            callstmt.setObject(1,nuempl)
            callstmt.registerOutParameter(2,OracleTypes.CURSOR)
            callstmt.execute()
            val result : ResultSet = callstmt.getObject(2) as ResultSet

            while (result.next()){
                var nomproj = result.getString("nomproj")
                var nomempl = result.getString("nomempl")
                var duree = result.getInt("duree")
                println("$nomproj ,$nomempl, $duree")
            }


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }

    }

    fun listeService(s : Service){
        val conn: Connection?
        conn = session?.getConnectionOracle()

        val requete  = "call LECTURE.LISTE_SERVICE(?,?)"

        val nuserv = s.getNuserv()

        try{
            println("Liste Service")
            val callstmt: CallableStatement = conn!!.prepareCall(requete)
            callstmt.setObject(1,nuserv)
            callstmt.registerOutParameter(2,OracleTypes.CURSOR)
            callstmt.execute()
            val result : ResultSet = callstmt.getObject(2) as ResultSet

            while (result.next()){
                var nomproj = result.getString("nomproj")
                var nomserv = result.getString("nomserv")
                println("$nomproj ,$nomserv")
            }


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }
    }
}