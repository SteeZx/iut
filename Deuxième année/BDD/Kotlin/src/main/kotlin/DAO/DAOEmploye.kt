package DAO

import Application.Classes.Employe
import BD.SessionOracle
import oracle.jdbc.proxy.annotation.Pre
import java.sql.Connection
import java.sql.*

class DAOEmploye(ss: SessionOracle) {
    var session: SessionOracle? = null
    init {
        this.session=ss
    }

    fun read(){


        //var essai = SessionOracle();
        val conn: Connection?
        conn= session?.getConnectionOracle()
        val requete ="SELECT * FROM employe"
        try {
            val stmt: Statement = conn!!.createStatement()// Création d'une requete de type Statemen
            val result: ResultSet= stmt.executeQuery(requete) //Le contenu du select est dans ResultSet

            /* Parcourir le résultat du select avec la fonction next();*/
            while (result.next()) {

                // getting the value of the id column
                val id = result.getInt("nuempl")
                val nom=result.getString("nomempl")
                println("$id $nom")

            }
            println("-----------------------")
            result.close()
        }

        catch(e: SQLException){
            println(e.errorCode)//numéro d'erreur
            println(e.message)// message d'erreur qui provient d'oracle, trigger ou procédure
        }
    }

    fun creer_employe(e: Employe){
        val nuempl : Int = e.getNuempl()
        val nom  :String = e.getNomempl()
        val hebdo :Int = e.getHebdo()
        val affect :Int = e.getAffect()
        val salaire :Int = e.getSalaire()
        val conn: Connection?
        conn = session?.getConnectionOracle()
        val requete: String = "INSERT into employe values( ? , ? , ? , ? , ? )"

        try{
            println("INSERT")
            val pstmt: PreparedStatement = conn!!.prepareStatement(requete)
            /*val stmt: Statement = conn!!.createStatement()*/
            pstmt.setInt(1,nuempl)
            pstmt.setString(2,nom)
            pstmt.setInt(3, hebdo)
            pstmt.setInt(4, affect)
            pstmt.setInt(5,salaire)

            val result: Int = pstmt.executeUpdate()
            print("$pstmt")


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }
    }

    fun updateEmploye(e: Employe) {
        val nuempl : Int = e.getNuempl()
        val nom  :String = e.getNomempl()
        val hebdo :Int = e.getHebdo()
        val affect :Int = e.getAffect()
        val salaire :Int = e.getSalaire()
        val conn: Connection?
        conn = session?.getConnectionOracle()

    val requete  = "UPDATE employe SET nomempl = ?, hebdo = ?, affect = ? , salaire = ? where nuempl = ?"

        try{
            println("UPDATE")
            val pstmt: PreparedStatement = conn!!.prepareStatement(requete)
            /*val stmt: Statement = conn!!.createStatement()*/
            pstmt.setString(1,nom)
            pstmt.setInt(2,hebdo)
            pstmt.setInt(3,affect)
            pstmt.setInt(4,salaire)
            pstmt.setInt(5,nuempl)
            pstmt.executeUpdate()
            print("$pstmt")


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }

    }

    fun supprimerEmploye(e: Employe) {
        val nuempl : Int = e.getNuempl()
        val conn: Connection?
        conn = session?.getConnectionOracle()
        val requete: String = "DELETE FROM employe where nuempl = ?"

        try{
            println("DELETE")
            val pstmt : PreparedStatement = conn!!.prepareStatement(requete)
            /*val stmt: Statement = conn!!.createStatement()*/
            pstmt.setInt(1,nuempl)
            val result: Int= pstmt.executeUpdate()
            print("$pstmt")

        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }
    }


}