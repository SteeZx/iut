package Application.DAO

import Application.Classes.Employe
import Application.Classes.Projet
import Application.Classes.Travail
import BD.SessionOracle
import java.sql.*

class DAOMAJ(ss: SessionOracle) {
    var session: SessionOracle? = null
    init {
        this.session=ss
    }

    fun creer_employe(e: Employe) {
        val nuempl : Int = e.getNuempl()
        val nom  :String = e.getNomempl()
        val hebdo :Int = e.getHebdo()
        val affect :Int = e.getAffect()
        val salaire :Int = e.getSalaire()
        val conn: Connection?
        conn = session?.getConnectionOracle()

        val requete  = "call MAJ.CREER_EMPLOYE(?, ?, ?, ?, ?)"

        try{
            println("UPDATE")
            val pstmt: CallableStatement = conn!!.prepareCall(requete)
            /*val stmt: Statement = conn!!.createStatement()*/
            pstmt.setInt(1,nuempl)
            pstmt.setString(2,nom)
            pstmt.setInt(3,hebdo)
            pstmt.setInt(4,affect)
            pstmt.setInt(5,salaire)
            pstmt.executeUpdate()
            print("$pstmt")


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }

    }

    fun supprimerEmploye(e: Employe) {
        val nuempl : Int = e.getNuempl()
        val conn: Connection?
        conn = session?.getConnectionOracle()
        val requete: String = "call MAJ.SUPPRIMER_EMPLOYE(?)"

        try{
            val callstmt : CallableStatement = conn!!.prepareCall(requete)
            /*val stmt: Statement = conn!!.createStatement()*/
            callstmt.setInt(1,nuempl)
            val result: Int= callstmt.executeUpdate()
            print("$callstmt")

        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }
    }

    fun modification_hebdo(e: Employe) {
        val nuempl : Int = e.getNuempl()
        val hebdo :Int = e.getHebdo()
        val conn: Connection?
        conn = session?.getConnectionOracle()

        val requete  = "call MAJ.MODIFICATION_HEBDO(?, ?)"

        try{
            val callstmt: CallableStatement = conn!!.prepareCall(requete)
            /*val stmt: Statement = conn!!.createStatement()*/
            callstmt.setInt(1,nuempl)
            callstmt.setInt(2,hebdo)
            callstmt.executeUpdate()
            print("$callstmt")


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }

    }

    fun modification_salaire(e: Employe) {
        val nuempl : Int = e.getNuempl()
        val salaire :Int = e.getSalaire()
        val conn: Connection?
        conn = session?.getConnectionOracle()

        val requete  = "call MAJ.MODIFICATION_SALAIRE(?, ?)"

        try{
            val callstmt: CallableStatement = conn!!.prepareCall(requete)
            /*val stmt: Statement = conn!!.createStatement()*/
            callstmt.setInt(1,nuempl)
            callstmt.setInt(2,salaire)
            callstmt.executeUpdate()
            print("$callstmt")


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }

    }

    fun modification_duree(e: Employe, p : Projet, t : Travail) {
        val nuempl : Int = e.getNuempl()
        val nuproj = p.getNuproj()
        val duree = t.getDuree()
        val conn: Connection?
        conn = session?.getConnectionOracle()

        val requete  = "call MAJ.MODIFICATION_DUREE(?, ?, ?)"

        try{
            val callstmt: CallableStatement = conn!!.prepareCall(requete)
            /*val stmt: Statement = conn!!.createStatement()*/
            callstmt.setInt(1,nuempl)
            callstmt.setInt(2,nuproj)
            callstmt.setInt(3,duree)
            callstmt.executeUpdate()
            print("$callstmt")


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }

    }

    fun insertion_travail(e: Employe, p : Projet, t : Travail) {
        val nuempl : Int = e.getNuempl()
        val nuproj = p.getNuproj()
        val duree = t.getDuree()
        val conn: Connection?
        conn = session?.getConnectionOracle()

        val requete  = "call MAJ.INSERTION_TRAVAIl(?, ?, ?)"

        try{
            val callstmt: CallableStatement = conn!!.prepareCall(requete)
            /*val stmt: Statement = conn!!.createStatement()*/
            callstmt.setInt(1,nuempl)
            callstmt.setInt(2,nuproj)
            callstmt.setInt(3,duree)
            callstmt.executeUpdate()
            print("$callstmt")


        }catch (e : SQLException){
            println(e.errorCode)
            println(e.message)
        }

    }


}