package Application.Classes

class Travail(nuempl : Int, nuproj : Int, duree : Int) {
    private var nuempl : Int
    private var nuproj : Int
    private var duree : Int

    init {
        this.nuempl = nuempl
        this.nuproj = nuproj
        this.duree = duree
    }

    fun getNuempl(): Int = this.nuempl

    fun getNuproj(): Int = this.nuproj

    fun getDuree(): Int = this.duree

    fun setNuempl(nuempl: Int){this.nuempl = nuempl}

    fun setNuproj(nuproj: Int){this.nuproj = nuproj}

    fun setDuree(duree: Int){this.duree = duree}
}