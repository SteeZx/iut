package Application.Classes

class Projet(nuproj : Int, nomproj : String, resp : Int) {
    private var nuproj : Int
    private var nomproj : String
    private var resp : Int

    init {
        this.nuproj = nuproj
        this.nomproj = nomproj
        this.resp = resp
    }

    fun getNuproj(): Int = this.nuproj
    fun getNomproj(): String = this.nomproj
    fun getResp(): Int = this.resp

    fun setNuproj(nuproj : Int) {
        this.nuproj = nuproj
    }

    fun setNomproj(nomproj : String) {
        this.nomproj = nomproj
    }

    fun setResp(resp : Int) {
        this.resp = resp
    }
}