package Application.Classes

import java.util.StringJoiner

class Service(nuserv : Int, nomserv : String, chef : Int) {

    private var nuserv : Int
    private var nomserv : String
    private var chef : Int

    init{
        this.nuserv = nuserv
        this.nomserv = nomserv
        this.chef = chef
    }

    fun getNuserv(): Int = nuserv

    fun getNomserv(): String = nomserv

    fun getChef(): Int = chef

    fun setNuserv(nuserv : Int){this.nuserv = nuserv}

    fun setNomserv(nomserv: String){this.nomserv = nomserv}

    fun setChef(chef : Int){this.chef = chef}
}