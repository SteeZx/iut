package Application.Classes

class Employe(nuempl : Int, nomempl : String, hebdo : Int, affect : Int, salaire : Int ) {

    private var nuempl : Int
    private var nomempl : String
    private var hebdo : Int
    private var affect : Int
    private var salaire : Int

    init{
        this.nuempl = nuempl
        this.nomempl = nomempl
        this.hebdo = hebdo
        this.affect = affect
        this.salaire = salaire
    }

    fun getNuempl(): Int = this.nuempl

    fun getNomempl(): String = this.nomempl

    fun getHebdo(): Int = this.hebdo

    fun getAffect(): Int = this.affect

    fun getSalaire(): Int = this.salaire

    fun setNuempl(nuempl : Int) { this.nuempl = nuempl}

    fun setNomempl(nomempl: String) { this.nomempl = nomempl}

    fun setHebdo(hebdo: Int) { this.hebdo = hebdo}

    fun setAffect(affect: Int) { this.affect = affect}

    fun setSalaire(salaire: Int) { this.salaire = salaire}

    override fun toString(): String {
        return "N°${this.nuempl}, Nom : ${this.nomempl}, Heures Hebdo : ${this.hebdo}, Affectation : ${this.affect}, Salaire : ${this.salaire}"
    }
}