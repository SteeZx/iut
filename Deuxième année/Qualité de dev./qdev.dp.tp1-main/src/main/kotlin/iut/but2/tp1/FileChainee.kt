package iut.but2.tp1

class FileChainee<E> : File<E> {

    private var debut : Cellule<E>? = null
    private var fin : Cellule<E>? = null

    init {
    }

    override fun insererEnQueue(element: E) {
        var cell = Cellule(element)
        if (debut == null) debut = cell
        else fin?.modifieSuivant(cell)
        fin = cell
    }

    override fun supprimerEnTete() {
        debut = debut?.suivant()
    }

    override fun listerDepuisDebut() : MutableList<E>  {
        var debutSave = debut

        var list : MutableList<E> = mutableListOf()
        if (debut == null) list
        else {
            while (debut != null) {
                list.add(debut!!.valeur())
                debut = debut!!.suivant()
            }
        }
        debut = debutSave
        return list
    }

    override fun taille(): Int {
        var count = 0
        var debutSave = debut

        if (debut!!.valeur() == null) return 0

        while (debut != null){
            count++
            debut = debut!!.suivant()
        }
        return count
    }

    override fun consulter(position: Int): E {
        if (position >= taille()) throw IndexOutOfBoundsException("Position trop grande par rapport à la valeure max")
         var debutSave = debut
        for (i in 0 until position) debut!!.suivant()
        var retour = debut!!.valeur()
        debut = debutSave
        return retour
    }

    override fun listerDepuisFin() : MutableList<E> = listerDepuisDebut().asReversed()

}