CREATE TABLE distribution
    AS
        SELECT
            *
        FROM
            basetd.distribution;

CREATE TABLE departement
    AS
        SELECT
            *
        FROM
            basetd.departement;

CREATE TABLE operateur
    AS
        SELECT
            *
        FROM
            basetd.operateur;

CREATE TABLE commune
    AS
        SELECT
            *
        FROM
            basetd.commune;
            
            

CREATE OR REPLACE VIEW VueCarquefou AS
    SELECT
        co.nom_commune,
        co.code_insee,
        op.nomfo,
        op.generation,
        op.technologie,
        de.nomdep,
        de.code_departement,
        di.adresse
    FROM
        commune        co,
        departement    de,
        operateur      op,
        distribution   di
    WHERE
        op.numfo = di.numfo
        AND co.code_insee = di.code_insee
        AND co.nomdep = de.nomdep
        AND op.generation = '5G'
        AND co.nom_commune = 'Carquefou';
        
SELECT COUNT(*) as "Nombre Antenne" FROM VueCarquefou;

SELECT vc.nomfo, COUNT(*) "Antennes" FROM VueCarquefou vc GROUP BY vc.nomfo;

CREATE OR REPLACE VIEW CINQVILLESANT AS
    SELECT
        co.nom_commune,
        COUNT(op.generation) "Nombre d'Antennes"
    FROM
        commune        co,
        departement    de,
        operateur      op,
        distribution   di
    WHERE
        op.numfo = di.numfo
        AND co.code_insee = di.code_insee
        AND co.nomdep = de.nomdep
        AND generation = '5G'
    GROUP BY
        co.nom_commune
    ORDER BY
        2 DESC,
        1
    FETCH NEXT 10 ROWS ONLY;
    
SELECT * FROM CINQVILLESANT
ORDER BY 2 ASC;

CREATE OR REPLACE VIEW nombre5g AS
    SELECT
        co.code_insee,
        co.nom_commune,
        (
            SELECT
                COUNT(*)
            FROM
                distribution   di,
                operateur      op
            WHERE
                op.numfo = di.numfo
                AND co.code_insee = di.code_insee
                AND op.generation = '5G'
        ) nb5g
    FROM
        commune co
    WHERE
        co.nomdep = 'Loire-Atlantique'
    ORDER BY
        2 DESC;
        
CREATE OR REPLACE VIEW nombre4g AS
    SELECT
        co.code_insee,
        co.nom_commune,
        (
            SELECT
                COUNT(*)
            FROM
                distribution   di,
                operateur      op
            WHERE
                op.numfo = di.numfo
                AND co.code_insee = di.code_insee
                AND op.generation = '4G'
        ) nb4g
    FROM
        commune co
    WHERE
        co.nomdep = 'Loire-Atlantique'
    ORDER BY
        2 DESC;


SELECT nombre4g.nom_commune, nb5g, nb4g FROM nombre5g, nombre4g 
WHERE nombre5g.code_insee = nombre4g.code_insee
FETCH NEXT 10 ROWS ONLY;

select * from nombre4g;

SELECT nb4g FROM nombre4g;



CREATE OR REPLACE VIEW CommuneLA AS 
    SELECT co.*
    FROM
        commune co,
        departement de
    WHERE co.nomdep=de.nomdep
    AND de.nomdep = 'Loire-Atlantique';
    
ALTER TABLE commune ADD CONSTRAINT Pk_insee PRIMARY KEY (code_insee);
ALTER TABLE commune ADD CONSTRAINT Fk_nomdep FOREIGN KEY (nomdep) REFERENCES departement(nomdep);
    
ALTER TABLE departement ADD CONSTRAINT Pk_nomdep PRIMARY KEY (nomdep);

ALTER TABLE departement DROP PRIMARY KEY;
    
insert into CommuneLA values(44999,'fontome2','Loire-Atlantique');

CREATE OR REPLACE VIEW DistributionLA AS 
    SELECT di.*
    FROM
        distribution di,
        communeLA
    WHERE co.nomdep=de.nomdep
    AND co.code_insee = di.code_insee
    AND de.nomdep = 'Loire-Atlantique';



SELECT DISTINCT * FROM distributionLA; 
SELECT * FROM distribution;





























DROP TABLE commune;
DROP TABLE distribution; 
DROP TABLE operateur;
DROP TABLE departement;
