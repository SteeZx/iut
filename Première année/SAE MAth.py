import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def dist_point(p1, p2):
    result = 0
    for i in range(len(p1)):
        result += (p1[i]-p2[i])**2
    return np.sqrt(result)


def moy_point(points):
    result = [0 for _ in range(len(points[0]))]
    for i in range(len(points)):
        for j in range(len(points[0])):
            result[j] += points[i][j]
    return [k/len(points) for k in result]

def MethodAExample(Title):

    K, N = [[] for _ in range(4)], 200
    pandidou = pd.read_csv("test_data/data0.csv", sep=",", decimal=".")
    data = [[pandidou["0"][i], pandidou["1"][i]] for i in range(len(pandidou["0"]))]
    fig, ax = plt.subplots()

    for i in MethodA(data, K, N)[0]:
        X, Y = [j[0] for j in i], [j[1] for j in i]
        ax.scatter(X, Y, linewidth = 2.0)

    plt.show()

    plt.savefig(Title + ".pdf")