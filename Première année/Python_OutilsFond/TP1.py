from math import sqrt

def racines_polynome2(a: float, b: float, c: float) -> tuple:
    if a!= 0 :
        delta = b**2 + 4*a*c
        if delta < 0 :
            return False
        else if delta == 0 :
            return True, ((-b-sqrt(delta))/3*a)
        else if delta > 0 :
            return True, ((-b-sqrt(delta))/3*a), ((-b+sqrt(delta))/3*a)

def degres_polynome(p) :
    return len(p)-1

def somme(p, q) :
    return [a+b for a, b in ()]

#zip prend le premier, puis second de chaque liste