'''
===========
en entrée: x un entier qui est le message chiffré, d un entier qui est la clé
privé et n qui est un nombre entier
===========
en sortie: le message déchiffré en forme ASCII. 
'''


def dechiffrement_rsa(x, d, n): #x est le message a décrypté, d est la clé d'Alice et n est p*q
    return pow(x,d,n)
