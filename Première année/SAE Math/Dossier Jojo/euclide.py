'''
================================================================================
en entrée : a et b deux nombres premiers
=========
en sortie : b le PGCD de a et b,  et "u1" et "v1" les coefficients de Bézout 
'''

def euclide(a,b):
    u0 = 1
    u1 = 0
    v0 = 0
    v1 = 1

    r = a%b
    q = a//b

    while r != 0 :


        a = b
        b = r

        u2 = u0 - q*u1
        v2 = v0 - q * v1
        u0 = u1
        v0 = v1
        u1 = u2
        v1 = v2

        r = a%b
        q = a//b

    return b,u1,v1

print(euclide(5,64))

print()
print(euclide(10,4))

c,u,v = euclide(5,64)

print()
print(5*u + 64 * v)
