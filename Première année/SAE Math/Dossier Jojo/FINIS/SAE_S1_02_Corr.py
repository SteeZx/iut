import math

'''
en entrée : list_char qui est une liste de nombre en valeur décimale ASCII

en sortie: tab, un tableau comportant tous les nombres formattés en binaire.
'''

def binary(list_char): 
    tab = []
    x = 0
    for i in list_char:
        z = str(format(i, "b"))
        while len(z)%4 != 0 :
            z = "0" + z
        tab.append(z)
    return tab

'''
en entrée : binaire, un nombre binaire sous formatt de chaîne de caractère

en sortie: le nombre binaire par paquet de 4 bite, 0 compris devant, sous forme de tableau de chaîne de caractère
'''

def cut(binaire) :
    tab = []
    nb_elements = 4
    if len(binaire)%4 == 0 and len(binaire) > 4 :
        for i in range(0, len(binaire), 4):
            tab.append(binaire[i:nb_elements])
            nb_elements += 4
    else :
        tab.append(str(binaire))
    return tab

'''
en entrée : un binaire en chaîne de caractère

en sortie : le nombre de 1 que comporte le nombre binaire
'''

def poids(binaire):
    count = 0
    for i in binaire :
        if int(i) == 1 :
            count+=1
    return count

'''
en entrée : deux vecteurs, v1 et v2 sous forme de chaine de caractère ex "0101"

en sortie : un vecteur v3, issue de la somme de v1 et v2
'''

def somme_vecteur(v1, v2) :
    v3 = []
    for i in range(len(v1)):
        v3.append((int(v1[i])+int(v2[i]))%2)

    return v3

'''
en entrée : deux vecteurs, v1 et v2 sous forme de chaine de caractère ex "0101"

en sortie : le nombre de 1 issu de la somme de v1 et v2
'''

def distance_vecteur(v1,v2):
    return poids(somme_vecteur(v1,v2))

'''
en entrée : un vecteur de 4 bit sous forme de chaine de caractère

en sortie : un vecteur v2, sur 7 bit en format chaine de caractère avec un bit de parité
'''


def bit_parité(v):
    c = (int(v[0]) + int(v[1]) + int(v[3]))%2
    b = (int(v[0]) + int(v[2]) + int(v[3]))%2
    a = (int(v[1]) + int(v[2]) + int(v[3]))%2
    v2 = str(c)+str(b)+v[0]+str(a)+v[1]+v[2]+v[3]
    return v2

'''
en entrée : aucune

en sortie : une liste de tous les vecteurs de 4 bit (16 vecteurs)
'''

def vect_b():
    tab = []
    for i in range(0,16,1):
        j = binary([i])
        tab.append(j[0])
    return tab

'''
en entrée : aucune

en sortie : une liste des mots valides (de 7 bit)
'''

def mot_valide():

    liste = []

    for i in vect_b():
        liste.append(bit_parité(i))
    return liste

'''
en entrée : aucune

en sortie : un premier mot comparé à un deuxième mot valide et la distance entre les deux vecteurs
'''

def exercice24():
    print("Vecteur 1 :","Vecteur 2" ,"Distance entre les deux vecteurs:" )
    for i in mot_valide():

        for j in mot_valide():
            if i != j:
                a = distance_vecteur(i,j)
                print(i, j, a)

'''
en entrée : une chaine de caractère x, représentant un nombre binaire

en sortie : le nombre décimal correspondant
'''

def bin_to_ascii(x):
    return int(x, 2)


def trouver_err(mess_noise):

    for i in mot_valide():
        if distance_vecteur(mess_noise,i) <= 1 :
            return i
