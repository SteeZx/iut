import random
import math

def ASCII(string): #ASCII convertis un texte en nombres décimaux
    code = ""
    for i in range(len(string)):
        code = code + str(ord(string[i])).zfill(3) # Donne la valeur décimal du nombre via le code ASCII sur 3 chiffre donc 001 pour ma valeur 1
    return code

def premier(a): #Vérifie que le nombre a est premier
    liste = []
    for i in range (1,a+1):
        if a%i == 0:
            liste.append(i)
    return len(liste) == 2



def list_prime(n): #Créer une liste de nombre premier compris entre 1 et n
    liste = []
    for i in range (n):
        if premier(i):
            liste.append(i)
    return liste


def PGCD(A,B):
    if A>0 and B>0:
        #initialisation de X et Y
        if A>B:
            X = A
            Y = B
        else:
            X = B
            Y = A

            #calcul du PGCD
        while X % Y != 0 :
            R=X%Y
            X=Y
            Y=R

            q = A/B
    return Y

def extended_gcd(a,b):
    u0 = 1
    u1 = 0
    v0 = 0
    v1 = 1

    r = a%b
    q = a//b

    while r != 0 :


        a = b
        b = r

        u2 = u0 - q*u1
        v2 = v0 - q * v1
        u0 = u1
        v0 = v1
        u1 = u2
        v1 = v2

        r = a%b
        q = a//b

    return b,u1,v1



 def cle_privee(p,q,e):#e est un exposant choisit tel que PGCD(e,phi(n))=1
     n = p * q
     phi = (p - 1)*(q - 1)
     if PGCD(e,phi) == 1:
         c , d, dd = extended_gcd(e,phi)#extended_gcd(e,phi) #PGCD et coeff de Bézout (u + v = PGCD())
     return(d % phi,d)



def key_creation():
    liste = list_prime(1000)
    p = liste[random.randint(0, len(liste))]
    q = liste[random.randint(0, len(liste))]
    e = liste[random.randint(0, len(liste))]
    while p == q :
        q = liste[random.randint(0, len(liste))]

    n = p * q
    phi = (p - 1)*(q - 1)

    while e == phi:
        e = liste[random.randint(0, len(liste))]

    if PGCD(e,phi) == 1:
        c , d, dd = extended_gcd(e,phi)

    else:
        return key_creation(e)

    while d < 0:
        d = d+phi
    return (n,e,d)


def separmot(message):
    tab = []
    nb_elements = 4
    for i in range(0, len(message), 4):
        tab.append(message[i:nb_elements])
        nb_elements += 4

    return tab

def pow(a,b,c):
    return (a**b)%c


def encryption(message ,e, n): #m est le message a crypté e (un exposant) et n (p*q) sont la clé publique donné pour chiffrer le message
    message_crypt = ""
    error = "Valeur d'un caractère inférieure à 0 ou supérieur à n"
    message = ASCII(message)
    tab = separmot(message)
    for w in range(len(tab)):
        if int(tab[w]) < 0 or int(tab[w]) > n :
            return error
    for i in range(len(tab)):
        message_crypt.append(pow(int(tab[i]), e, n))

    return message_crypt




def decryption(tab, d, n): #x est le message a décrypté, d est la clé d'Alice et n est p*q
    message_crypt = ""
    for i in range(len(tab)):
        message_crypt = message_crypt + str(pow(tab[i], d, n)).zfill(4)
    decrypt = ""
    x = separmot(message_crypt)
    for w in x :
        decrypt = decrypt + chr(int(w))
    return decrypt