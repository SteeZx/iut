import random
import math

def ASCII(string): #ASCII convertis un texte en nombres décimaux
    code = ""
    for i in range(len(string)):
        code = code + str(ord(string[i])).zfill(4) # Donne la valeur décimal du nombre via le code ASCII sur 3 chiffre donc 001 pour ma valeur 1
    return code

def premier(a): #Vérifie que le nombre a est premier
    liste = []
    for i in range (1,a+1):
        if a%i == 0:
            liste.append(i)
    return len(liste) == 2

# print(premier(982))


def list_prime(n): #Créer une liste de nombre premier compris entre 1 et n
    liste = []
    for i in range (n):
        if premier(i):
            liste.append(i)
    return liste

# print()
# print(list_prime(983))

def PGCD(A,B):
    if A>0 and B>0:
        #initialisation de X et Y
        if A>B:
            X = A
            Y = B
        else:
            X = B
            Y = A

            #calcul du PGCD
        while X % Y != 0 :
            R=X%Y
            X=Y
            Y=R

            q = A/B
    return Y
            #affichage
        # print('Le PGCD de ' +str(A)+' et ' +str(B)+ ' vaut ' +str(Y))
        
        # print('Le coeff de Bézout est ( '+str(u1)+' , '+str(v1)+' )')
        # return R,u1,v1

# PGCD(5,64)
# print()

def euclide(a,b):
    u0 = 1
    u1 = 0
    v0 = 0
    v1 = 1

    r = a%b
    q = a//b

    while r != 0 :


        a = b
        b = r

        u2 = u0 - q*u1
        v2 = v0 - q * v1
        u0 = u1
        v0 = v1
        u1 = u2
        v1 = v2

        r = a%b
        q = a//b

    return b,u1,v1



# def cle_privee(p,q,e):#e est un exposant choisit tel que pgcd(e,phi(n))=1
#     n = p * q
#     phi = (p - 1)*(q - 1)
#     if PGCD(e,phi) == 1:
#         c , d, dd = euclide(e,phi)#euclide_etendu(e,phi) #Pgcd et coeff de Bézout (u + v = PGCD())
#     return(d % phi,d)


# print('clé privé')
# print(cle_privee(5,17,5))


def key_creation():
    liste = list_prime(1000)
    p = liste[random.randint(0, len(liste))]
    q = liste[random.randint(0, len(liste))]
    e = liste[random.randint(0, len(liste))]
    while p == q :
        q = liste[random.randint(0, len(liste))]

    n = p * q
    phi = (p - 1)*(q - 1)

    while e == phi:
        e = liste[random.randint(0, len(liste))]

    if PGCD(e,phi) == 1:
        c , d, dd = euclide(e,phi)

    else:
        return key_creation(e)

    while d < 0:
        d = d+phi
    return (n,e,d)

# print(key_creation())

def separmot(message):
    tab = []
    nb_elements = 4
    for i in range(0, len(message), 4):
        tab.append(message[i:nb_elements])
        nb_elements += 4

    return tab

def pow(a,b,c):
    return (a**b)%c


def chiffrement_rsa(message ,e, n): #m est le message a crypté e (un exposant) et n (p*q) sont la clé publique donné pour chiffrer le message
    message_crypt = []
    error = "Valeur d'un caractère inférieure à 0 ou supérieur à n"
    message = ASCII(message)
    tab = separmot(message)
    for w in range(len(tab)):
        if int(tab[w]) < 0 or int(tab[w]) > n :
            return error
    for i in range(len(tab)):
        message_crypt.append(pow(int(tab[i]), e, n))

    return message_crypt

print(chiffrement_rsa("Pute", 29, 273487))
# print()
# print('message chiffré')
# print(chiffrement_rsa(ord('m'),29,273487))



def dechiffrement_rsa(tab, d, n): #x est le message a décrypté, d est la clé d'Alice et n est p*q
    message_crypt = ""
    for i in range(len(tab)):
        message_crypt = message_crypt + str(pow(tab[i], d, n)).zfill(4)
    decrypt = ""
    x = separmot(message_crypt)
    for w in x :
        decrypt = decrypt + chr(int(w))
    return decrypt

print(dechiffrement_rsa([193009, 260385, 47572, 83295], 18789, 273487))
# print()
# print('message déchiffré')
# print(chr(dechiffrement_rsa(109139,18789,273487)))



# def main(phrase):
#     chiffré = ""
#     n, e, d = key_creation()
#     num = ASCII(phrase)
#     print("Phrase en ASCII :")
#     print(x)
#     tab = separmot(num)
#     for i in range(len(tab)) :
#         chiffré.append(chiffrement_rsa(tab[i]))
#     print(chiffré)
