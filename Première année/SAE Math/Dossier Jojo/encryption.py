'''
====================
en entrée: m qui est le message en ASCII, e un entier obtenue lors de la création
de clé et n un nombre entier.
====================
en sortie: le message chiffré.
'''

def chiffrement_rsa(m ,e, n): #m est le message a crypté e (un exposant) et n (p*q) sont la clé publique donné pour chiffrer le message
    if 0<=m<n:
        return pow(m,e,n)


'''
========
en entrée: 3 entiers : a,b et c.
========
en sortie: un entier qui est le résultat d'un calcul.
'''

def pow(a,b,c):
    return (a**b)%c
