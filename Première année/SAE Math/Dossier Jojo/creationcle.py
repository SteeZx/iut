import random
import math

def premier(a):
    liste = []
    for i in range (1,a+1):
        if a%i == 0:
            liste.append(i)
    return len(liste) == 2


def list_prime(n):

    liste = []

    for i in range (n):
        if premier(i):
            liste.append(i)
    return liste

print()

def PGCD(A,B):
    if A>0 and B>0:
        #initialisation de X et Y
        if A>B:
            X = A
            Y = B
        else:
            X = B
            Y = A

            #calcul du PGCD
        while X % Y != 0 :
            R=X%Y
            X=Y
            Y=R

            q = A/B
            #affichage
        return Y

def euclide(a,b):
    u0 = 1
    u1 = 0
    v0 = 0
    v1 = 1

    r = a%b
    q = a//b

    while r != 0 :


        a = b
        b = r

        u2 = u0 - q*u1
        v2 = v0 - q * v1
        u0 = u1
        v0 = v1
        u1 = u2
        v1 = v2

        r = a%b
        q = a//b

    return b,u1,v1

d = 0

'''
================================================================================
en sortie : n un nombre entier, e un nombre premier et d un entier.
'''

def key_creation():
    liste = list_prime(1000)
    p = liste[random.randint(0, len(liste))]
    q = liste[random.randint(0, len(liste))]
    e = liste[random.randint(0, len(liste))]
    while p == q :
        q = liste[random.randint(0, len(liste))]

    n = p * q
    phi = (p - 1)*(q - 1)

    while e == phi:
        e = liste[random.randint(0, len(liste))]

    if PGCD(e,phi) == 1:
        c , d, dd = euclide(e,phi)

    else:
        return key_creation(e)

    while d < 0:
        d = d+phi
    return (n,e,d)

print(key_creation())
