'''
================================================================================
en entrée : un entier a
=========

en sortie : un booléen qui me dit si a est premier

'''


def premier(a):
    liste = []
    for i in range (1,a+1):
        if a%i == 0:
            liste.append(i)
    return len(liste) == 2

print(premier(982))

'''
================================================================================
en entrée : un entier n
=========
en sortie : une liste qui retourne tous les nombres premiers qui
sont inférieur ou égal à n.
'''

def list_prime(n):

    liste = []

    for i in range (n):
        if premier(i):
            liste.append(i)
    return liste

print()
print(list_prime(983))
