DROP TABLE concerne;
DROP TABLE travail;
DROP TABLE projet;
DROP TABLE services;
DROP TABLE employe;


create table employe
(nuempl NUMBER(4,0) PRIMARY KEY,
Nomemple VARCHAR(10) NOT NULL,
hebdo NUMBER(4,0) CHECK(hebdo < 35),
affect NUMBER (2,0) NOT NULL
);

create table services
(Nuserv NUMBER(2,0) PRIMARY KEY,
Nomserv VARCHAR(20) NOT NULL,
Chef NUMBER(2,0) NOT NULL,
CONSTRAINT Ct6 FOREIGN KEY (chef) REFERENCES employe(nuempl) 
);

create table projet
(Nuproj NUMBER(3,0) PRIMARY KEY,
Nomproj VARCHAR(20) NOT NULL,
resp NUMBER(4,0) NOT NULL
);

create table travail
(nuempl NUMBER(4,0),
nuproj NUMBER(3,0),
duree NUMBER(2,0) NOT NULL,
CONSTRAINT PK_travail PRIMARY KEY ( nuempl, nuproj),
CONSTRAINT FK_employe FOREIGN KEY (nuempl) REFERENCES employe(nuempl),
CONSTRAINT FK_projet FOREIGN KEY (nuproj) REFERENCES projet(nuproj)
);

create table concerne
(Nuserv NUMBER(2,0),
 nuproj NUMBER(4,0),
 CONSTRAINT PK_concern PRIMARY KEY (Nuserv, nuproj),
 CONSTRAINT FK_service FOREIGN KEY (Nuserv) REFERENCES services(Nuserv),
 CONSTRAINT FK_proje FOREIGN KEY (nuproj) REFERENCES projet(nuproj)
);

update employe set hebdo=hebdo-2 where nuempl = 20;

   
insert into employe values (1,'Martin',25,12);

DELETE from employe where nuempl = 1;

alter table employe add salaire number(5);

update employe set salaire = 0 where affect=3;

select nomempl from basetd.employe, basetd.travail where employe.nuempl=travail.nuempl ;

select nomempl from basetd.employe where nuempl in (20,30,42) ;

select nuproj from basetd.projet where nomproj like 'c%';

select nuempl from basetd.employe where hebdo is NULL;

select * from basetd.employe, basetd.travail where travail.nuempl(+) = employe.nuempl ;

select * from basetd.employe e NATURAL JOIN basetd.employe t;

select nuempl, nomempl from basetd.employe union select nuempl, nuproj from basetd.travail;

select nuproj from basetd.projet intersect select nuproj from basetd.travail;

select nuproj from basetd.projet intersect select nuproj, nuproj from basetd.travail;

select nuempl from basetd.employe minus select nuempl from basetd.travail;

select count(*) from basetd.employe;

select avg(hebdo) from basetd.employe;

select sum(duree) from basetd.travail;

select nomempl from basetd.employe order by nomempl;

select travail.nuempl, duree from basetd.employe, basetd.travail where employe.nuempl = travail.nuempl order by travail.nuempl DESC;

select nomempl, hebdo from basetd.employe where hebdo <= 30 and hebdo >= 20;

select * from basetd.employe;

select * from basetd.service;

select service.nomserv, nomempl from basetd.service, basetd.employe where service.chef = employe.nuempl;

select * from basetd.projet;

select nomempl from basetd.employe e inner join basetd.service s on s.chef=e.nuempl;

select nomempl from basetd.employe e where nuempl = service.chef;