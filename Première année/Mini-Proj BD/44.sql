-- Tables bonnes (sans clés)

SELECT * FROM i2b04a.correctrecharge;

CREATE TABLE rechargementla
    AS
        SELECT
            "AMÉNAGEUR",
            "OPÉRATEUR",
            enseigne,
            codestation,
            "LIBELLÉSTATION",
            adresse,
            insee,
            longitude,
            latitude,
            nombrepointsdecharge,
            puissancemaximum,
            typedeprise,
            "ACCÈSRECHARGE",
            observations,
            datedemiseajour,
            localisation,
            code_dep,
            commune,
            "DÉPARTEMENT",
            source
        FROM
            I2B04A.correctrecharge
        WHERE
            code_dep = 44;
            
DROP TABLE communela;


CREATE TABLE communela
    AS
        SELECT DISTINCT
            insee,
            commune,
            "DÉPARTEMENT"
        FROM
            I2B04A.correctrecharge
        WHERE 
        code_dep = 44;
            

CREATE TABLE stationla

AS ( SELECT
    codestation,
    insee,
    "AMÉNAGEUR",
    libelléstation,
    puissancemaximum,
    longitude,
    latitude,
    observations,
    accèsrecharge,
    datedemiseajour,
    source,
    adresse,
    nombrepointsdecharge,
    typedeprise
FROM
    i2b04a.correctrecharge
WHERE
    insee IN (
        SELECT
            insee
        FROM
            communela
    )
    
    );
    
CREATE TABLE operateurla AS
    SELECT DISTINCT "AMÉNAGEUR", "OPÉRATEUR", enseigne FROM I2B04A.correctrecharge
    WHERE code_dep = 44;

     
ALTER TABLE stationla
ADD PRIMARY KEY (codestation);

ALTER TABLE communela
ADD PRIMARY KEY (insee);

ALTER TABLE stationla
ADD FOREIGN KEY (insee) references communela(insee);

ALTER TABLE communela
ADD FOREIGN KEY ("DÉPARTEMENT") references i2b04a.departements("DÉPARTEMENT");

ALTER TABLE operateurla
ADD PRIMARY KEY ("AMÉNAGEUR");

ALTER TABLE stationla
ADD FOREIGN KEY ("AMÉNAGEUR") REFERENCES operateurla("AMÉNAGEUR");

--======= REQUÊTES

/*SELECT * FROM operateurla;

SELECT o."AMÉNAGEUR", c.commune, COUNT(*) FROM operateurla o, communela c, stationla s
    WHERE "AMÉNAGEUR" IN (SELECT * FROM stationla  WHERE codeinsee IN (SELECT * FROM communela));*/

--Regroupement par communes

SELECT co.commune, COUNT(*) as "NbStations"
FROM operateurla op, stationla sta, communela co  
WHERE op."AMÉNAGEUR" = sta."AMÉNAGEUR"
AND sta.insee = co.insee
GROUP BY co.commune
ORDER BY 2 DESC
FETCH FIRST 20 ROWS ONLY;


--Regroupement par aménageurs

SELECT sta."AMÉNAGEUR", COUNT(*)
FROM operateurla op, stationla sta, communela co  
WHERE op."AMÉNAGEUR" = sta."AMÉNAGEUR"
AND sta.insee = co.insee
GROUP BY sta."AMÉNAGEUR";

-- Nombre de stations par aménageur avec la puissance max.

SELECT sta."AMÉNAGEUR", COUNT(*) "nombre de stations", max(regexp_substr(sta.puissancemaximum, '[0-9]*')) "Puissance Maximum"
FROM operateurla op, stationla sta, communela co
WHERE op."AMÉNAGEUR" = sta."AMÉNAGEUR"
AND sta.insee = co.insee
GROUP BY sta."AMÉNAGEUR";

-- Requête pour la répartition des prises par commune

SELECT
    c.commune,
    c.insee,
    COUNT(s.codestation) AS "Nombres de Stations"
FROM
    communela c,
    stationla s
WHERE
    c.insee = s.insee
GROUP BY
    c.commune,
    c.insee;

-- Requête pour la répartition des prises par commune et types de prises
    
SELECT
    c.commune,
    s.typedeprise,
    COUNT(*) AS "Nombres de Stations"
FROM
    communela c,
    stationla s
WHERE
    c.insee = s.insee
GROUP BY
    c.commune, s.typedeprise;
    
-- Stations par Enseigne

SELECT
    a.enseigne,
    COUNT(s.codestation) AS "Stations par Enseigne"
FROM
    operateurla a,
    stationla   s
WHERE
    s."AMÉNAGEUR" = a."AMÉNAGEUR"
GROUP BY
    a.enseigne;
    
    
-- Viens de Ewenn 
    
CREATE OR REPLACE VIEW regroupementcommunela as SELECT co."DÉPARTEMENT" ,sum("nbstations") "Nbstations"
    FROM stationla t, communela co, departements dep
    WHERE t.commune = co.commune
    AND co."DÉPARTEMENT" = dep."DÉPARTEMENT"
    GROUP BY co."DÉPARTEMENT";
    
CREATE TABLE nbstationsbycommune AS (
    SELECT co.commune, co."DÉPARTEMENT", COUNT(*) "nbstations"
    FROM operateurla op, stationla sta, communela co  
    WHERE op."AMÉNAGEUR" = sta."AMÉNAGEUR"
    AND sta.insee = co.insee
    GROUP BY co.commune, co."DÉPARTEMENT"
);


CREATE OR REPLACE VIEW nbondepartement AS (
    SELECT co."DÉPARTEMENT" ,sum("nbstations") "Nbstations"
    FROM nbstationsbycommune t, communela co, i2b04a.departements dep
    WHERE t.commune = co.commune
    AND co."DÉPARTEMENT" = dep."DÉPARTEMENT"
    GROUP BY co."DÉPARTEMENT"
);


GRANT SELECT ON nbondepartement TO i2b04a WITH GRANT OPTION;
GRANT SELECT ON nbstationsbycommune to i2b04a;
