import matplotlib.pyplot as plt
import numpy as np

def BaseLagrange(X, listX, i):
    assert(i < len(listX))
    result = 1
    tabresult = []
    for j in range(len(listX)):
        if j != i :
            result *= (X - listX[j])/(listX[i] - listX[j])
            tabresult.append(result)
    return tabresult[0]

# z = np.linspace(-1, 2, 300)
# print(np.linspace(-1, 2, 300))
# plt.plot(z, BaseLagrange(z, [-0.5, 0.5, 1.5, 2], 1))
# plt.scatter([-0.5, 0.5, 1.5, 2], [0,1,0,0])
# plt.show()

def interLagrange(X, listX, listY):
    assert len(listX) != len(listY), "bite"
    s = 0
    for i in range(len(listY)):
        s+= listY[i]*BaseLagrange(X, listX, i)
    return s


## Ceci est un commentare

def EvalP(b, listCoef):
    a0 = listCoef[0]
    for i in range(1, len(listCoef)):
        a0 += listCoef[i]*(b**i)
    return a0 


print(EvalP(2, [1, 2, 3, 4, 5]))

n + !n