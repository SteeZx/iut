package tri

/*
La fonction tri doit trier un tableau d'entiers du plus petit au plus grand.
Cette fonction ne doit pas modifier le tableau donné en entrée.

# Entrée
- tinit : un tableau d'entiers qui ne doit pas être modifié.

# Sortie
- tfin : un tableau contenant les mêmes entiers que tinit mais triés du plus
         petit au plus grand.
*/

func tri(tinit []int) (tfin []int) {

	if len(tinit) == 0 || tinit == nil {
		return tfin
	}

	var num int = tinit[0]

	for k := 0; k < len(tinit); k++ {
		if tinit[k] < num {
			num = tinit[k]
		}
	}
	tfin = append(tfin, num)

	for i := 0; i < len(tinit); i++ {
		for j:=0; j < len(tfin); j++ {
			if tinit[i] > tfin[j] {
				tfin = append(tfin, tinit[i])
				break
			} else if tinit[i] == tfin[j] {
				break
			}
		}
	}
	return tfin
}



for i, v := range tinit {
	for i range tfin {
		if v < tfin[i] {
			tfin[i], v = v, tfin[i]
		}
	}
	tfin = append(tfin, v)
}
