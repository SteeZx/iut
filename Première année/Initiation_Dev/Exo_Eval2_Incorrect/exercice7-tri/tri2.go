package tri2

/*
La fonction triabs doit trier un tableau d'entiers de la plus grande valeure
absolue à la plus petite valeure absolue. Cette fonction ne doit pas modifier
le tableau donné en entrée.

# Entrée
- tinit : un tableau d'entiers qui ne doit pas être modifié.

# Sortie
- tfin : un tableau contenant les mêmes entiers que tinit mais triés du plus
         grand (en valeure absolue) au plus petit (en valeure absolue).
*/

func absolue(n int) (sort int) {
	if n<0{
		return n*-1
	}
	return n
}

func triabs(tinit []int) (tfin []int) {

if len(tinit) == 0 || tinit == nil {
		return tfin
	}
	
    tfin = append(tfin, tinit[0])

	for i:=1; i < len(tinit); i++ {
		tfin = append(tfin, tinit[i])
	}

	var a, b int
	b = 1
	
	for b < len(tfin){

		a = tfin[b]
		var c int
		c = b-1

		for c >= 0 && absolue(tfin[c]) < absolue(a){
		tfin[c+1] = tfin[c]
		c--
	}
	tfin[c+1] = a 
	b++
}
	return tfin
}
