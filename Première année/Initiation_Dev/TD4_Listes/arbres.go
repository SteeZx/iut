package main

import "fmt"

type arbre struct{
	val int
	filsG *arbre
	filsD *arbre

}

func main(){
	var A *arbre = nil
	// fmt.Println(root(A))
	// A = inserer(16, A)
	A = inserer(14, A)
	A = inserer(24, A)
	A = inserer(2, A)

	fmt.Println(A)

}

func root(A *arbre)(int){
	return A.val
}

func make(v int, filsG *arbre, filsD *arbre)(*arbre){

	var A arbre = arbre{val: v, filsG: nil, filsD: nil}
	return &A
}

func isEmpty(A *arbre)(bool){
	return A == nil
}

func inserer(v int, A *arbre)(*arbre){
	if isEmpty(A) {
		return make(v, nil, nil)
	}
	if v < A.val {
		if isEmpty(A.filsG) {
			A.filsG = make(v, nil, nil)
		}else{
			return inserer(v, A.filsG)
		}
	}else if v >= A.val {
		if isEmpty(A.filsD) {
			A.filsD = make(v, nil, nil)
		}else{
			return inserer(v, A.filsG)
		}
	}
	return A
}