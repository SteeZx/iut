CREATE TABLE distribution
    AS
        SELECT
            *
        FROM
            basetd.distribution;

CREATE TABLE departement
    AS
        SELECT
            *
        FROM
            basetd.departement;

CREATE TABLE operateur
    AS
        SELECT
            *
        FROM
            basetd.operateur;

CREATE TABLE commune
    AS
        SELECT
            *
        FROM
            basetd.commune;
            
            
--===================Création de tables======================

CREATE TABLE communeLA AS SELECT * FROM basetd.commune WHERE nomdep = 'Loire-Atlantique';

CREATE TABLE operateurLA AS SELECT * FROM basetd.operateur;
     
CREATE TABLE distributionLA AS SELECT * 
    FROM basetd.distribution
    WHERE code_insee IN (
        SELECT code_insee
        FROM communela
    )
    AND numfo IN (
        SELECT numfo
        FROM operateurla
    );
    
DELETE FROM operateurLA where generation not in ('4G', '5G');
DELETE FROM distributionla WHERE numfo NOT IN (SELECT numfo FROM operateurla);

DROP TABLE operateur;
--==========================


GRANT SELECT ON DistributionLA TO i2b04a;
GRANT SELECT ON operateurLA TO i2b04a;
GRANT SELECT ON communeLA TO i2b04a;

--Ajout des contraintes
ALTER TABLE communela 
ADD CONSTRAINT PK_insee PRIMARY KEY (code_insee);

ALTER TABLE distributionla
ADD CONSTRAINT PK_id PRIMARY KEY (id);

ALTER TABLE operateurla
ADD CONSTRAINT PK_numfo PRIMARY KEY (numfo);

ALTER TABLE distributionla
ADD CONSTRAINT FK_codeinsee 
FOREIGN KEY (code_insee) REFERENCES communela(code_insee);

ALTER TABLE distributionla
ADD CONSTRAINT FK_numfo
FOREIGN KEY (numfo) REFERENCES operateurla(numfo);

DELETE FROM operateurLA where generation not in ('4G', '5G');
DELETE FROM distributionla WHERE numfo NOT IN (SELECT numfo FROM operateurla);

SELECT * FROM distributionla ORDER BY code_insee DESC;

SELECT * FROM i2b04a.restriction_distribution;
    
SELECT * FROM distributionla;
GRANT INSERT ON distributionla TO i2b04a;  


UPDATE i2b04a.distributionve SET adresse = 'Mes couilles sur ton front ça fait un dindon' WHERE id = 481650;
COMMIT;
SELECT * FROM i2b04a.distributionve WHERE ID = 481650;

GRANT INSERT ON distributionla TO i2b04a;

-- Q4

REVOKE ALL PRIVILEGES ON operateurla FROM i2b04a;

-- Q5

CREATE OR REPLACE VIEW restrict_distributionla AS 
    SELECT d.id, d.numfo, d.code_insee, d.adresse , d.statut FROM distributionla d;
    
GRANT SELECT ON restrict_distributionla TO i2b04a;

-- Q6

SELECT * FROM i2b04a.restriction_operateur;

CREATE OR REPLACE VIEW nb4g AS
    SELECT nom_commune, (
    SELECT COUNT(*)
    FROM distributionla dis
    WHERE dis.code_insee = co.code_insee
    AND dis.numfo IN (
        SELECT op.numfo
        FROM operateurla op
        WHERE op.generation = '4G'
        )
    ) nb4g
FROM communela co;
CREATE OR REPLACE VIEW nb5g AS
    SELECT nom_commune, (
    SELECT COUNT(*)
    FROM distributionla dis
    WHERE dis.code_insee = co.code_insee
    AND dis.numfo IN (
        SELECT op.numfo
        FROM operateurla op
        WHERE op.generation = '5G'
        )
    ) nb5g
FROM communela co;
CREATE OR REPLACE VIEW nbAntennes AS
    SELECT a.nom_commune, a.nb4g, b.nb5g 
    FROM nb4g a, nb5g b 
    WHERE a.nom_commune = b.nom_commune;
    
GRANT SELECT ON count_antennes TO i2b04a;

-- Q7

DROP VIEW nbantennes;

CREATE OR REPLACE VIEW op_antennes AS
SELECT co.nom_commune, (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.generation IN('4G', '5G')
        AND op.nomfo = 'FREE MOBILE'
    ) "Free", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.generation IN('4G', '5G')
        AND op.nomfo = 'ORANGE'
    ) "Orange", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.generation IN('4G', '5G')
        AND op.nomfo = 'BOUYGUES TELECOM'
    ) "Bouygues Telecom", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.generation IN('4G', '5G')
        AND op.nomfo = 'SFR'
    ) "SFR"
FROM communela co;

GRANT SELECT ON op_antennes TO i2b04a;

-- nbantennesbyoperateur

-- Q8
CREATE OR REPLACE VIEW count_antennes_tech AS
SELECT co.nom_commune, (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.technologie = 'LTE 700'
    ) "LTE 700", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.technologie = 'LTE 800'
    ) "LTE 800", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.technologie = 'LTE 1800'
    ) "LTE 1800", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.technologie = 'LTE 2100'
    ) "LTE 2100", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.technologie = 'LTE 2600'
    ) "LTE 2600", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.technologie = '5G NR 700'
    ) "5G NR 700", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.technologie = '5G NR 2100'
    ) "5G NR 2100", (
    SELECT COUNT(*)
        FROM distributionla dis, operateurla op
        WHERE co.code_insee = dis.code_insee
        AND op.numfo = dis.numfo
        AND op.technologie = '5G NR 3500'
    ) "5G NR 3500"
FROM communela co;

GRANT SELECT ON count_antennes_tech TO i2b04a;

-- nbantennesbytechnologie

--Q9

CREATE OR REPLACE VIEW nb4gOrange AS
    SELECT nom_commune, (
    SELECT COUNT(*)
    FROM distributionla dis
    WHERE dis.code_insee = co.code_insee
    AND dis.numfo IN (
        SELECT op.numfo
        FROM operateurla op
        WHERE op.generation = '4G'
        AND op.nomfo = 'ORANGE'
        )
    ) nb4g
FROM communela co;
CREATE OR REPLACE VIEW best5g AS
    SELECT nb5g.nom_commune, nb5g.nb5g, nb4gOrange.nb4g
    FROM nb5g, nb4gorange
    WHERE nb5g.nom_commune = nb4gOrange.nom_commune
    ORDER BY nb5g DESC
    FETCH NEXT 20 ROWS ONLY;
GRANT SELECT ON best5g TO i2b04a;

SELECT * FROM i2b03b.count_best_antennes_5g;


--==========Exo 2

--Q1

CREATE ROLE monAmiEwen;

GRANT monAmiEwen TO i2b04a;

GRANT SELECT ON restrict_distributionla TO monAmiEwen;

-- Q2 

GRANT SELECT ON DistributionLA TO monAmiEwen;
GRANT SELECT ON operateurLA TO monAmiEwen;
GRANT SELECT ON communeLA TO monAmiEwen;
GRANT UPDATE(adresse) ON distributionla to monAmiEwen;
GRANT INSERT ON distributionla TO monAmiEwen;


-- Q3

REVOKE ALL PRIVILEGES ON restrict_distributionla FROM i2b04a;

UPDATE i2b04a.distributionve SET adresse = 'Chez vous sieur' WHERE id = 481650;
COMMIT;

--Q4

REVOKE SELECT ON distributionla FROM monAmiEwen;
REVOKE SELECT ON operateurla FROM monAmiEwen;
REVOKE SELECT ON communela FROM monAmiEwen;

--Q5

REVOKE monAmiEwen FROM i2b04a;

-- Q6

CREATE ROLE ewenVoir_mesTables;
CREATE ROLE ewenUpdate_mesTables;

GRANT SELECT ON DistributionLA TO ewenVoir_mesTables;
GRANT SELECT ON operateurLA TO ewenVoir_mesTables;
GRANT SELECT ON communeLA TO ewenVoir_mesTables;

GRANT UPDATE(adresse) ON distributionla to ewenUpdate_mesTables;
GRANT INSERT ON distributionla TO ewenUpdate_mesTables;


-- Q7

CREATE ROLE ewenVoirUpdate;

GRANT ewenVoir_mesTables, ewenUpdate_mesTables to ewenVoirUpdate;

--Q8

GRANT ewenVoirUpdate TO i2b04a;

REVOKE ewenUpdate_mestables FROM ewenvoirupdate;

CREATE TABLE communeVE AS SELECT * FROM basetd.commune WHERE nomdep = 'Vendée';
CREATE TABLE operateurVE AS SELECT * FROM basetd.operateur;
CREATE TABLE distributionVE AS SELECT * 
    FROM basetd.distribution
    WHERE code_insee IN (
        SELECT code_insee
        FROM communeve
    )
    AND numfo IN (
        SELECT numfo
        FROM operateurve
    );

DELETE FROM operateurve WHERE generation NOT IN('4G', '5G');
DELETE FROM distributionve WHERE numfo NOT IN (SELECT numfo FROM operateurve);

