package valabs

/*
La fonction valabs doit trier un tableau d'entiers de la plus petite valeur absolue
à la plus grande valeur absolue. En cas
d'égalité de valeur absolue, les nombres
négatifs doivent être placés avant les
nombres positifs.

# Entrée
- tab : un tableau d'entiers
*/

func valabs(tab []int) {

	for i:= 0; i < len(tab); i++ {
		for j := len(tab)-1 ; j > 0; j-- {
			if abs(tab[j]) < abs(tab[j-1]) {
				tab[j], tab[j-1] = tab[j-1], tab[j]
			} 
		}
	}
}

func abs(x int) (int){
	if x < 0 {
		return -x
	}
	return x
}