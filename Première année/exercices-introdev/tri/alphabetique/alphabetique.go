package alphabetique

/*
La fonction alphabetique trie un dicoleau de chaînes de caractères dans l'ordre
alphabétique.

# Entrée
- dico : le dicoleau de chaînes de caractères à trier
*/

func alphabetique(dico []string) {

	for i:= 0; i < len(dico); i++ {
		for j := len(dico)-1 ; j > 0; j-- {
			if dico[j] < dico[j-1] {
				dico[j], dico[j-1] = dico[j-1], dico[j]
			} 
		}
	}
}
