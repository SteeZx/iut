package decroissant

/*
La fonction décroissant doit trier un tableau d'entiers du plus grand
au plus petit.

# Entrée
- tab : le tableau à trier
*/

func decroissant(tab []int) {

	//if tab == nil || len(tab) == 0 {
		//return tab
	//} else if len(tab) == 1 {
		//.
		//return tab
	//}else {
		for i:= 0; i < len(tab); i++ {
			for j := len(tab)-1 ; j > 0; j-- {
				if tab[j] > tab[j-1] {
					tab[j], tab[j-1] = tab[j-1], tab[j]
				} 
			}
		}
	}
	//return tab
