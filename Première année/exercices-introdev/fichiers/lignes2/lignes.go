package lignes

import (
	"os"
	"bufio"
)

/*
La fonction lignes doit compter le nombre de lignes non vides dans un
fichier dont le nom est indiqué en paramètre.

# Entrée
- fName : une chaîne de caractères correspondant à un nom de fichier

# Sortie
- nLignes : un entier indiquant le nombre de lignes non vides dans le
fichier de nom fName ou -1 si le fichier n'existe pas
*/

func lignes(fName string) (nLignes int) {

	myFile, err := os.Open(fName)

	for err != nil {
		return -1
	}
	
	scanner := bufio.NewScanner(myFile)
	for scanner.Scan(){
		if len(scanner.Text()) > 0 {
			nLignes++
	}
	

	}
	
	myFile.Close()
	return nLignes
}