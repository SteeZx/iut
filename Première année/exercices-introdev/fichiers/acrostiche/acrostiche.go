package acrostiche

import (
        "bufio"
        "os"

)

/*
La fonction acrostiche doit reconstituer le mot formé par le premier caractère
de chaque ligne d'un fichier, en ignorant les lignes vides.

# Entrée
- fName : le nom d'un fichier

# Sortie
- mot : la chaîne de caractère obtenue en mettant l'une après l'autre, dans
        l'ordre des lignes du fichier, les premières lettres de chacunes des
        lignes du fichier dont le nom est fName (on considère que ce fichier
        existe toujours)
*/

func acrostiche(fName string) (mot string) {



        myFile, _ := os.Open(fName)
        scanner := bufio.NewScanner(myFile)
        for scanner.Scan(){
                if len(scanner.Text())>0{
                        mot = mot + scanner.Text()[0:1]
                }

        }
        myFile.Close()
	return mot
}
