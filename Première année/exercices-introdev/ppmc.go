package main

import "fmt"

func main() {
	fmt.Println(ppcm(12, 9))
}

func ppcm(x, y int) int{
	return f(x, x, y, y)
}

func f(x, x0, y, y0 int) int {

	if x == y {
		return x
	}
	if x < y {
		return f(x+x0, x0, y, y0)
	}
	return f(x, x0, y+y0, y0)
}