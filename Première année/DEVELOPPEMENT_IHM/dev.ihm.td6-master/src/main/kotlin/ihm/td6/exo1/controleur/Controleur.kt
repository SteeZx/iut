package ihm.td6.exo1.controleur

import ihm.td6.exo1.vue.Vue


class Controleur(vue: Vue) {
    val vue : Vue


    init {
        this.vue = vue
    }
    fun bindVue() {
       vue.txt_result.textProperty().bind(vue.txt_chain1.textProperty().concat(vue.txt_chain2.textProperty()))
    }

}