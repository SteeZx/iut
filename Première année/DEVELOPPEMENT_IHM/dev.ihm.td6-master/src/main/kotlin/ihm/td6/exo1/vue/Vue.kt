package ihm.td6.exo1.vue


import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.GridPane
import javafx.scene.layout.VBox

class Vue: VBox() {
    var grille : GridPane
    var chain1 : Label
    var chain2 : Label
    var result : Label

    var txt_chain1 : TextField
    var txt_chain2 : TextField
    var txt_result : TextField

    init {
       grille = GridPane()
       chain1 = Label("Chaîne 1 :")
       chain2 = Label("Chaîne 2 :")
       result = Label("Resultat !")

       txt_chain1 = TextField()
       txt_chain2 = TextField()
       txt_result = TextField()
        this.children.add(grille)
        grille.add(chain1, 0, 0)
        grille.add(chain2, 2, 0)
        grille.add(txt_chain1, 1, 0)
        grille.add(txt_chain2, 3, 0)
        grille.add(result, 0, 1)
        grille.add(txt_result, 1, 1)

    }







    
}
