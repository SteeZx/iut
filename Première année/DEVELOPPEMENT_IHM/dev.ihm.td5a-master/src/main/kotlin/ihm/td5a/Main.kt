package ihm.td5a


import ihm.td5a.controleur.ControleurComboBox
import ihm.td5a.controleur.ControleurTouche
import ihm.td5a.modele.Calendrier
import ihm.td5a.vue.Vue
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

class Main: Application() {

   override fun start(primaryStage: Stage) {

       val Calendrier = Calendrier()
       val vue = Vue()

       vue.populateMonth(Calendrier.giveAllMonths())
       vue.update(Calendrier.getYear(), Calendrier.getMonth(), Calendrier.getDayNumber(), Calendrier.getDay() )

       vue.subscribeControleurComboBox(ControleurComboBox(vue, Calendrier))


       val scene = Scene(vue, 400.0, 270.0)
       scene.onKeyPressed = ControleurTouche(vue, Calendrier)

       primaryStage.title="Calendrier MVC en javaFX"
       primaryStage.scene=scene
       primaryStage.show()

      
   }

}

fun main(){
    Application.launch(Main::class.java)
}
