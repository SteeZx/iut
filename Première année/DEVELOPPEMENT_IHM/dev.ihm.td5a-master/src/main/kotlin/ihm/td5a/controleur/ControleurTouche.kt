package ihm.td5a.controleur

import javafx.event.EventHandler
import javafx.scene.input.KeyEvent
import ihm.td5a.modele.CalendarInterface
import ihm.td5a.vue.Vue
import javafx.scene.input.KeyCode
import java.util.*

class ControleurTouche(vue: Vue, modele: CalendarInterface): EventHandler<KeyEvent> {
    val vue : Vue
    val modele : CalendarInterface
    init {
        this.vue = vue
        this.modele = modele
    }

    override fun handle(event: KeyEvent) {
        if (event.code == KeyCode.LEFT) {
            modele.moveDayOfMonth(modele.getDayNumber()-1)
        } else if (event.code == KeyCode.RIGHT) {
            modele.moveDayOfMonth(modele.getDayNumber()+1)
        }
        vue.update(modele.getYear(), modele.getMonth(), modele.getDayNumber(), modele.getDay() )
    }
}
