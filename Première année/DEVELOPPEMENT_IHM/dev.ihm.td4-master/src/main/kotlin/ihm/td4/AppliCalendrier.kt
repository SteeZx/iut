package ihm.td4


import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Label
import javafx.scene.layout.HBox


import javafx.stage.Stage
import java.util.Calendar

class AppliCalendrier : Application() {
    
    val calendar = Calendar.getInstance()
    val tabDay= arrayOf("dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi")

    override fun start(primaryStage: Stage) {

        val root = HBox()
        root.spacing = 10.0
        // LISTE DES VALEURS DE DATES

        // TEXTE

        val tabMonth= arrayOf(
                "janvier",
                "février",
                "mars",
                "avril",
                "mai",
                "juin",
                "juillet",
                "août",
                "septembre",
                "octobre",
                "novembre",
                "décembre"
        )

        val txt = Label()
        val txt2 = Label()
        val combo = ChoiceBox<String>()
        txt.text = "${this.tabDay[calendar.get(Calendar.DAY_OF_WEEK)-1]} " + "${calendar.get(Calendar.DATE)}"
        txt2.text = "${calendar.get(Calendar.YEAR)}"

        root.children.addAll(txt, combo, txt2)

        combo.getItems().addAll(tabMonth)

        val scene = Scene(root, 200.0, 200.0)
        primaryStage.title = "Calendar"
        primaryStage.scene = scene
        primaryStage.show()

    }
    
    
    
// pour mettre à jour les composants graphiques de la vue quand une modification du calendrier a eu lieu
    fun update(appli : Application){


    }
}

fun main() {
    Application.launch(AppliCalendrier::class.java)
}
