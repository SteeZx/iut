package ihm.td5b.controleur

import ihm.td5b.Bibliotheque
import ihm.td5b.MainVue
import javafx.event.ActionEvent
import javafx.event.EventHandler

class ControleurLivreSuivant(vue: MainVue, modele: Bibliotheque): EventHandler<ActionEvent> {

    val vue : MainVue
    val modele : Bibliotheque

    init {
        this.vue = vue
        this.modele = modele
    }

    override fun handle(event: ActionEvent) {
       if (modele.livreSuivantPossible()){
           modele.livreSuivant()
           vue.panneauDroit.update(modele.courant, modele.donneLivre())
           vue.effacerSelectionPanneauGauche()
           vue.selectionnerLignePanneauGauche(modele.courant)

           vue.activerBouton1(true)
           if (!modele.livreSuivantPossible()) {
               vue.activerBouton2(false)
           }
       }
    }
}