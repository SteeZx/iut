package ihm.td5b.controleur

import ihm.td5b.Bibliotheque
import ihm.td5b.MainVue
import ihm.td5b.vue.TitledPaneLivreModification
import javafx.event.ActionEvent
import javafx.event.EventHandler


class ControleurModification(vue: MainVue, modele: Bibliotheque): EventHandler<ActionEvent> {
    val vue : MainVue
    val modele : Bibliotheque

    init {
        this.vue = vue
        this.modele = modele
    }

    override fun handle(event: ActionEvent) {
        vue.updatePanneauDroit(TitledPaneLivreModification("modification livre"))
        vue.panneauDroit.update(modele.courant, modele.donneLivre())
    }
}