package ihm.td5b.controleur

import ihm.td5b.Bibliotheque
import ihm.td5b.MainVue
import javafx.event.EventHandler
import javafx.scene.control.Label
import javafx.scene.input.MouseEvent
import javafx.scene.layout.GridPane

class ControleurDetailLivre(vue: MainVue,modele: Bibliotheque ): EventHandler<MouseEvent> {
    private val vue: MainVue
    private val modele: Bibliotheque

    init {
        this.vue = vue
        this.modele = modele
    }   


    override fun handle(event: MouseEvent) {
        val event = event.source
        if (event is Label) {
            var nb = GridPane.getRowIndex(event)
            vue.effacerSelectionPanneauGauche()
            vue.selectionnerLignePanneauGauche(nb)
            modele.courant = nb
            vue.panneauDroit.update(nb, modele.livres[nb])
            if (nb == 0) {
                vue.activerBouton1(false)
                vue.activerBouton2(true)
            }
            if (nb == modele.livres.size-1) {
                vue.activerBouton2(false)
                vue.activerBouton1(true)
            }
            if ((nb > 0) && (nb < modele.livres.size-1)) {
                vue.activerBouton1(true)
                vue.activerBouton2(true)
            }
        }
    }
}
