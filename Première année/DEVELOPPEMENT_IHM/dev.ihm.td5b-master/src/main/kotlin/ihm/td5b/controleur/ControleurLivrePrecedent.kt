package ihm.td5b

import javafx.event.ActionEvent
import javafx.event.EventHandler


class ControleurLivrePrecedent(vue: MainVue, modele: Bibliotheque): EventHandler <ActionEvent> {

    val vue : MainVue
    val modele : Bibliotheque

    init {
        this.vue = vue
        this.modele=modele
    }
    override fun handle(event: ActionEvent) {
        if (modele.livrePrecedentPossible()) {
           modele.livrePrecedent()
           vue.panneauDroit.update(modele.courant, modele.donneLivre())
            vue.effacerSelectionPanneauGauche()
            vue.selectionnerLignePanneauGauche(modele.courant)
            vue.activerBouton2(true)
            if (!modele.livrePrecedentPossible()){
                vue.activerBouton1(false)
            }
        }
    }

}
