package ihm.td5b


import ihm.td5b.controleur.ControleurDetailLivre
import ihm.td5b.controleur.ControleurLivreSuivant
import ihm.td5b.controleur.ControleurModification
import ihm.td5b.vue.TitledPaneLivre
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

class Main: Application() {

    override fun start(primaryStage: Stage) {

        val vue = MainVue()
        val Bibliotheque = Bibliotheque()
        val panneauDroit = TitledPaneLivre("Informations livres")
        Bibliotheque.preremplir()
        val ControleurDetailLivre = ControleurDetailLivre(vue, Bibliotheque)
        val ControleurLivrePrecedent = ControleurLivrePrecedent(vue, Bibliotheque)
        val ControleurLivreSuivant = ControleurLivreSuivant(vue, Bibliotheque)
        val ControleurModification = ControleurModification(vue, Bibliotheque)


       /* for (i in Bibliotheque.livres.size-1 downTo 0 ) {
            panneauDroit.update(Bibliotheque.livres[i].numero-1, Bibliotheque.livres[i])
        }*/
        panneauDroit.update(0, Bibliotheque.livres[0])
        vue.updatePanneauDroit(panneauDroit)
        vue.updateLivres(Bibliotheque.livres, ControleurDetailLivre, 0)
        vue.fixeListenerBouton(vue.panneauDroit.bouton1, ControleurLivrePrecedent)
        vue.fixeListenerBouton(vue.panneauDroit.bouton2, ControleurLivreSuivant)
        vue.fixeListenerBouton(vue.boutonModification, ControleurModification)
        vue.activerBouton1(false)

        val scene = Scene(vue, 550.0, 350.0)
        primaryStage.title="TD5B MVC"
        primaryStage.scene=scene
        primaryStage.show()


    }
}

fun main(){
    Application.launch(Main::class.java)
}