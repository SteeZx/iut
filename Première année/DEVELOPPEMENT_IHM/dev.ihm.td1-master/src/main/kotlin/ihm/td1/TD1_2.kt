package ihm.td1

import ihm.td1.cos.*
import javafx.application.Application
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.layout.BorderPane
import javafx.scene.paint.Color
import javafx.stage.Stage

class TD1_2 : Application(){
    val canvas= Canvas(800.0, 400.0)
    val canvas2 = Canvas(1000.0, 600.0)

    override fun start(stage: Stage) {
        // affichage de la zone d'affichage de la courbe au centre d'un BorderPane
        val g2: GraphicsContext = canvas2.getGraphicsContext2D()
        val g: GraphicsContext = canvas.getGraphicsContext2D()
        val graph = BorderPane()
        graph.children.add(canvas2)
        graph.children.add(canvas)
        graph.setStyle("-fx-background-color: grey")
        val sceneFond = Scene(graph, canvas2.width, )
        val scene = Scene(graph, canvas.width, canvas.height)

        stage.setTitle("Graphique de Cos(x)")
        stage.setScene(scene)
        stage.show()

        // affichage du repère orange avec les valeurs xmin et xmax de l'axe horizontal et ymin et ymax du repère de l'axe vertical
        g.setStroke(Color.ORANGERED)
        g.strokeLine(0.0, canvas.height/2, canvas.width, canvas.height/2)
        g.strokeLine(canvas.width/2, 0.0, canvas.width/2, canvas.height)
        g.setStroke(Color.BLACK)
        g.strokeText("-π", 12.5, (canvas.height/2+15))
        g.strokeText("π", canvas.width-20, (canvas.height/2+15))
        g.strokeText("0.0", canvas.width/2+8, (canvas.height/2+16))
        g.strokeText("-1", canvas.width/2+8, canvas.height-12.5)
        g.strokeText("1", canvas.width/2+8, 15.0)

        // calcul pas à pas des points de la courbe (fonction cos()): vous pourrez prendre un pas de 0.1
        // Les coordonnées x et y des points doivent ensuite être placées dans la zone d'affichage (mise à l'échelle en utilisant les méthodes xtoPixel() et yToPixel() que vous aurez développé)
        //la courbe va s'afficher en prenant x dans [-Math.PI, Math.PI]

        // pour afficher le caractère PI sur la courbe => "\u03c0"

    }

    /*
    transforme un double passé en paramètre en une coordonnée x sur l'écran en respectant l'échelle du repère
    @param x le double à transformer
    @return la coordonnée en x du pixel sur l'écran
    */
    private  fun xToPixel( x: Double ): Int? {
        //TODO
        return null
    }

    /*
    transforme un double passé en paramètre en une coordonnée y sur l'écran en respectant l'échelle du repère
    @param y le double à transformer
    @return la coordonnée en y du pixel sur l'écran
    */

    private fun yToPixel(y: Double): Int? {
        //TODO
        return null
    }

}



fun main() {
    Application.launch(TD1_2::class.java)

}