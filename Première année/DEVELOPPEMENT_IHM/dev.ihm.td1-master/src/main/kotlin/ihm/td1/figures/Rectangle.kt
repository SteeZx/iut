package ihm.td1.figures

import javafx.scene.canvas.GraphicsContext
import javafx.scene.paint.Color


open class Rectangle(largeur:Double, longueur:Double, centre: Point, couleur: Color=Color.BLUE):  Figure(couleur,centre) {

  private  var largeur:Double
  private  var longueur:Double

    init{
        this.largeur = largeur
        this.longueur = longueur
    }

    override fun périmètre(): Double {
        return (this.longueur + this.largeur) * 2
    }

    override fun etiquette():String {
        return "Rectangle"
    }

    override fun dessiner(g: GraphicsContext) {
        g.setFill(couleur)
        g.fillRect(centre.x, centre.y, longueur, largeur)
        g.fillText(etiquette(), centre.x, centre.y)
    }
}
