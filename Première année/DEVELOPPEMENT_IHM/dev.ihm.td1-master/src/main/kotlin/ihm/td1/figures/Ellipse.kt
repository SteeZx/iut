package ihm.td1.figures

import javafx.scene.canvas.GraphicsContext
import javafx.scene.paint.Color
import kotlin.math.PI
import kotlin.math.sqrt

class Ellipse(centre : Point, rayonh : Double, rayonl : Double, couleur: Color=Color.RED): Figure(couleur, centre) {
    private var rayonh : Double
    private var rayonl : Double

    init {
        this.rayonh = rayonh
        this.rayonl = rayonl
    }

    override fun périmètre(): Double {
        return 2*PI*((sqrt((rayonh*rayonh)+(rayonl*rayonl)))/2)
    }

    override fun etiquette(): String {
        return "Ellipse"
    }

    override fun dessiner(g: GraphicsContext) {
        g.setFill(couleur)
        g.fillOval(centre.x, centre.y, rayonh, rayonl)
        g.fillText(etiquette(), centre.x, centre.y)
    }
}