package ihm.td1.figures

import javafx.scene.canvas.GraphicsContext
import javafx.scene.paint.Color

class Point(x:Double, y:Double, couleur:Color=Color.BLACK):FormeGeo(couleur) {
   var x:Double
   private set
   var y: Double
   private set

    init{
        this.x = x
        this.y = y
    }


    override fun toString():String {
        return "[" + this.x + "," + this.y + "]"
    }

    override fun dessiner(g: GraphicsContext) {
        g.setFill(couleur)
        g.strokeRoundRect(x, y, x-198, y-198, 2.0, 2.0)

    }

}
