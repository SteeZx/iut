package ihm.td2


import javafx.application.Application
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.layout.BorderPane
import javafx.scene.layout.TilePane
import javafx.scene.layout.VBox
import javafx.stage.Stage


class AppliBibliotheque: Application() {


    override fun start(primaryStage: Stage) {
        val base = BorderPane()
        val base_centre = VBox()
        val base_top = Button("-")
        val consult = TilePane()
        val auteur = TilePane()
        BorderPane.setAlignment(base_top, Pos.TOP_RIGHT)
        base.top = base_top

        base.center = base_centre


        val scene = Scene(base, 600.0, 500.0)
        primaryStage.title="TP2_2 en javaFX"
        primaryStage.scene=scene
        primaryStage.show()
    }

}

fun main() {
    Application.launch(AppliBibliotheque::class.java)
}