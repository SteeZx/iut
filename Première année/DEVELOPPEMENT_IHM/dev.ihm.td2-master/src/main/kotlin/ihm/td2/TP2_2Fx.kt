package ihm.td2


import javafx.application.Application
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.layout.*
import javafx.stage.Stage

const val TEXTE= ("voici un texte relativement long à "
        + "lire et qui n'a aucune sorte d'intérêt à part"
        + " celui de prendre beaucoup de place et donc "
        + "d'occuper de l'espace dans la TextArea... ")
const val ADMINISTRATEUR = "Administrateur"
const val ETUDIANT = "Etudiant(e)"
const val ENSEIGNANT = "Enseignant(e)"
const val ETAT = "Etat de l'application > en cours d'identification"


class TP2_2Fx: Application() {

    override fun start(primaryStage: Stage) {
        // création du BorderPane principal

        val root = BorderPane()
        val scene = Scene(root, 600.0, 500.0)

        val border_Center = TextArea(TEXTE)
        border_Center.isWrapText = true

        val anchor_Right = AnchorPane()
        root.center = border_Center
        root.right = anchor_Right

        anchor_Right.prefWidth = 200.0

        val hbox = HBox()
        root.bottom = hbox

        val anchor_Grid = GridPane()

        val label_Grid = Label("Bienvenu")
        val label_login = Label("Login:")
        val label_password = Label("Password:")

        val txt_login = TextField()
        val txt_password = PasswordField()
        txt_password.alignment = Pos.CENTER_LEFT
        val col1 = ColumnConstraints()

        col1.maxWidth = 50.0

        val prog = ProgressBar(0.6525)
        hbox.alignment = Pos.CENTER
        hbox.children.add(prog)

        //anchor_Grid.columnConstraints.add(1, col1)


        label_Grid.style = "-fx-font-weight : bold"

        anchor_Grid.add(label_Grid, 0, 0)
        anchor_Grid.add(label_login, 0, 1)
        anchor_Grid.add(label_password, 0, 2)
        anchor_Grid.add(txt_login, 1, 1)
        anchor_Grid.add(txt_password, 1, 2)

        AnchorPane.setTopAnchor(anchor_Grid, 0.0)
        AnchorPane.setLeftAnchor(anchor_Grid, 0.0)

        anchor_Right.children.add(anchor_Grid)

        primaryStage.title="TP2_2 en javaFX"
        primaryStage.scene=scene
        primaryStage.show()
    }
}


fun main() {
    Application.launch(TP2_2Fx::class.java)
}





