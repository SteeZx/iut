package ihm.td2

import javafx.application.Application
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.layout.*
import javafx.stage.Stage


class TD2_1Fx: Application() {

    override fun start(primaryStage: Stage) {

        // Création des variables des trois éléments principaux
        val root=GridPane()
        val FlowPane_1=FlowPane(Orientation.VERTICAL)
        val BorderPane_1=BorderPane()
        root.isGridLinesVisible=true
        root.add(FlowPane_1, 0, 0)
        root.add(BorderPane_1, 1, 0)
        root.setPrefSize(500.0, 500.0)
        FlowPane_1.alignment = Pos.CENTER
        FlowPane_1.vgap = 10.0
        FlowPane_1.setPrefSize(400.0, 300.0)
        FlowPane_1.style = "-fx-background-color : pink"

        val col1 = ColumnConstraints()
        val col2 = ColumnConstraints()
        val rawAll = RowConstraints()

        col1.percentWidth = 40.0
        col2.percentWidth = 60.0
        rawAll.percentHeight = 100.0

        root.columnConstraints.addAll(col1, col2)
        root.rowConstraints.add(rawAll)

        // Create the childrens of FlowPane_1 and BorderPane_1

        val button_1 = Button("1")
        val button_2 = Button("2")
        val button_3 = Button("3")
        val button_4 = Button("4")
        val button_5 = Button("5")
        val button_6 = Button("6")

        button_3.setPrefSize(100.0, 100.0)

        val button_top = Button("^")
        button_top.maxWidth = Double.MAX_VALUE
        val button_left = Button("<")
        button_left.maxHeight = Double.MAX_VALUE
        val button_right = Button(">")
        button_right.maxHeight = Double.MAX_VALUE
        val button_center = Button("Activation")
        button_center.maxWidth = Double.MAX_VALUE
        button_center.maxHeight = Double.MAX_VALUE

        // Ajout des enfants de chaque.

        FlowPane_1.children.addAll(button_1, button_2, button_3, button_4, button_5, button_6)
        BorderPane_1.top = button_top
        BorderPane_1.right = button_right
        BorderPane_1.left = button_left
        BorderPane_1.center = button_center









        val scene = Scene(root, 300.0, 300.0)
        primaryStage.title="TP2_1 en javaFX"
        primaryStage.scene=scene
        primaryStage.show()
    }
}


fun main() {
    Application.launch(TD2_1Fx::class.java)
}

