package ihm.td3.ecouteurs

import ihm.td3.Appli

import javafx.event.EventHandler
import javafx.scene.input.MouseEvent



class ClicPanneauEcouteur(appli: Appli):EventHandler<MouseEvent> {
    val appli : Appli
    var count = 0

    init {
        this.appli = appli
    }

    override  fun handle(event: MouseEvent) {

        count +=1
        appli.labelNbClicPanneau.text = "$count"
    }
}