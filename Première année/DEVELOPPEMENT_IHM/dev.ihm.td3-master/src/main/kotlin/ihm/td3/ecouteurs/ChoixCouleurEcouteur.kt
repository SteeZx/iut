package ihm.td3.ecouteurs

import ihm.td3.Appli
import javafx.event.ActionEvent
import javafx.event.EventHandler

class ChoixCouleurEcouteur(Appli : Appli) : EventHandler<ActionEvent>{
        private val appli : Appli

        init {
            this.appli = Appli
        }

    override fun handle(event: ActionEvent) {
        if (appli.choixCouleurs.value == "Vert") {
            appli.panneauCouleurs.style = "-fx-background-color: green"
        }
        if (appli.choixCouleurs.value == "Bleu") {
            appli.panneauCouleurs.style = "-fx-background-color: blue"
        }
        if (appli.choixCouleurs.value == "Rouge") {
            appli.panneauCouleurs.style = "-fx-background-color: red"
        }

    }
}


