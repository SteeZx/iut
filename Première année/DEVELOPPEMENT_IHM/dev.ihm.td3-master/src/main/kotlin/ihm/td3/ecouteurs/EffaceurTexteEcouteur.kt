package ihm.td3.ecouteurs

import ihm.td3.Appli
import javafx.event.EventHandler
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import kotlin.system.measureTimeMillis


class EffaceurTexteEcouteur(appli: Appli) : EventHandler<MouseEvent>{
    val appli : Appli
    var count = 0


    init {
        this.appli = appli
    }

    override  fun handle(event: MouseEvent) {

            if (event.clickCount == 2) {
                appli.zoneTexte.text = ""
                appli.textArea.text = ""
            }

    }

}
