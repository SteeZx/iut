package ihm.td3.ecouteurs

import ihm.td3.Appli
import javafx.event.ActionEvent
import javafx.event.EventHandler



class BoutonGoEcouteur(appli: Appli) : EventHandler<ActionEvent> {
        private val appli: Appli
        private var count = 0
        //--- Constructeur ---------------------------------
        init {
            this.appli=appli
        }

        //--- Code exécuté lorsque l'événement survient ----
       override  fun handle(event: ActionEvent) {

            count +=1
            appli.labelNbClicBouton.text = "$count"


       }
    }
