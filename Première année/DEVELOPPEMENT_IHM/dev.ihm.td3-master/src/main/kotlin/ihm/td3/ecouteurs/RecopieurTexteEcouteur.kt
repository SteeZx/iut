package ihm.td3.ecouteurs

import ihm.td3.Appli
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.control.Alert
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseEvent

class RecopieurTexteEcouteur(appli: Appli): EventHandler<KeyEvent>{
    val appli : Appli
    init {
        this.appli = appli
    }

    override  fun handle(event: KeyEvent) {
        if (event.text == "$"){
            warningEvent()
            appli.zoneTexte.text = appli.zoneTexte.text.replace("$", "")
            appli.textArea.text = appli.textArea.text.replace("$", "")
        } else {
            appli.textArea.text = appli.zoneTexte.text
        }
    }

    private fun warningEvent(){
        val dialog = Alert(Alert.AlertType.WARNING)
        dialog.title="ATTENTION"
        dialog.headerText = "Un caractère ennemi est trouvé !"
        dialog.contentText = "Il vous est conseillé de supprimer $"
        dialog.showAndWait()
    }
}