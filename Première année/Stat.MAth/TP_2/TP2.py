from cProfile import label
import csv
from turtle import color
import matplotlib.pyplot as plt
from sympy import li
import numpy as np
import pandas as pd

 # DEBUT FONCTION PRELIMINAIRES

def moy(*list):
    ret = [sum(list[i]) / len(list[i]) for i in range(len(list))]
    if len(ret) == 1:
        return ret[0]
    return ret

def point_moyen(X, Y):
    ret = [moy(X), moy(Y)]
    return ret

def listsmoy(*list):

    moys = []
    for i in range(len(list[0])):
        s = 1
        for j in range(len(list)):
            s *= list[j][i]
        moys.append(s)

    return sum(moys) / len(list[0])


def covariance(x, y):
    return listsmoy(x, y) - moy(x) * moy(y)

def coeffs_droite_reg(x, y, affiche = False):
    ret = [covariance(x, y) / covariance(x, x), moy(y) - (covariance(x, y) / covariance(x, x)) * moy(x)]
    if affiche:
        return str(round(ret[0], 2)) + "x + " + str(round(ret[1], 2))
    return ret

def coeff_corr(x, y):
    return (covariance(x, y)) / (np.sqrt(covariance(x, x))*np.sqrt(covariance(y, y)))

# FIN DES FONCTIONS PRELIMINAIRES


# CREATION DE LA FONCTION ETUDE

def etude(X, Y, name = "Graphique") :
    fig, ax = plt.subplots()
    fig.suptitle(name) 

    pt_moyen = point_moyen(X, Y)

    ax.scatter(X, Y, linewidth = 2.0)
    ax.scatter(pt_moyen[0], pt_moyen[1], linewidth = 2.0)

    a, b = coeffs_droite_reg(X, Y, False)
    ax.plot([min(X), max(X)], [a * min(X) + b, a*max(X) + b], c = "orange", label = "Coeff. Correlation = "+str(coeff_corr(X, Y)))

    ax.legend()
    plt.show()


# INITIALISATION DES VALEURS EXO 1 PUIS UTILISATION

annee = [1990+i for i in np.arange(13)]
pib = [1121.0, 1132.2, 1149.1, 1138.9, 1162.4, 1181.8, 1194.9, 1217.6, 1259.1, 1299.5, 1348.8, 1377.1, 1393.7]
depense = [627.5,631.7,637.5,633.7,641.2,649.0,657.3,658.2,680.7,702.6,721.2,740.1,748.9]

# etude(pib, depense, "Graphique des dépenses par rapport au PIB")

# LECTURE DU CSV ET UTILISATION DE LA FONCTION ETUDE : EXEMPLE CONCRET

data = pd.read_csv('~/Documents/iut2/iut/Stat.MAth/TP_2/1_Correlation_lineaire/PercenRevParlVSEtude.csv', sep=',', decimal=',')

# etude(data['Parent Income Rank'], data['College Attendance Rates'], "Jeunes en études sup par rapport au percentile de revenu parentale")


# ==== EXO 3.a LECTURE CSV PUIS CREATION DES GRAPHIQUES

data1 = pd.read_csv('~/Documents/iut2/iut/Stat.MAth/TP_2/1_Correlation_lineaire/ExAsetA.csv', sep=',', decimal='.')
data2 = pd.read_csv('~/Documents/iut2/iut/Stat.MAth/TP_2/1_Correlation_lineaire/ExAsetB.csv', sep=',', decimal='.')

# etude(data1['x'], data1['y'], "Graphique de Y par rapport à X : Set A")
# etude(data2['x'], data2['y'], "Graphique de Y par rapport à X : Set B")

data3 = pd.concat([data1, data2], ignore_index=True)
# etude(data3['x'], data3['y'], "Graphique de Y par rapport à X : Set A Union B")


# ==== EXO 3.b LECTURE CSV PUIS CREATION DES GRAPHIQUES

data1 = pd.read_csv('TP_2/1_Correlation_lineaire/ExBsetA.csv', sep=',', decimal='.')
data2 = pd.read_csv('TP_2/1_Correlation_lineaire/ExBsetB.csv', sep=',', decimal='.')

# etude(data1['x'], data1['y'], "Graphique de Y par rapport à X : Set A")
# etude(data2['x'], data2['y'], "Graphique de Y par rapport à X : Set B")
data3 = pd.concat([data1, data2], ignore_index=True)
# etude(data3['x'], data3['y'], "Graphique de Y par rapport à X : Set A U B")


# ==== EXO 3.c LECTURE CSV PUIS CREATION DES GRAPHIQUES

data1 = pd.read_csv('TP_2/1_Correlation_lineaire/ExCsetA.csv', sep=',', decimal='.')
data2 = pd.read_csv('TP_2/1_Correlation_lineaire/ExCsetB.csv', sep=',', decimal='.')

# etude(data1['x'], data1['y'], "Graphique de Y par rapport à X : Set A")
# etude(data2['x'], data2['y'], "Graphique de Y par rapport à X : Set B")

# etude(data1['x'], np.sqrt(data1['y']), "Graphique racine carré de Y par rapport à X : Set A")
# etude(data2['x'], np.square(data2['y']), "Graphique carré de Y par rapport à X : Set B")

# ======== EXO 2.1.a

# CREATION DE LA FONCTION

from scipy.stats import linregress

def etude_v2(X, Y, name = "Graphique") :
    fig, ax = plt.subplots()
    fig.suptitle(name) 
    
    pt_moyen = point_moyen(X, Y)
    ax.scatter(X, Y, linewidth = 2.0)
    ax.scatter(pt_moyen[0], pt_moyen[1], linewidth = 2.0)

    lr = linregress(X, Y)

    ax.plot(X, lr.intercept + lr.slope*X, c = "orange", label = "Coeff. Correlation = "+str(coeff_corr(X, Y)))
    ax.legend()
    plt.show()

# UTILISATION DE LA FONCTION

x, y = np.loadtxt("TP_2/2_Donnees_trompeuses/regLinData.dat", unpack=True)

# etude_v2(x, y, "Graphique de Y par rapport à X")

# =========EXO 2.1.b


# CREATION DE LA FONCTION

def etude_v3(X, Y, name = "Graphique") :
    fig, ax = plt.subplots()
    fig.suptitle(name)

    
    reg = coeffs_droite_reg(X, Y)
    ax.scatter(X, Y - (X*reg[0]+reg[1]), linewidth = 2.0)

    plt.show()

# UTILISATION DE LA FONCTION

etude_v3(x, y, "Graphique des résidus par rapport à X")


# ========= Exo 2.2.a et 2.2.b


# CREATION DE LA FONCTION

def etude_v4(X, Y, name = "Graphique") :
    fig, ax = plt.subplots()
    fig.suptitle(name)

    temp = np.polyfit(X, Y, 2)

    ax.scatter(X, temp[0]*X**2 + temp[1]*X + temp[2], linewidth = 2.0)

    plt.show()


# UTILISATION DE LA FONCTION

etude_v4(x, y, "Graphique de Y par rapport à X : Fonction Poly. deg. 2")