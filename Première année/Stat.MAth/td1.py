from typing import Collection
import math

X = {17, 13, 9, 6, 13, -3, -2, 3, 18, 0, 17, -1, 16}

def mean(X) : return sum(X)/len(X)

print(mean(X))

def variance(X):
    return sum((xi - mean(X)) ** 2 for xi in X) / len(X)
    
print(variance(X))

def ecart_type(X):
    return math.sqrt(variance(X))

print(ecart_type(X))

def median(X):
    tab = list(X)
    if len(tab)%2 == 0 :
        a = len(tab)/2
        b = len(tab)/2+1
        return (a+b)/2
    return tab[len(tab)%2+1]

print(median(X))