import pandas as pd
import matplotlib.pyplot as plt

# plt.style.use('_mpl-gallery')

data = pd.read_csv('1_Consommation_Menages/consommation_menages.csv', sep=',', decimal=',')

# print(type(data))
# print("===============================")
# print(data.info)
# print("===============================")
# print(data.Fonction)
# print("===============================")
print(data['Fonction'])
# print("===============================")
# print(data['1960'])
# print("===============================")
# print(data[['1960','2020']])



# -------============= Génération du plot

def genplot_barre(data, date):
    #Défini x et y, deux listes se ceront les valeurs de ces listes qui seront marqué
    y = date
    x = data.Fonction

    #Défini la zone de dessins
    fig, ax = plt.subplots()
    #Créer les barres, avec les valeurs x et y, la largeur, la couleur du contour et la largeur des lignes
    ax.bar(x, y, width=1, edgecolor="white", linewidth=1)

    #Rotate les noms
    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=30, horizontalalignment='right')
    ax.set_ylabel('Valeurs')
    ax.set_xlabel('Fonctions')
    
    plt.tight_layout()
    plt.show()

def genplot_camembert(data, func):
    #Défini x et y, deux listes se ceront les valeurs de ces listes qui seront marqué
    x = data

    #Défini la zone de dessins
    fig, ax = plt.subplots()
    #Créer les barres, avec les valeurs x et y, la largeur, la couleur du contour et la largeur des lignes
    ax.pie(x, labels = func)

    #Rotate les noms
    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=30, horizontalalignment='right')
    
    plt.tight_layout()
    plt.show()

def genplot_barreh(data, date1, date2):
    #Défini x et y, deux listes se ceront les valeurs de ces listes qui seront marqué
    y = data["age"]
    date1 = -date1

    #Défini la zone de dessins
    fig, ax = plt.subplots()
    #Créer les barres, avec les valeurs x et y, la largeur, la couleur du contour et la largeur des lignes
    ax.barh(y, date1, align='center', color='orange', height=0.5)
    ax.barh(y, date2, align='center', color='blue', height=0.5)

    #Rotate les noms
    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=30, horizontalalignment='right')
    ax.set_ylabel('Valeurs')
    ax.set_xlabel('Fonctions')

    #TODO: GENERER LA PTN DE COURBE VERTE ENFAISANT LA DIFFERENCE, DESSINER AVEC PLOT
    
    plt.tight_layout()
    plt.show()

data3 = pd.read_csv('3_Pyramide_des_ages/pyramide_ages.csv', sep=',', decimal=',')

print(data3.columns)

genplot_barreh(data3, data3['2019_F'], data3['2019_H'])

