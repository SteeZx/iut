import json
from pickle import DICT
from tokenize import String
from xml.dom import minicompat
from keyring import delete_password 
import unidecode
import random
import string
# random.seed(35)

#========== FREQUENCE ==========

import json
import glob
import os
from frequence import LettersCounter

from sympy import ordered
import platform

crypt_msg = LettersCounter.count("Exercice3_Substitution/texte_chiffre_substitution.txt")
#======== EXERCICE 2 ==========


alphabet = string.ascii_lowercase
ALPHABET = string.ascii_uppercase

str = []

#========== EXERCICE 3 =========

def string_shuffle(word):
    """
    word est une chaine de caractères
    """
    c = list(word)
    random.shuffle(c)
    return ''.join(c)

# new_file = open("freq_lettre_fr_approx.json", "")

"""
Définition de la fonction pour crypté un message aléatoirement.
"""

def crypt(msg):
    msg_w = ""
    for chara in msg :
        chara_uni = unidecode.unidecode(chara)
        if chara_uni == " ":
            continue
        else :
            msg_w += chara_uni
    msg_w = msg_w.upper()
    return(string_shuffle(msg_w))

"""
Définition de la fonction pour crypté un message avec clé.
"""

def chiffre(msg : str, key : str):
    crypted_msg = ""
    msg = msg.upper()
    for chara in msg :
        chara_uni = unidecode.unidecode(chara)
        if chara_uni == " " :
            continue
        else :
            k_tmp = key.find(chara)

            crypted_msg += ALPHABET[k_tmp]

    return crypted_msg

"""
Définition de la fonction de décryptage, utilisant le message et une clé donnée.
"""

def dechiffre(msg, key) :
    tab = []
    for chara in msg :
        tab.append(ALPHABET[key.index(chara)])
    return "".join(tab)

# def texte_crypte() :
#     texte_crypte = {}

#     txt = open("Exercice3_Substitution/texte_chiffre_substitution.txt", "r").readlines()
#     for line in txt:
#         texte_crypte[line.split(" ")[0]] = int(line.split(" ")[1])

#     return texte_crypte
# text_crypt = texte_crypte()

"""
Définition de la fonction pour récupérer les données présentes dans le fichier quadrigramme.txt, de les stocker dans
DICT_QUAD pour enfin les enregistrer en forma json.
"""

def quadri():
    DICT_QUAD = {}

    txt = open("Exercice3_Substitution/quadrigramme.txt", "r").readlines()
    for line in txt:
        DICT_QUAD[line.split(" ")[0]] = int(line.split(" ")[1])

    open("quadrigramme.json", "w").write(json.dumps(DICT_QUAD))
    return DICT_QUAD

DICT_QUAD = quadri()

"""
Fonction logscore donnée dans le PDF de TP.
"""

import numpy as np
def logscore(chain):
    """
    Calcule le log_score de la cha^
    ıne de caractère chain
    grace au dictionnaire de quadrigrammes
    """
    logsum = 0
    min_freq = 1e-10
    for i in range(len(chain)-3):
        logsum += np.log10(DICT_QUAD.get(chain[i:i+4], min_freq))
    return -logsum

"""
Ouverture puis lecture du fichier, les données de ce dernier se trouvant alors dans crypted texte
"""

file = open("Exercice3_Substitution/texte_chiffre_substitution.txt", 'r')

crypted_text = file.read()

"""
Les trois prochaines fonctions ont pour but de simplifier la fonction de decryptage et de la déchargée un peu.
"""

"""
Définition de la fonction pour permuter deux charactères dans une chaîne de charactère
"""

def change(string, index_1, index_2):
    txt = list(string)
    txt[index_1], txt[index_2] = txt[index_2], txt[index_1]
    return "".join(txt)

"""
Définition de la fonction pour déplacer un élément de la chaine de caractère
"""

def deplace(string, index_1, index_2):
    txt = list(string)
    txt.insert(index_2, txt.pop(index_1))
    return "".join(txt)

"""
Fonction pour générer les clés voisines d'une clé précise.
"""

def voisines(key, Old_keys):
    cle = []
    cle = [change(key, i, j) for i in range(len(key)) for j in range(i+1, len(key)) if change(key, i, j) not in Old_keys]
    cle += [deplace(key, i, j) for i in range(len(key)) for j in range(len(key)) if deplace(key, i, j) not in Old_keys]

    cle = list(set(cle))

    if key in cle :
        cle.remove(key)
    return cle

"""
Définition de la fonction pour décrypté assez efficacement une 
"""

def decryt_full(msg):
    key = string_shuffle(ALPHABET)
    Old_keys = []
    minimum = 50000
    for i in range(100):
        cles_voisines = voisines(key, Old_keys)
        Old_keys += cles_voisines


        score = [logscore(dechiffre(msg, i)) for i in cles_voisines]


        if min(score) > minimum :
            break
        else :
            key = cles_voisines[score.index(min(score))]

            minimum = min(score)

    return key, dechiffre(msg, key)
        
        
print(decryt_full(crypted_text))