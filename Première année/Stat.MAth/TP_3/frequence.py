import json
import glob
import os

from sympy import ordered
import unidecode
import platform

class LettersCounter():

    def count(path, pathRender = None):

        # Création du dictionnaire
        frequence = {"a":0, "b":0, "c":0, "d":0, "e":0, "f":0, "g":0, "h":0, "i":0, "j":0, "k":0, "l":0, "m":0, "n":0, "o":0, "p":0, "q":0, "r":0, "s":0, "t":0, "u":0, "v":0, "w":0, "x":0, "y":0, "z":0, "total":0}

        # Choix entre comptage dans un fichier / plusieurs fichier ou un string
        all_files = glob.glob(path)
        if len(glob.glob(path)) == 0:
            return LettersCounter.__count_in_String(path)

        # Boucle permettant au comptage des lettres des fichiers
        for fichier in all_files:

            file = open(fichier, "r")

            for line in file:
                for character in line.lower():                  # Toute la ligne est mise en minuscule
                    caractere = unidecode.unidecode(character)  # Les accents sont retirés pour n'avoir qu'une lettre "basique"
                    if caractere in frequence:
                        frequence[caractere] += 1
                        frequence["total"] += 1

        # Calcul du pourcentage d'apparition de chaque lettre
        if frequence["total"] != 0:
            for i in frequence:
                if i != "total":
                    frequence[i] /= frequence["total"]

        # Ecriture dans le fichier json
        if pathRender != None:
            json.dump(frequence, open(pathRender, 'w'))

        return frequence


    def __count_in_String(msg):
        
        frequence = {"a":0, "b":0, "c":0, "d":0, "e":0, "f":0, "g":0, "h":0, "i":0, "j":0, "k":0, "l":0, "m":0, "n":0, "o":0, "p":0, "q":0, "r":0, "s":0, "t":0, "u":0, "v":0, "w":0, "x":0, "y":0, "z":0, "total":0}
        
        for character in msg:
                character = unidecode.unidecode(character.lower())
                if character in frequence:
                    frequence[character] += 1
                    frequence["total"] += 1

        if frequence["total"] != 0:
            for i in frequence:
                if i != "total":
                    frequence[i] /= frequence["total"]
        return frequence

    # Fonction permettant d'afficher proprement les résultats dans le terminal
    def afficher(dic):

        # Création d'une liste triée par ordre décroissant à partir du dictionnaire
        orderedDict = sorted([[dic[i], i] for i in dic], reverse = True)

        # Affichage des résultats
        for lettre in orderedDict:
            if lettre[1] == "total":
                print(lettre[1], ":", lettre[0])
            else:
                print(lettre[1], ":", round(lettre[0]*100, 3), "%")


def main():
    # Simplification d'accès au données en fonction du poste de travail utilisé
    if platform.system() == 'Windows':
        file = os.path.dirname(__file__) + "\\*.txt"
        save = os.path.dirname(__file__) + "\\freq_lettre_fr_approx.json"
    else:
        file = os.path.dirname(__file__) + "/*.txt"
        save = os.path.dirname(__file__) + "/freq_lettre_fr_approx.json"

    freq = LettersCounter.count(file, save)
    LettersCounter.afficher(freq)

    # chiffre = LettersCounter.count(os.path.dirname(__file__) + "/../Exercice3_Substitution/texte_chiffre_substitution.txt")
    # LettersCounter.afficher(chiffre)


if __name__ == "__main__":
    main()