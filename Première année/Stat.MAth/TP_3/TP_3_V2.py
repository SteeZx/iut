import json
from tokenize import String 
import unidecode
import random
random.seed(35)

def string_shuffle(word):
    """
    word est une chaine de caract`eres
    """
    c = list(word)
    random.shuffle(c)
    return ''.join(c)

# new_file = open("freq_lettre_fr_approx.json", "")

# EXO 3

class Message():

    def __init__(self):
        self.msg = ""
        self.crypted_msg = ""
    
    def crypt(self, msg):
        msg_w = ""
        for chara in msg :
            chara_uni = unidecode.unidecode(chara)
            if chara_uni == " ":
                continue
            else :
                msg_w += chara_uni
        msg_w = msg_w.upper()
        return(string_shuffle(msg_w))

    def chiffre(self, key : int) :
        rng = 0
        while rng != key :
            crypted_msg = self.crypt(self.msg)
            rng += 1
        return crypted_msg

    def print(self):
        print(self.crypted_msg)

    

msg1 = Message("je sais pas")
msg1.msg = "Je sais pas"
msg1.chiffre(10)
msg1.print()