import numpy as np
import matplotlib.pyplot as plt

def f(x):
    return (x**2)-2

def taux_accroissement(f, x0, x1):
    return (f(x1)-f(x0))/(x1-x0)

def f_secante(f, x, x1):
    return taux_accroissement(f, x, x1)*(x - x1)+f(x1)

def Secante(f, x0, x1, N):
    for _ in range(N):
        y = taux_accroissement(f, x0, x1)
        x1 = x1 - f(x1) / y
    return x1

def SecanteSuite(f, x0, x1, N):

    tab = []
    for _ in range(N):
        y = taux_accroissement(f, x0, x1)
        x1 = x1 - f(x1)/y
        tab.append(x1)
    return tab



def VitesseSecante(f, x0, x1, N):
    tab = SecanteSuite(f, x0, x1, N)
    t = [(i*np.tan(i)-1) for i in tab]
    fig, ax = plt.subplots()
    ax.plot(np.arange(1, N+1), t)
    plt.show()
    


VitesseSecante(f, -20, 25, 10)

print(Secante(f, -5, 25, 50))