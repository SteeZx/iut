package but1.iut.r203

fun main(args: Array<String>) {
    val op = OperationsBinaires()

    println(op.additionner(1,2))
    println(op.soustraire(1,2))
    println(op.multiplier(1,2))
    println(op.diviserNaturel(10,2))
    println(op.factorielle(12))
    println(5.0.equals(5.0))
}