package but1.iut.r203

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.junit.jupiter.api.Assertions.*

internal class OperationsBinairesTest {
    val op = OperationsBinaires()

    @Test
    fun `Test Addition Basiques`() {
        assertEquals(24, op.additionner(10, 14))
        assertEquals(-24, op.additionner(10, -34))
        assertEquals(IndexOutOfBoundsException(), op.additionner(10, 14))
    }

    @Test
    fun `Test Addition Exception`() {
        assertThrows<ArithmeticException>{op.additionner(Int.MAX_VALUE, 1)}
    }

    @Test
    fun soustraire() {
    }

    @Test
    fun multiplier() {
    }

    @Test
    fun diviserNaturel() {
    }

    @Test
    fun factorielle() {
    }
}