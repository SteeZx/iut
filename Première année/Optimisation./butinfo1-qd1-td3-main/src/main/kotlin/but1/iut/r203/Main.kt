package but1.iut.r203

import but1.iut.r203.calcul.OperationsDependantes
import but1.iut.r203.chenil.Chien

fun main(args: Array<String>) {
    val op = OperationsDependantes()
    println(op.factorielleConsole())

    var ch1 = Chien("Lassie", "Yorkshire")
    ch1.setDateNaissance(2021, 2, 28)
    println(ch1)
    println(ch1.ageMois())

    var ch2 = Chien("Ewenn", "Lévrier afghan")
    ch2.setDateNaissance(2021, 2, 15)
}