package iut.but1.r203.td1

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

internal class OperationsTest {

    private val op = Operations()

    @Test
    fun testAdditionner() {
        assertEquals(3, op.additionner(arrayOf(1, 2)))
    }

    @Test
    fun testAdditionner1() {
        assertEquals(10, op.additionner(arrayOf(5, 5)))
    }

    @Test
    fun testAdditionner2() {
        assertEquals(15, op.additionner(arrayOf(5, 5, 5)))
    }

    @Test
    fun testAdditionner3() {
        assertEquals(6, op.additionner(arrayOf(1, 2, 3)))
    }

    @Test
    fun testAdditionner4() {
        assertEquals(6, op.additionner(arrayOf(2, 1, 3)))
    }

    @Test
    fun testAdditionner5() {
        assertEquals(6, op.additionner(arrayOf(3, 2, 1)))
    }

    @Test
    fun testAdditionner6() {
        assertEquals(0, op.additionner(arrayOf(0, 0)))
    }

    @Test
    fun testAdditionner7() {
        assertEquals(0, op.additionner(arrayOf(-4, 4)))
    }

    @Test
    fun testAdditionner8() {
        assertEquals(0, op.additionner(arrayOf(-2,0,2)))
    }

    @Test
    fun testSoustraire1() {
        assertEquals(0, op.soustraire(arrayOf(3,3)))
    }

    @Test
    fun testSoustraire2() {
        assertEquals(-3, op.soustraire(arrayOf(3,3,3)))
    }

    @Test
    fun testSoustraire3() {
        assertEquals(-4, op.soustraire(arrayOf(1,2,3)))
    }

    @Test
    fun testSoustraire4() {
        assertEquals(-2, op.soustraire(arrayOf(2,1,3)))
    }

    @Test
    fun testSoustraire5() {
        assertEquals(0, op.soustraire(arrayOf(3,2,1)))
    }

    @Test
    fun testSoustraire6() {
        assertEquals(0, op.soustraire(arrayOf(0,0)))
    }

    @Test
    fun testSoustraire7() {
        assertEquals(-10, op.soustraire(arrayOf(-5, 5)))
    }

    @Test
    fun testSoustraire8() {
        assertEquals(-4, op.soustraire(arrayOf(-2, 0, 2)))
    }

    @Test
    fun testMultiplication1() {
        assertEquals(9, op.multiplier(arrayOf(3,3)))
    }
    @Test
    fun testMultiplication2() {
        assertEquals(27, op.multiplier(arrayOf(3,3,3)))
    }
    @Test
    fun testMultiplication3() {
        assertEquals(6, op.multiplier(arrayOf(1,2,3)))
    }
    @Test
    fun testMultiplication4() {
        assertEquals(6, op.multiplier(arrayOf(2,1,3)))
    }
    @Test
    fun testMultiplication5() {
        assertEquals(6, op.multiplier(arrayOf(3,2,1)))
    }
    @Test
    fun testMultiplication6() {
        assertEquals(0, op.multiplier(arrayOf(0)))
    }
    @Test
    fun testMultiplication7() {
        assertEquals(-25, op.multiplier(arrayOf(-5,5)))
    }
    @Test
    fun testMultiplication8() {
        assertEquals(0, op.multiplier(arrayOf(-2,0,2)))
    }










    @ParameterizedTest
    @CsvSource(
        "3, 3, 9",
        "-5, 5, -25"
    )
    fun testParamMultiplication_deux_valeur(v1 : Int, v2 : Int, expected : Int) {
        assertEquals(expected, op.multiplier(arrayOf(v1,v2)))
    }

    @ParameterizedTest
    @CsvSource(
        "3, 3, 3, 27",
        "1, 2, 3, 6",
        "2, 1, 3, 6",
        "3, 2, 1, 6",
        "-2, 0, 2, 0"
    )
    fun testParamMultiplication_trois_valeur(v1 : Int, v2 : Int, v3 : Int, expected : Int) {
        assertEquals(expected, op.multiplier(arrayOf(v1,v2,v3)))
    }

    @ParameterizedTest
    @CsvSource(
        "0, 0"
    )
    fun testParamMultiplication_une_valeur(v1 : Int, expected : Int) {
        assertEquals(expected, op.multiplier(arrayOf(v1)))
    }

    @ParameterizedTest
    @CsvSource(
        "1, 2, 3",
        "2, 1, 3",
        "3, 2, 1"
    )
    fun testAssociativité_mutl(v1 : Int, v2 : Int, v3 : Int) {
        val expect = op.multiplier(arrayOf(op.multiplier(arrayOf(v1, v2)), v3))
        val valeurexpect = op.multiplier(arrayOf(v1, op.multiplier(arrayOf(v2, v3))))
        assertTrue(expect == valeurexpect)
    }

    @ParameterizedTest
    @CsvSource(
        "1, 2, 3",
        "2, 1, 3",
        "3, 2, 1"
    )
    fun testAssociativité_add(v1 : Int, v2 : Int, v3 : Int) {
        val expect = op.additionner(arrayOf(op.additionner(arrayOf(v1, v2)), v3))
        val valeurexpect = op.additionner(arrayOf(v1, op.additionner(arrayOf(v2, v3))))
        assertTrue(expect == valeurexpect)
    }

}