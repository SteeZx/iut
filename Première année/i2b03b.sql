SELECT
    di.adresse,
    o.technologie,
    o.generation,
    o.numfo,
    o.nomfo,
    c.code_insee,
    c.nom_commune,
    d.nomdep,
    d.code_departement
FROM
    basetd.distribution   di,
    basetd.operateur      o,
    basetd.departement    d,
    basetd.commune        c
WHERE
    di.numfo = o.numfo
    AND di.code_insee = c.code_insee
    AND c.nomdep = d.nomdep
    AND c.nom_commune = 'Nantes'
ORDER BY
    1;

SELECT
    di.adresse,
    o.technologie,
    o.generation,
    o.numfo,
    o.nomfo,
    c.code_insee,
    c.nom_commune,
    d.nomdep,
    d.code_departement
FROM
    basetd.distribution   di,
    basetd.operateur      o,
    basetd.departement    d,
    basetd.commune        c
WHERE
    di.numfo = o.numfo
    AND di.code_insee = c.code_insee
    AND c.nomdep = d.nomdep
    AND c.nom_commune = 'Nantes'
    AND o.generation = '5G'
ORDER BY
    1;

SELECT
    di.adresse,
    o.technologie,
    o.generation,
    o.numfo,
    o.nomfo,
    c.code_insee,
    c.nom_commune,
    d.nomdep,
    d.code_departement
FROM
    basetd.distribution   di,
    basetd.operateur      o,
    basetd.departement    d,
    basetd.commune        c
WHERE
    di.numfo = o.numfo
    AND di.code_insee = c.code_insee
    AND c.nomdep = d.nomdep
    AND c.nom_commune = 'Nantes'
    AND o.generation = '5G'
    AND o.technologie = '5G NR 3500'
ORDER BY
    1;

SELECT DISTINCT
    co.nom_commune,
    op.nomfo,
    op.technologie,
    op.generation
FROM
    basetd.commune        co,
    basetd.operateur      op,
    basetd.distribution   di
WHERE
    op.generation = '5G'
    AND di.code_insee = co.code_insee
    AND di.numfo = op.numfo
ORDER BY
    1;

SELECT
    co.nom_commune,
    op.nomfo,
    COUNT(op.generation)
FROM
    basetd.commune        co,
    basetd.operateur      op,
    basetd.distribution   di
WHERE
    op.generation = '5G'
    AND di.numfo = op.numfo
    AND co.code_insee = di.code_insee
GROUP BY
    co.nom_commune,
    op.nomfo
ORDER BY
    COUNT(op.generation) DESC,
    co.nom_commune ASC;

SELECT DISTINCT
    co.nom_commune,
    COUNT(op.generation)
FROM
    basetd.commune        co,
    basetd.operateur      op,
    basetd.distribution   di
WHERE
    op.generation = '5G'
    AND di.code_insee = co.code_insee
    AND di.numfo = op.numfo
GROUP BY
    co.nom_commune
ORDER BY
    2 DESC
FETCH NEXT 10 ROWS ONLY;

SELECT
    co.nom_commune,
    COUNT(op.generation)
FROM
    basetd.commune        co,
    basetd.operateur      op,
    basetd.distribution   di
WHERE
    op.generation = '5G'
    AND di.numfo = op.numfo
    AND co.code_insee = di.code_insee
HAVING
    COUNT(op.generation) > 50
    group by co.nom_commune
    order by count(op.generation) desc, co.nom_commune asc;
    

-- ===========================================================================
-- ========================= TP2 =============================================
-- ===========================================================================

/*select co.nom_commune 
    from basetd.commune co, basetd.departement de
    where co.nomdep = de.nomdep
    and de.code_departement = 49;*/

SELECT
    co.nom_commune
FROM
    basetd.commune co
WHERE co.nomdep
in

( SELECT
    nomdep
FROM
    basetd.departement
WHERE
    code_departement = 49
);

SELECT
    co.nom_commune
FROM
    basetd.commune co
WHERE
    EXISTS (
        SELECT
            *
        FROM
            basetd.departement de
        WHERE
            co.nomdep = de.nomdep
            AND de.code_departement = 49
    );
    
-- Exo 2

SELECT DISTINCT
    co.nom_commune,
    co.nomdep
FROM
    basetd.commune     co,
    basetd.operateur   op
WHERE co.code_insee 
IN ( SELECT di.code_insee
            FROM basetd.distribution di where di.numfo IN ( SELECT
                       op.numfo
                   FROM
                       basetd.operateur op
                   WHERE
                       op.generation = '5G'));
    
SELECT DISTINCT
    co.nom_commune,
    co.nomdep
FROM
    basetd.commune     co,
    basetd.operateur   op
WHERE
    EXISTS (
        SELECT
            *
        FROM
            basetd.distribution di
        WHERE
            co.code_insee = di.code_insee
            AND EXISTS (
                SELECT
                    *
                FROM
                    basetd.operateur
                WHERE
                    op.generation = '5G'
                    AND di.numfo = op.numfo
            )
    );
                
-- Exo 3
SELECT DISTINCT
    co.nom_commune,
    co.nomdep
FROM
    basetd.commune     co,
    basetd.operateur   op
WHERE
    co.code_insee NOT IN (
        SELECT
            di.code_insee
        FROM
            basetd.distribution di
        WHERE di.numfo IN (
    SELECT
        op.numfo
    FROM
        basetd.operateur op
    WHERE
        op.generation = '5G'
) ); 

SELECT DISTINCT
          co.nom_commune,
          co.nomdep
      FROM
          basetd.commune       co,
          basetd.departement   de
      WHERE
          NOT EXISTS (
              SELECT
                  *
              FROM
                  basetd.distribution   di,
                  basetd.operateur      op
              WHERE di.code_insee = co.code_insee AND op.numfo = di.numfo
AND op.generation = '5G' );

-- Exo 4

SELECT DISTINCT co.nom_commune, co.nomdep
from basetd.commune co
where co.code_insee NOT IN (
    SELECT DISTINCT
        dis.code_insee
    FROM
        basetd.distribution   dis,
        basetd.operateur      op
    WHERE
        op.numfo = dis.numfo
        AND op.generation = '5G'
) AND co.nomdep
IN

( SELECT
    dep.nomdep
FROM
    basetd.departement dep
WHERE
    dep.nomdep = co.nomdep
    AND dep.code_departement = 44
);

SELECT DISTINCT
    co.nom_commune,
    co.nomdep
FROM
    basetd.commune co
WHERE
    NOT EXISTS (
        SELECT
            *
        FROM
            basetd.distribution   dis,
            basetd.operateur      op
        WHERE
            dis.code_insee = co.code_insee
            AND op.numfo = dis.numfo
                AND op.generation = '5G'
    )
        AND EXISTS (
        SELECT
            *
        FROM
            basetd.departement dep
        WHERE
            dep.nomdep = co.nomdep
            AND dep.code_departement = '44'
    );

-- Exo 5

SELECT
    dep.nomdep,
    COUNT(co.nom_commune)
FROM
    basetd.departement   dep,
    basetd.commune       co
WHERE
    co.nomdep = dep.nomdep
GROUP BY
    dep.nomdep;

SELECT
    dep.nomdep,
    (
        SELECT
            COUNT(*)
        FROM
            basetd.commune co
        WHERE
            co.nomdep = dep.nomdep
    ) nombre
FROM
    basetd.departement dep
GROUP BY
    dep.nomdep;

-- Exo 6

SELECT
    co.nom_commune,
    COUNT(op.generation)
FROM
    basetd.commune        co,
    basetd.operateur      op,
    basetd.departement    de,
    basetd.distribution   dis
WHERE
    de.code_departement = 44
    AND dis.numfo = op.numfo
    AND dis.code_insee = co.code_insee
    AND de.nomdep = co.nomdep
    AND op.generation = '5G'
GROUP BY
    co.nom_commune
ORDER BY
    2 DESC;

SELECT DISTINCT
    co.nom_commune,
    ( SELECT
    COUNT(*)
FROM
    basetd.operateur      op,
    basetd.departement    de,
    basetd.distribution   dis
WHERE
    de.code_departement = 44
    AND dis.numfo = op.numfo
    AND dis.code_insee = co.code_insee
    AND de.nomdep = co.nomdep
    AND op.generation = '5G' ) as "Nombre d'antenne"
    FROM basetd.commune co 
    ORDER BY 2 DESC;

-- Exo 7
    
SELECT
    co.nom_commune,
    COUNT(op.generation)
FROM
    basetd.commune        co,
    basetd.operateur      op,
    basetd.departement    de,
    basetd.distribution   dis
WHERE
    de.code_departement = 44
    AND dis.numfo = op.numfo
    AND dis.code_insee = co.code_insee
    AND de.nomdep = co.nomdep
    AND op.generation = '5G'
HAVING COUNT(op.generation) > 10
GROUP BY
    co.nom_commune
ORDER BY
    2 DESC;
    
SELECT DISTINCT
    co.nom_commune,
    ( SELECT
    COUNT(*)
FROM
    basetd.operateur      op,
    basetd.departement    de,
    basetd.distribution   dis
WHERE
    de.code_departement = 44
    AND dis.numfo = op.numfo
    AND dis.code_insee = co.code_insee
    AND de.nomdep = co.nomdep
    AND op.generation = '5G'
    HAVING COUNT(op.generation) > 10
    ) na
    FROM basetd.commune co
WHERE
    ( SELECT
    COUNT(*)
FROM
    basetd.operateur      op,
    basetd.departement    de,
    basetd.distribution   dis
WHERE
    de.code_departement = 44
    AND dis.numfo = op.numfo
    AND dis.code_insee = co.code_insee
    AND de.nomdep = co.nomdep
    AND op.generation = '5G'
    HAVING COUNT(op.generation) > 10
    ) IS NOT NULL
    ORDER BY 2 DESC;
    
-- Exo 8
    
SELECT DISTINCT
    co.nom_commune,
    ( SELECT
    COUNT(*)
FROM
    basetd.operateur      op,
    basetd.distribution   dis
WHERE
    dis.numfo = op.numfo
    AND dis.code_insee = co.code_insee
    AND op.generation = '4G'
    ) "4G",
    ( SELECT
    COUNT(*)
FROM
    basetd.operateur      op,
    basetd.distribution   dis
WHERE
    dis.numfo = op.numfo
    AND dis.code_insee = co.code_insee
    AND op.generation = '5G'
    ) "5G"
FROM 
    basetd.commune co
WHERE
    co.nomdep = 'Loire-Atlantique'
    ORDER BY 3;
    
select * from basetd.commune;
-- Exo 9

SELECT 
    op.nomfo, op.technologie, COUNT(*) "Nombre d'Antennes" 
FROM
    basetd.operateur op, basetd.distribution di
WHERE 
    op.numfo = di.numfo
    AND op.technologie in ('5G NR 3500', '5G NR 2100', '5G NR 700')
GROUP BY op.nomfo, op.technologie ORDER BY 1, 2;

SELECT 
    op.nomfo, op.technologie, (SELECT COUNT(*) FROM basetd.distribution di WHERE op.numfo = di.numfo) "Nombres d'Antennes"
FROM
    basetd.operateur op
WHERE 
    op.technologie in ('5G NR 3500', '5G NR 2100', '5G NR 700')
ORDER BY 1, 2;


-- Exo 10

SELECT 
    op.nomfo, op.technologie, COUNT(*) "Nombre d'Antennes" 
FROM
    basetd.operateur op, basetd.distribution di, basetd.commune co
WHERE 
    op.numfo = di.numfo
    AND op.technologie in ('5G NR 3500', '5G NR 2100', '5G NR 700')
    AND co.nom_commune = 'Nantes'
    AND di.code_insee = co.code_insee
GROUP BY op.nomfo, op.technologie ORDER BY 1, 2;

SELECT
    op.nomfo,
    op.technologie,
    (
        SELECT
            COUNT(*)
        FROM
            basetd.distribution   di,
            basetd.commune        co
        WHERE
            op.numfo = di.numfo
            AND di.code_insee = co.code_insee
            AND co.nom_commune = 'Nantes'
    ) "Nombres d'Antennes"
FROM
    basetd.operateur op 
WHERE 
    op.technologie in ('5G NR 3500', '5G NR 2100', '5G NR 700')
ORDER BY 1, 2;

-- Exo 11

SELECT 
    op.nomfo, op.technologie, COUNT(*) "Nombre d'Antennes" 
FROM
    basetd.operateur op, basetd.distribution di, basetd.departement de, basetd.commune co
WHERE 
    op.numfo = di.numfo
    AND co.nomdep = de.nomdep
    AND de.code_departement = 44
    AND di.code_insee = co.code_insee
    AND op.technologie in ('5G NR 3500', '5G NR 2100', '5G NR 700')
GROUP BY op.nomfo, op.technologie ORDER BY 1, 2;

SELECT
    op.nomfo,
    op.technologie,
    (
        SELECT
            COUNT(*)
        FROM
            basetd.distribution   di,
            basetd.commune        co,
            basetd.departement de
        WHERE
            op.numfo = di.numfo
            AND di.code_insee = co.code_insee
            AND co.nomdep = de.nomdep
            AND de.code_departement = 44
    ) "Nombres d'Antennes"
FROM
    basetd.operateur op 
WHERE 
    op.technologie in ('5G NR 3500', '5G NR 2100', '5G NR 700')
ORDER BY 1, 2;

-- Exo 12
SELECT DISTINCT
    co.nom_commune
FROM
    basetd.commune co
WHERE co.code_insee NOT IN (
SELECT DISTINCT
    co.code_insee
FROM
    basetd.commune co
CROSS JOIN basetd.operateur op
WHERE 
    (co.code_insee, op.numfo) NOT IN (
        SELECT di.code_insee, di.numfo FROM basetd.distribution di)
);


        
-- Exo 13 

SELECT DISTINCT
    co.nom_commune
FROM
    basetd.commune co
WHERE co.code_insee NOT IN (
    SELECT DISTINCT
        co.code_insee
    FROM
        basetd.commune co
    CROSS JOIN basetd.operateur op
    WHERE 
        (co.code_insee, op.numfo) NOT IN (
            SELECT di.code_insee, di.numfo FROM basetd.distribution di)
    AND op.generation = '5G'
);

SELECT co.nom_commune
FROM 
    basetd.commune co
WHERE NOT EXISTS ( SELECT *
    FROM basetd.operateur op
    WHERE op.numfo NOT IN (
        SELECT di.numfo 
        FROM 
            basetd.distribution di 
        WHERE 
            di.code_insee = co.code_insee)
            AND op.generation = '5G'
        );
        
--Exo 14

SELECT DISTINCT
    co.nom_commune
FROM
    basetd.commune co
WHERE co.code_insee NOT IN (
    SELECT DISTINCT
        co.code_insee
    FROM
        basetd.commune co
    CROSS JOIN basetd.operateur op
    WHERE 
        (co.code_insee, op.numfo) NOT IN (
            SELECT di.code_insee, di.numfo FROM basetd.distribution di)
    AND op.generation = '4G'
    AND op.nomfo = 'ORANGE'
);

SELECT DISTINCT co.nom_commune
FROM 
    basetd.commune co
WHERE NOT EXISTS ( SELECT *
    FROM basetd.operateur op
    WHERE op.numfo NOT IN (
        SELECT di.numfo 
        FROM 
            basetd.distribution di 
        WHERE 
            di.code_insee = co.code_insee)
            AND op.generation = '4G'
            AND op.nomfo = 'ORANGE'
        );

