package q1

import com.google.gson.Gson
import com.google.gson.JsonObject
import java.io.FileReader
import java.io.Reader

fun main() {
    val nomFichier = "ressources/exemple.json"
    val reader = FileReader(nomFichier)
    val arbreJson = Gson().fromJson(reader, JsonObject::class.java)
    println(arbreJson)
    println(arbreJson.get("matiere"))
    println(arbreJson.get("responsable"))
    var cnt = 0
    arbreJson.get("enseignants").asJsonArray.size()
    var temp = arbreJson.get("enseignants").asJsonArray[2].asJsonObject.get("email")
    println(temp)
    println(arbreJson.get("enseignants").asJsonArray.map { e -> e.asJsonObject.get("nom") })
    println(cnt)
    reader.close()
}