package q2

fun main() {
    println("**** serialisation ***")
    val objet1 = Message("Un test", "Ceci est définitivement un test")
    objet1.serialiser("Test.json")
    print(objet1.deserialiserMessage("Test.json"))
}