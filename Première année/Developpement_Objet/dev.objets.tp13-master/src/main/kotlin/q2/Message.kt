package q2

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.apache.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.runBlocking
import org.apache.http.HttpHost
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException

class Message (titre :String, contenu : String) {
    companion object Choice {
        fun iut() : HttpClient {
            val client = HttpClient(Apache) {
                engine {
                    customizeClient {
                        setProxy(HttpHost("srv-proxy-etu-2.iut-nantes.univ-nantes.prive", 3128))
                    }
                }
            }
            return client
        }

    }
    private var titre : String
    private var contenu : String

    init {
        this.titre  = titre
        this.contenu = contenu
    }

    @Override
    override fun toString(): String {
        return "\n-------\n$titre\n-------\n$contenu"
    }


    fun serialiser(nomFichier : String) {
        val writer = FileWriter(nomFichier)
        Gson().toJson(this, writer)
        writer.flush()
        writer.close()
    }

    fun deserialiserMessage(nomFichier : String) : Message? {
        val reader = FileReader(nomFichier)
        val message = Gson().fromJson(reader, Message::class.java)
        reader.close()
        return message
    }

    fun charger(url : String): Message? {

        var choix = readln()
        var client : HttpClient
        if (choix == "IUT") {
           client = iut()
        }
        else {
            client = HttpClient(CIO)
        }

        var titre : String
        var contenu : String

        runBlocking {
            val reponse: io.ktor.client.statement.HttpResponse = client.get(url).body()
            val json = JsonParser.parseString(reponse.body<String>())
            titre = json.asJsonObject.get("title").asString
            contenu = json.asJsonObject.get("body").asString
        }
    return Message(titre, contenu)
    }


}




