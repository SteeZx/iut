package exo2

class Pays(nom : String, capitale : String,
           continent : String, population : Int,
           superficie : Double) : Comparable<Pays> {


    private val nom : String
    private val capitale : String
    private val continent : String
    private val superficie : Double
    private val population : Int

    init {
        this.nom=nom
        this.capitale = capitale
        this.continent = continent
        this.superficie = superficie
        this.population = population
    }


    fun getNom() : String {
       return nom
    }

    fun getCapitale() : String {
       return capitale
    }

    fun getContinent() : String {
       return continent
    }

    fun getPopulation() : Int {
        return population
    }

    fun getSuperficie() : Double {
       return superficie
    }

    override fun toString(): String {
        return "Un truc"
    }

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is Pays) return false
        if (this.getNom() != other.getNom()) return false
        return true
    }

    override operator fun compareTo(o: Pays): Int {
        return this.getPopulation().compareTo(o.getPopulation())
    }


}