package exo2

import java.io.File
import java.util.*

class Monde {

    private val monde = mutableListOf<Pays>()

    /**
     * Retourne le nombre de pays dans le monde
     */
    fun taille(): Int {
        return monde.size
    }

    /**
     * Ajoute le pays passé en paramètre s'il n'est pas déjà présent
     * @param  p le pays à ajouter
     * @return true si le pays a bien été ajouté
     */
    fun ajouter(p: Pays): Boolean {
        if (monde.indexOf(p) == -1) {
            monde.add(p)
            return true
        }
        return false
    }

    override fun toString(): String {
        return "Test"
    }

    fun remplir(nomFichier : String) {
        val inputFile : File
        inputFile = File(nomFichier)
        assert(inputFile != null)
        val scanner = Scanner(inputFile)

        while (scanner.hasNext()){
            val items = scanner.nextLine().split(";")
            val pays = Pays(items[0], items[1], items[2], items[3].toInt(),items[4].toDouble())
            ajouter(pays)
        }

    }

    /**
     * Retourne le Pays de nom passé en paramètre.
     * Méthode utilisant une boucle pour effectuer une recherche séquentielle.
     * @param  nom du pays à obtenir
     * @result le pays trouvé ou null si le pays indiqué n'est pas présent.
     */
    fun getPays(nom: String): Pays? {
        for (i in monde) {
            if (i.getNom() == nom) return i
        }
        return null
    }

    /**
     * Retourne le Pays de nom passé en paramètre.
     * Méthode utilisant indexOf() et get() de List.
     * indexOf() utilise equals... qui a été redéfinie
     * Pensez à créer un faux pays, à partir du nom recherché
     * @param  nom du pays à obtenir
     * @result le pays trouvé ou null si le pays indiqué n'est pas présent.
     */
    fun getPays2(nom: String): Pays? {
        val fauxPays = Pays(nom, "", "", 0, 0.0)
        for (i in monde) {
            if (i.getNom() == fauxPays.getNom()){
                return i
            }
        }
        return null
    }

    /**
     * Retourne une List des Pays plus peuplés que le pays passé en paramètre.
     * @param  ref le pays reference
     */
    fun plusPeuple(ref: Pays): MutableList<Pays> {
        val monde_new = mutableListOf<Pays>()
        for (i in monde){
            if (i.getPopulation() >= ref.getPopulation()){
                monde_new.add(i)
            }
        }
        return monde_new
    }

    /**
     * Retourne une List des Pays dont la population
     * est strictement supérieure à la limite
     */
    fun populationSuperieureA(limite : Int) : MutableList<Pays> {
        val monde_new = mutableListOf<Pays>()
        for (i in monde){
            if (i.getPopulation() > limite){
                monde_new.add(i)
            }
        }
        return monde_new
    }

    /**
     * Retourne (une copie de) la liste des Pays du monde
     */
    fun monde(): MutableList<Pays> {
        val monde_copie = monde
        return monde_copie
    }

    /**
     * Retourne (une copie de) la liste des Pays
     * du monde trié de manière croissante
     * selon l'ordre naturel.
     */
    fun mondeTrie(): MutableList<Pays> {
        val monde2 = monde
        monde2.sort()
        return monde2
    }

    /**
     * Retourne un tableau trié de manière croissante
     * des Pays du monde
     * selon l'ordre naturel.
     */
    fun mondeTrieTab(): Array<Pays> {
        val monde2 = monde.toTypedArray()
        monde2.sort()
        return monde2
    }


    /**
     * Retourne (une copie de) la liste des Pays
     * du monde trié selon la superficie des pays
     */
    fun mondeTrieSelonSuperficie(): MutableList<Pays> {
        val newMonde = monde
        newMonde.sortWith(Comparator { o1, o2 -> o1.getSuperficie().compareTo(o2.getSuperficie()) })
        return newMonde
    }

}

