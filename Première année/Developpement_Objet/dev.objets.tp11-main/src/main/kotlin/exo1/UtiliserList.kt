package exo1

fun main() {
    val premiereListe = mutableListOf<String>("Lanoix", "Berdugin", "Renaud", "a")
    val premierLinkedHashSet = linkedSetOf<String>("Lanoix", "Berdugin", "Renaud")
    val hashSet = hashSetOf<String>("Lanoix", "Berdugin", "Renaud", "A", "B", "C")
    premiereListe.sortWith(Comparator { o1, o2 -> o1.uppercase().compareTo(o2.uppercase()) })
    for (i in premiereListe) {
        println(i)
    }
    println(premierLinkedHashSet)
    println("===============")
    println(premiereListe.find { i -> (i == "Renaud") })


    premierLinkedHashSet.add("A")
    premierLinkedHashSet.add("B")
    premierLinkedHashSet.add("C")
    premierLinkedHashSet.add("A")
    for (i in hashSet){
        println(i)
    }
}

