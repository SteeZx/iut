class Marchandise(poids : Double, volume : Double) {

    private var poids : Double
    private var volume : Double

    init {
        this.poids = poids
        this.volume = volume
    }

    public fun donneVolume() = volume
    public fun donnePoids() = poids
}