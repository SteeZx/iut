open class CargaisonAerienne(distance : Int, ): Cargaison(distance) {

    override fun encombrement(marchandise: Marchandise) = marchandise.donneVolume()

    override fun limite() = 80_000.0

    override fun facteur() = 10
}