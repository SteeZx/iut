class CargaisonRoutiere(distance : Int): Cargaison(distance) {
    override fun limite() = 38_000.0

    override fun facteur() = 4
}