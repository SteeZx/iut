abstract class Cargaison(distance : Int) {

    var distance : Int
    var encombrementActuel : Double
    var cargaison  = arrayOfNulls<Marchandise?>(100)

    init {
        this.distance = distance
        encombrementActuel = 0.0
    }

    fun ajouter(marchandiseAjoutee: Marchandise): Boolean {
        if (encombrementActuel + encombrement(marchandiseAjoutee) <= limite()) {
            for (i in 0 until (cargaison.size)) {
                if (cargaison[i] == null) {
                    cargaison[i] = marchandiseAjoutee
                    encombrementActuel += encombrement(marchandiseAjoutee)
                    return true
                }
            }
            return false
        }
        return false
    }

    protected open fun encombrement(marchandise: Marchandise): Double {
        return marchandise.donnePoids()
    }

    fun cout() = encombrementActuel*distance*facteur()

    fun rechercher(marchandiseRecherchee: Marchandise): Int {
        for (i in 0 until (cargaison.size)) {
            if (cargaison[i] == marchandiseRecherchee) return i
        }
        return -1
    }

    protected abstract fun limite() : Double

    protected open fun facteur() = 1
}