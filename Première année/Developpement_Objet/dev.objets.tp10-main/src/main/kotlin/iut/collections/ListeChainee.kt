package iut.collections

open class ListeChainee<E>: Liste<E>, PileChainee<E>() {

    override fun index(element: E): Int {
        var index = 0
        var curseur = debut

        while (curseur!!.valeur() != element) {
            curseur = curseur.suivant()
            index += 1
        }
        return index
    }

    override fun consulter(index: Int): E {
        if (this.taille() < index || index < 0) throw IndexOutOfBoundsException()

        var i = 0
        var curseur = debut

        while (i != index) {
            i += 1
            curseur = curseur!!.suivant()
        }
        return curseur!!.valeur()
    }

    override fun insererEnQueue(element: E) {
        TODO("Not yet implemented")
    }

    override fun inserer(index: Int, element: E) {
        if (this.taille() < index || index < 0) throw IndexOutOfBoundsException()

        var cell = Cellule<E>(element)
        var curseur = debut
        for (i in 0 until taille()) {
            if (i == index-1) {
                curseur?.modifieSuivant(cell)
            } else if (i == taille()) {
                cell.modifieSuivant(null)
                break
            } else if (i == index+1) {
                cell.modifieSuivant(curseur)
                break
            }
            curseur = curseur!!.suivant()
        }

    }

    override fun supprimer(index: Int) {
        TODO("Not yet implemented")
    }

    override fun consulterEnQueue(): E {
        TODO("Not yet implemented")
    }

    override fun supprimerEnQueue() {
        TODO("Not yet implemented")
    }

}