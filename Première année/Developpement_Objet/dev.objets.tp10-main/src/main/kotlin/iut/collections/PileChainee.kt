package iut.collections

open class PileChainee<E>(): Pile<E>  {

    protected var debut : Cellule<E>? = null

    override fun estVide(): Boolean = debut?.valeur() == null

    override fun taille(): Int {
        var value = debut
        var count = 0
        while (value!!.suivant() != null)
            count += 1
            value = value.suivant()
        return count
    }

    override fun empiler(element: E) {
        debut = Cellule<E>(element, debut)
    }

    override fun consulter(): E {
        if (this.taille() < 1) {
            throw NoSuchElementException()
        }
        return debut!!.valeur()
    }

    override fun depiler(): E {
        if (this.taille() < 1) {
            throw NoSuchElementException()
        }
        debut = debut?.suivant()
        return debut!!.valeur()
    }
}