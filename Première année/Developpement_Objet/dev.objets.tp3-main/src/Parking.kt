class Parking(places : Int = 2){
    private var park  : Array<Voiture?>

    init {
        park = arrayOfNulls<Voiture>(places)
    }

    fun nombreDePlacesLibres() : Int {
        var cpt : Int = 0
        for (i in 0 until park.size) {
            if (park[i] == null) cpt++
        }
        return cpt
    }
    fun nombreDePlacesTotales() : Int {
        return park.size
    }
    fun placeLibre(numeroPlace : Int): Boolean {
        if (numeroPlace > park.size-1) return false
        if (numeroPlace < 0 || (park[numeroPlace] != null)) return false
        return true
    }

    fun stationner(numeroPlace : Int, voitureStationnee : Voiture) : Boolean {
        if (numeroPlace < 0) return false
        if (numeroPlace > park.size-1) return false
        if (park[numeroPlace] != null || voitureStationnee in park) return false
        park.set(numeroPlace, voitureStationnee)
        return true
    }

} 



