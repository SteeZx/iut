class Voiture (
    mod : String,
    coul : String,
    vitesseCourante : Double = 0.0,
    vitMax: Double = 240.0,
    enMarche : Boolean = false) {

    private val modele : String
    private var couleur : String
    private var vitesseCourante : Double
    private val vitesseMaximum : Double
    private var enMarche : Boolean
    private var acheter : Personne?

    init {
        modele = mod
        couleur = coul
        vitesseMaximum = vitMax
        acheter = null
        this.vitesseCourante = vitesseCourante
        this.enMarche = enMarche
        this.acheter = null
    }

    fun acheter(acheteur : Personne) {
        acheter = acheteur
    }

    fun demarrer() {
        enMarche = true
    }

    fun arreter() {
        enMarche = false
        vitesseCourante = 0.0
    }

    fun estEnMarche() : Boolean {
        return enMarche
    }

    fun repeindre(nouvelleCouleur : String) {
        couleur = nouvelleCouleur
    }

    fun accelerer(acceleration : Double) : Double{
        
        if ((enMarche) && (acceleration > 0)) {
            vitesseCourante = vitesseCourante + acceleration
            if (vitesseCourante > vitesseMaximum) { vitesseCourante = vitesseMaximum }
        }
        return vitesseCourante
    }

    fun decelerer(deceleration : Double) : Double{
        if (deceleration > vitesseCourante) {
            vitesseCourante = 0.0
        } else if (deceleration > 0) { vitesseCourante = vitesseCourante - deceleration }
        return vitesseCourante
    }

    // fun estGaree() : Boolean{
    //     if (Voiture in Parking) return true
    //     return false
    // }
}