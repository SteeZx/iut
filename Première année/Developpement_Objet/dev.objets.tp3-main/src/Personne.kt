class Personne (prenom : String, nom : String){
    private var nom : String
    private var prenom : String

    init{
        this.nom = nom
        this.prenom = prenom
    }

    fun donneNomComplet() : String {
        val nomComplet : String = prenom + " " + nom.uppercase()
        return nomComplet
    }
}