package iut.math

import kotlin.random.Random

class SquareMatrix (order : Int = 0) {
    private var matrice  : Array<Array<Int>>
    private var order : Int
    init {
        //require(order > 0){"ordre négatif ou null"}
        if (order<=0) throw IllegalArgumentException("order must be greater than 0")
        matrice = Array(order){Array(order) {0} }
        this.order = order
    }

    override fun equals(other : Any?): Boolean {
        if (this === other) return true
        if (other === null) return false
        if (other !is SquareMatrix) return false
        if (this.order() != other.order()) return false

        for (line in matrice.indices) {
            for (elt in matrice[line].indices) {
                if (matrice[line][elt] != other.matrice[line][elt]) return false
            }
        }
        return true
    }

    fun order() : Int {
        return order
    }

    fun isNull() : Boolean = this == SquareMatrix(order())



    override fun toString(): String {
        var str : String = ""
        for (line in matrice) {
            for (elt in line) {
                str += elt.toString() + "\t"
            }
            str += "\n"
        }
        return str
    }

    fun setAsIdentity() {
        var new = Array(order){Array(order) {0} }
        for (indice in matrice.indices) {
            new[indice][indice] = 1
        }
        matrice = new

    }

    fun isIdentity() : Boolean {
        val identity = SquareMatrix(matrice.size)
        identity.setAsIdentity()
        return this == identity

    }

    fun randomize(from : Int, until :Int) {
        matrice = Array(order){Array(order) {Random.nextInt(from, until)} }
        require((from <= Int.MAX_VALUE) && (from >= Int.MIN_VALUE) && (until <= Int.MAX_VALUE) && (until >= Int.MIN_VALUE)){"Valeur de from ou until incorrecte"}
    }

    operator fun plus(matrix : SquareMatrix) : SquareMatrix {
        if (this.order() != matrix.order()) throw ArithmeticException()
        for (i in matrice.indices) {
            for (j in matrice[i].indices) {
                if ((matrice[i][j] + matrix.matrice[i][j] <= Int.MAX_VALUE) && (matrice[i][j] + matrix.matrice[i][j]) >= Int.MIN_VALUE)
                    matrice[i][j] += matrix.matrice[i][j]
                else {
                    throw ArithmeticException()
                }
            }
        }
        return this
    }

    operator fun times(factor : Int) : SquareMatrix {
    // TODO

    }

    operator fun unaryMinus() : SquareMatrix {
        // TODO
        throw NotImplementedError()
    }

    operator fun minus(matrix : SquareMatrix) : SquareMatrix {
        // TODO
        throw NotImplementedError()
    }

    fun transposite() : SquareMatrix {
        // TODO
        throw NotImplementedError()
    }


    fun isSymetrical() : Boolean {
        return false
    }

    fun isAntiSymetrical() : Boolean {
        return false
    }


    operator fun times(matrix : SquareMatrix) : SquareMatrix {
        // TODO
        return SquareMatrix(1)
    }


}