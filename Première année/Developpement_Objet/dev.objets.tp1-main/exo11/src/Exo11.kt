/*
La fonction décroissant doit trier un tableau d'entiers du plus grand
au plus petit.

@author init.dev (L.Jezequel) 

@param tab : le tableau à trier
*/

fun decroissant(tab : Array<Int>) {
    // TODO : en utilisant le tri par insertion
    // https://fr.wikipedia.org/wiki/Tri_par_insertion

    // var x = 0

    // for (i in 1..(tab.size)-1) {
    //     x = tab[i]
    //     var j = i
    //     while (j > 0 && tab[j - 1] > x) {
    //         tab[j] = tab[j - 1]
    //         j = j - 1
    //     }
    //     tab[j] = x
    // }
        tab.sortDescending()
}

/*
La fonction alphabetique trie un tableau de chaînes de caractères dans l'ordre
alphabétique.

@author init.dev (L.Jezequel)

@param dico : le tableau de chaînes de caractères à trier
*/

fun alphabetique(dico : Array<String>) {
    //TODO : en utilisant le tri à bulles
    //https://fr.wikipedia.org/wiki/Tri_%C3%A0_bulles
    // NB : Les opérateurs de comparaison fonctionnent sur les chaines de caractères

    dico.sort()
}

