package exo2

fun main() {
    val d = donneDoubleSaisi()
    println("La valeur saisie est $d")
}

fun donneDoubleSaisi() : Double {
    var result = 0.0

    print("Saisir un double : ")
    return (
            try{
        val chaineLue = readln()
        result = chaineLue.toDouble()
        return result
    }
    catch (e : NumberFormatException) {
        println("Format de la saisie inconnus, veuillez marquer quelque chose du type : 12.0, 12, -12.0 etc ... Ne pas mettre de texte SVP")
        donneDoubleSaisi()
    }
            )
}