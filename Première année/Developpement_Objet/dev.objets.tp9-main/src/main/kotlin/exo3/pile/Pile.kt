package exo3.pile

class Pile(taille : Int = 10) {

    private val valeurs : Array<Int?>
    private var sommet : Int

    init {
        require(taille > 0) {"La taille doit être strictement supérieur à 0"}
        valeurs = arrayOfNulls(taille)
        sommet = -1
    }

    private fun estVide() =
        (sommet == -1)

    private fun estPleine() =
        (sommet == valeurs.size-1)

    fun empiler(elt : Int) {
        if (valeurs[valeurs.size-1] != null){
            throw PileException("Problème de pile")
        }
        valeurs[++sommet] = elt
    }

    fun depiler() : Int {
        if (valeurs[0] == null){
            throw PileException("Problème de pile")
        }
        return valeurs[sommet--]!!
    }

    override fun toString(): String {
        var str = "["
        for (i in 0 .. sommet)
            str += "> ${valeurs[i]} "
        return str + "]"
    }
}