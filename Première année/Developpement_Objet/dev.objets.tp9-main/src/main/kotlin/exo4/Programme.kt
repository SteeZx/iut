package exo4

import exo3.pile.Pile
import kotlin.random.Random

fun main() {
    val pile = genererPile()
    println(count)



}

var count = 0

fun genererPile(): Pile {
    var pile = Pile(1)
    count += 1
    try {
        pile = Pile(Random.nextInt(-1000000, 10))
        return pile
    }
    catch (e : IllegalArgumentException) {
        println("Taille de la pile inférieur à 1, régénération de la pile en cours ...")
        genererPile()
    }
    return pile
}