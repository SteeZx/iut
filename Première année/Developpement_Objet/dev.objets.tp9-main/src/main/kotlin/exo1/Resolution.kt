package exo1

import java.net.InetAddress
import java.net.UnknownHostException

/*
fun main() {
    print("Saisir un nom :")
    val nom = readln()
    val ip = nomVersIP(nom)
    println("IP correspondante :$ip")
}


fun nomVersIP(nom : String) : String {
    try{
        return InetAddress.getByName(nom).hostAddress
    }
    catch (e : UnknownHostException) {
        return "L'adresse en question est inconnu."
    }

}
 */

fun main() {
    print("Saisir un nom :")
    val nom = readln()
    try {
        val ip = nomVersIP(nom)
        println("IP correspondante :$ip")
    }
    catch(e : Exception) {println("Mauvaise Adresse")}

}


fun nomVersIP(nom : String) : String {
    try{
        return InetAddress.getByName(nom).hostAddress
    }
    catch (e : UnknownHostException) {
        throw Exception("Juif")
    }

}