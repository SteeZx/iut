open class Vehicule (
    mod : String, 
    coul : String,
    vitMax: Double,
    ) : Propriete {
    
    private val modele : String
    private var couleur : String
    private var vitesseCourante : Double
    private val vitesseMaximum : Double
    private var enMarche : Boolean
    private var conducteur : Personne?
    private var proprietaire : Proprietaire?

    init {
        modele = mod
        couleur = coul
        vitesseMaximum = vitMax
        vitesseCourante = 0.0
        enMarche = false
        conducteur = null
        proprietaire = null
    }

    fun devientConducteur(personne : Personne) {
        conducteur = personne
    }
    fun plusDeConducteur(){
        if (conducteur == null) arreter()
    }

    override fun acheter(acheteur : Proprietaire) {
        proprietaire = acheteur
    }

    fun demarrer() {
        if (conducteur != null) enMarche = true
        else enMarche = false
    }

    fun arreter() {
        enMarche = false
        vitesseCourante = 0.0
    }

    fun estEnMarche() : Boolean {
        return enMarche
    }

    fun repeindre(nouvelleCouleur : String) {
        couleur = nouvelleCouleur
    }

    open fun accelerer(acceleration : Double) : Double{
        
        if ((enMarche) && (acceleration > 0)) {
            vitesseCourante = vitesseCourante + acceleration
            if (vitesseCourante > vitesseMaximum) { vitesseCourante = vitesseMaximum }
        }
        return vitesseCourante
    }

    open fun decelerer(deceleration : Double) : Double{
        if (deceleration > vitesseCourante) {
            vitesseCourante = 0.0
        } else if (deceleration > 0) { vitesseCourante = vitesseCourante - deceleration }
        return vitesseCourante
    }

    // fun estGaree() : Boolean{
    //     if (Voiture in Parking) return true
    //     return false
    // }
}