
fun main() {
	var p1 = Point()
	var p2 = Point(1, 2)
	println("Coordonnées p1 : ${p1.getX()}, ${p1.getY()}")
	println("Coordonnées p2 : ${p2.getX()}, ${p2.getY()}")
	p2.setY(3)
	println("Coordonnées p2 : ${p2.getX()}, ${p2.getY()}")
	p2.translater(1, 1)
	p1.deplacer(2, 10)
	println("Coordonnées p1 : ${p1.getX()}, ${p1.getY()}")
	println("Coordonnées p2 : ${p2.getX()}, ${p2.getY()}")
	println(p2.distanceOrigine())
	println(p1.distance(p2))
	p2 = p1
	p1.translater(2, 1)
	println("Coordonnées p1 : ${p1.getX()}, ${p1.getY()}")
	println("Coordonnées p2 : ${p2.getX()}, ${p2.getY()}")
}
