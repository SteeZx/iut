import kotlin.random.*


fun afficher(points : Array<Point?>) {
	for (i in 0..(points.size-1)) {
		if (points[i] == null) {
			println("Vide")
		} else {
			println(points[i])
		}
	}
}

fun remplir(points : Array<Point?>) {
	for (i in 0..(points.size-1)) {
		if (points[i] == null) {
			points[i] = Point(Random.nextInt(1, 10), Random.nextInt(1, 10))
		}else {
			continue
		}
	}
}

fun main() {
	var p1 = Point()
	var p2 = Point(1, 2)
	println("Coordonnées p1 : ${p1.getX()}, ${p1.getY()}")
	println("Coordonnées p2 : ${p2.getX()}, ${p2.getY()}")
	p2.setY(3)
	println("Coordonnées p2 : ${p2.getX()}, ${p2.getY()}")
	p2.translater(1, 1)
	p1.deplacer(2, 10)
	println("Coordonnées p1 : ${p1.getX()}, ${p1.getY()}")
	println("Coordonnées p2 : ${p2.getX()}, ${p2.getY()}")
	println(p2.distanceOrigine())
	println(p1.distance(p2))
	p2 = p1
	p1.translater(2, 1)
	println("Coordonnées p1 : ${p1.getX()}, ${p1.getY()}")
	println("Coordonnées p2 : ${p2.getX()}, ${p2.getY()}")
	var vide = arrayOfNulls<Point>(10)
	vide[0] = p1
	afficher(vide)
	remplir(vide)
	println("==================================================")
	afficher(vide)
}
