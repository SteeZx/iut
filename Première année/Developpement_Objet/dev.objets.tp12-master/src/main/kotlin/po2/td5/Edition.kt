package po2.td5

/**
 * Classe Edition, pour mémoriser les différentes éditions d'un livre
 * @param isbn l'International Standard Book Number, identifiant l'édition d'un livre
 * @param editeur nom de l'éditeur
 * @param annee année de publication
 */

class Edition (isbn: String, editeur: String, annee: Int) {

    private var isbn: String = ""
    private var editeur: String = ""
    private var annee: Int = 0

    init {
        //println(isbn.length)
        var valeur = 0
        var somme = 0
        val longueur = isbn.length
        if (longueur == 10) {
            for (position in 1 .. longueur) {
                valeur = if (isbn[longueur - position] == 'X') 10
                        else isbn[longueur - position].digitToInt()
                println(valeur)
                somme += valeur * position
                //println(somme%11)
            }
            if (somme % 11 != 0) throw LivreException("")
            this.isbn = isbn
        }
        if (isbn.length != 13 || isbn.length != 10) {
            throw LivreException("")
        }
        this.editeur = editeur
        this.annee = annee
    }
}
