package po2.td5

/**
 * Classe permettant d'enregistrer des livres
 * @param listeDeLivres les livres à enregistrer
 */
class Livres(listeDeLivres: MutableList<Livre> = mutableListOf()) {

    private val mesLivres: MutableList<Livre>

    /**
     * créé une nouvelle classe prête à enregistrer des livres.
     */
    init {
        mesLivres = listeDeLivres
    }

    /**
     * ajoute un nouveau livre
     *
     * @throws LivreException si le livre est déjà enregistré
     */
    fun ajoute(livre: Livre) {
        // TODO
    }

    /**
     * @return le nombre de livres enregistrés
     */
    fun nbLivres(): Int {
        TODO()
    }

    /**
     * @return le nombre d'éditions enregistrées. Il peut y en avoir plusieurs pour un livre
     */
    fun nbEditions(): Int {
        TODO()
    }

    /**
     * @return une liste de toutes les livres (on conserve l'ordre d'ajout)
     */
    fun livres(): List<Livre> {
        TODO()
    }

    /**
     * efface le livre des Livres enregistrées
     *
     * @param livre le livre à effacer
     * @throws LivreException si le livre à effacer n'est pas dans les livre enregistrées
     */
    fun efface(livre: Livre) {
        TODO()
    }

    override fun toString(): String {
        TODO()
    }
}