import iut.math.Rational

fun main() {

    // usage toString()
    val r23 = Rational(2,3)
    val r12 = Rational(1,2)
    val r43 = Rational(4,3)
    println(r23)     // 2/3
    println(r12)     // 1/2
    println(r43)     // 4/3
    println(Rational(3))      // 3/1
    println(Rational(0,6))     // 0/6
    println(Rational(2,2))     // 2/2



    // usage "+"
    println("#####")
    var result : Rational = r23 + r12  // on peut utiliser le + car il ya la fonction plus()
    println(result) // 7/6
    result = r23 + r43
    println(result) // 18/9 ou (mieux) 6/3 ou (encore mieux) 2/1



    // usage "+ (entier)"
    println("#####")
    result = r43 + 2
    println(result) // 10/3
    result = r43 + 0
    println(result) // 4/3

    println("#####")
    val r36 = Rational(3,6)
    r23  == r12   // false
    // r23 != r12    // true
    r12 == r36    // true
    println(r23  == r12)
    println(r12 == r36)
    println(r23 != r12)
}