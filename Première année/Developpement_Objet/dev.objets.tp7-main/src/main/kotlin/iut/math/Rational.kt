package iut.math
/**
 * Fabrique un rationnel à partir de deux entiers passés en paramètre ;
 * @param numerator le numérateur
 * @param denominator le dénominateur
 * Par convention, si le denominateur vaut 0, alors on le positionne à 1
 */
class Rational (numerator : Int, denominator :Int=1) {
    private val numerator : Int
    private val denominator : Int

    init {
        this.numerator = numerator
        this.denominator  = if (denominator == 0) 1 else denominator
    }

    override fun toString(): String = "$numerator / $denominator"

    private fun reduce() : Rational {
        val comm = (pgcd(numerator, denominator))
        return Rational(numerator/comm, denominator/comm)
    }

    operator fun plus(i : Rational):Rational = Rational( (numerator*(i.denominator) + (i.numerator)*denominator),(denominator*(i.denominator)) ).reduce()


    operator fun plus(i : Int) = Rational((numerator+(i*denominator)), denominator).reduce()

    operator fun unaryMinus() = Rational(-this.numerator, this.denominator)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other === null) return false
        if(other !is Rational) return false
        val tempa = reduce()
        val tempb = other.reduce()

        if (tempa.numerator != tempb.numerator || tempa.denominator != tempb.denominator) return false

        return true
    }

    override fun hashCode(): Int {
        return hashCode()
    }
}