package iut.math

/**
 * renvoie le "Plus Grand Commun Diviseur" de deux entiers
 * @param aa un entier
 * @param bb un entier
 * @return le pgcd de aa et bb
 */


fun pgcd(aa : Int, bb : Int) : Int {

    var vaa : Int = aa
    var vbb : Int = bb
    var r = aa

    while(r!=0) {
        r = vaa%vbb
        vaa = vbb
        vbb = r
    }


    return vaa
}